# !/usr/bin/env python
#  -*- coding: utf-8 -*-

import time, json, sys, os, traceback
import pandas as pd

# import matplotlib
# matplotlib.use('Agg')

import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split
from scipy.interpolate import UnivariateSpline
import numpy as np
from DataExtractor import *
import fastdtw


obj = DataExtractor(params=['ALTITUDE', 'GS'], ns=None, flight_phase='takeoff')
Folder = '../../Data/TVF/dataTest/TVF-extract/'
# Files = ['F-GZHA-0415180.csv', 'F-GZHA-0415210.csv', 'F-GZHA-0516080.csv', 'F-GZHA-0516120.csv', 'Null_file.csv']
Files = ['F-GZHA-0415180.csv', 'F-GZHA-0415210.csv', 'Null_file.csv']
Time, data_sparse, data_list = obj.run(Files, Folder, ncpus=-1)


plt.plot(data_sparse[0].T, '-')
plt.figure()
plt.plot(data_sparse[1].T, '-')
plt.show()


sys.exit(0)

# Distance matrix

def compute_DTW_matrix(data_list, ncpus=-1, gamma=.001):
	""" Distance matrix using fastDtw. 
		data_list : list of size n which elements are lists of length N_i 
	"""

	n = len(data_list)
	indexes = []

	# GET INDEXES
	for i in range(0,n):
		for j in range(i+1,n):
			indexes.append((i,j))

	# INIT DISTANCE MATRIX
	distance_matrix = np.zeros((n,n))


	# COMPUTE DTW IN PARALLEL
	start_time = time.time()
	def wrapper(i, j, dat):
		return fastdtw(dat[i], dat[j])[0]

	job_server = pp.Server(ppservers=())
	if ncpus == -1:
		ncp = job_server.get_ncpus()
	else:
		ncp = ncpus
	print 'Distance matrix: parallel execution with ncpus = ' + str(ncp)
	job_server.set_ncpus(ncp)
	depfuncs = (fastdtw,)

	jobs = [job_server.submit(func=wrapper, args=(i, j, data_list), depfuncs=depfuncs) for i,j in indexes]
	print("--- CPUs initialization: %s seconds ---" % round(time.time() - start_time, 2))
	
	start_time = time.time()
	distance_vect = numpy.array([job() for job in jobs])
	print("--- DTW computation: %s seconds ---" % round(time.time() - start_time, 2))


	for k,(i,j) in enumerate(indexes):
		distance_matrix[i,j] = distance_vect[k]
		distance_matrix[j,i] = distance_vect[k]

	return np.exp(-gamma * distance_matrix**2)


def normalize(X):
	meanNorm = np.apply_along_axis(norm_2, 1, X).mean()
	def f(x):
		return x / meanNorm
	XX = np.apply_along_axis(f, 1, X)
	return XX

# GET DATA
data_list_alt = [z['ALTITUDE'] for z in data_list]
# NORMALIZE
meanNorm = np.mean([np.linalg.norm(z, ord = 2) for z in data_list_alt])
data_list_alt_norm = [z / meanNorm for z in data_list_alt]



# DTW
DistMat = compute_DTW_matrix(data_list_alt_norm, ncpus=-1, gamma=float(sys.argv[1]))

print 'Spectral Clustering'
from sklearn.cluster import SpectralClustering
SC = SpectralClustering(n_clusters=2, affinity='precomputed')
SC.fit(DistMat)

print SC.labels_
print SC.affinity_matrix_ == DistMat

sys.exit(0)


# 2 courbes

ALT = data_sparse[0]

c0 = ALT[0,:]
idx_null = np.where(np.isnan(c0))[0][0]
c0 = c0[0:idx_null]

c1 = ALT[1,:]
idx_null = np.where(np.isnan(c1))[0][0]
c1 = c1[0:idx_null]

print c0
print c1

print len(c0), len(c1)




dtw = fastdtw(c0, c1)

distDtw = dtw[0]
warp = dtw[1]

warp_x = [z[0] for z in warp]
warp_y = [z[1] for z in warp]
plt.plot(warp_x, warp_y, '-', linewidth=2)
plt.show()












# 
# TEST ONE
# 

obj = DataExtractor(params=['ALTITUDE', 'GS'], ns=500, flight_phase='takeoff')
R1 = obj.run_one('../../Data/TVF/dataTest/TVF-extract/F-GZHA-0415180.csv')

print np.max(R1['ALTITUDE']), len((R1['ALTITUDE']))

plt.figure()
plt.plot(R1['Time'], R1['ALTITUDE'], '-')
plt.ylabel('ALTITUDE')
plt.xlabel('Time')

plt.figure()
plt.plot(R1['Time'], R1['GS'], '-')
plt.ylabel('GS')
plt.xlabel('Time')


plt.show()
