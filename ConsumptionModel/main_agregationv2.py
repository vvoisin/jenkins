# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from src.Learner import *
from src.GridSearch import *

import time, json
import pandas as pd



print 'Loading data'
DesignMatrix = np.loadtxt('DesignMatrix.csv', delimiter=',')
Y = np.loadtxt('Consumption.csv', delimiter=',')
with open('groupsInfos.json', 'r') as f:
	groupsInfos = json.load(f)	




ntr = 1000
nval = 500
ntest = 100
Xtr, tmp, Ytr, Ytmp = train_test_split(DesignMatrix, Y, train_size = ntr, test_size=nval+ntest)
Xval, Xte, Yval, Yte = train_test_split(tmp, Ytmp, train_size = nval, test_size=ntest)





print '\t--- 1 mean'
model_SVR = Learner(regressor='SVR', C=100000)
model_RF = Learner(regressor='RF', n_estimators=50)
model_ETR = Learner(regressor='ETR', n_estimators=50)

print '\nCombined Learner'
Machines = {'SVR':model_SVR, 'RF':model_RF, 'ETR':model_ETR}
CL = CombinedLearner(np.mean, Machines)
CL.train(Xtr, Ytr)
mae = CL.predict(Xval, Yval)
print round(mae, 2)



print '\t--- 2 mean'
model_SVR = Learner(regressor='SVR', C=100000)
model_SVR.train(Xtr, Ytr)

model_RF = Learner(regressor='RF', n_estimators=50)
model_RF.train(Xtr, Ytr)

model_ETR = Learner(regressor='ETR', n_estimators=50)
model_ETR.train(Xtr, Ytr)

print '\nCombined Learner'
Machines = {'SVR':model_SVR, 'RF':model_RF, 'ETR':model_ETR}
CL = CombinedLearner(np.mean, Machines)
mae = CL.predict(Xval, Yval)
print round(mae, 2)



print '\t--- 3 median'
print '\nSVR'
model_SVR = Learner(regressor='SVR', C=100000)

print '\nRF'
model_RF = Learner(regressor='RF', n_estimators=50)

print '\nETR'
model_ETR = Learner(regressor='ETR', n_estimators=50)

print '\nCombined Learner'
Machines = {'SVR':model_SVR, 'RF':model_RF, 'ETR':model_ETR}
CL = CombinedLearner(np.median, Machines)
CL.train(Xtr, Ytr)
mae = CL.predict(Xval, Yval)
print round(mae, 2)



print '\t--- 4 median'
print '\nSVR'
model_SVR = Learner(regressor='SVR', C=100000)
model_SVR.train(Xtr, Ytr)

print '\nRF'
model_RF = Learner(regressor='RF', n_estimators=50)
model_RF.train(Xtr, Ytr)

print '\nETR'
model_ETR = Learner(regressor='ETR', n_estimators=50)
model_ETR.train(Xtr, Ytr)

print '\nCombined Learner'
Machines = {'SVR':model_SVR, 'RF':model_RF, 'ETR':model_ETR}
CL = CombinedLearner(np.median, Machines)
mae = CL.predict(Xval, Yval)
print round(mae, 2)






sys.exit(0)


Machines = np.concatenate((pred_SVR.reshape((nval,1)), pred_RF.reshape((nval,1)), pred_ETR.reshape((nval,1))), axis=1)
predMean = np.mean(Machines, axis=1)
maeMean = np.mean(np.fabs(Yval - predMean))
print round(maeMean, 2)

import matplotlib.pyplot as plt
line = np.arange(np.min(Yval), np.max(Yval), 100)
plt.plot(predMean, Yval, '.')
plt.ylabel('Observed')
plt.xlabel('Predicted')
plt.plot(line, line, 'k-')
plt.show()






print 'EXTRACT FOR r'
pred_SVR_train = model_SVR.predict(Xtr)
pred_SVR_test = model_SVR.predict(Xval)
m1 = np.concatenate((pred_SVR_train, pred_SVR_test)).reshape((ntr+nval, 1))

pred_RF_train = model_RF.predict(Xtr)
pred_RF_test = model_RF.predict(Xval)
m2 = np.concatenate((pred_RF_train, pred_RF_test)).reshape((ntr+nval, 1))

pred_ETR_train = model_ETR.predict(Xtr)
pred_ETR_test = model_ETR.predict(Xval)
m3 = np.concatenate((pred_ETR_train, pred_ETR_test)).reshape((ntr+nval, 1))

Machines = np.concatenate((m1,m2,m3), axis=1)

np.savetxt('machines.csv', Machines, delimiter=',')
np.savetxt('xtrain.csv', Xtr, delimiter=',')
np.savetxt('ytrain.csv', Ytr, delimiter=',')
np.savetxt('xtest.csv', Xval, delimiter=',')
np.savetxt('ytest.csv', Yval, delimiter=',')



