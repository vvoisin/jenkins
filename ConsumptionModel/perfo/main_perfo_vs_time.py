import sys, csv, math, os, traceback
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from datetime import *
from matplotlib.backends.backend_pdf import PdfPages


def adjust_spines(ax, spines):
    for loc, spine in ax.spines.items():
        if loc in spines:
            spine.set_position(('outward', 10))  # outward by 10 points
            spine.set_smart_bounds(True)
        else:
            spine.set_color('none')  # don't draw spine

    # turn off ticks where there is no spine
    if 'left' in spines:
        ax.yaxis.set_ticks_position('left')
    else:
        # no yaxis ticks
        ax.yaxis.set_ticks([])

    if 'bottom' in spines:
        ax.xaxis.set_ticks_position('bottom')
    else:
        # no xaxis ticks
        ax.xaxis.set_ticks([])



results = pd.read_csv('Perfo_constructeur.csv', sep=',')
Regist = np.unique(np.array([z[0:6] for z in results.file], dtype=str))
Time = np.array([z[7:14] for z in results.file], dtype=str)
results['Regist'] = pd.Series([z[0:6] for z in results.file])
results['Time'] = pd.Series(Time)

results_sorted = results.sort(columns='Time')



diff = results.perfo - results.conso

pp = PdfPages('Performance_vs_Consommation.pdf')

# All
# print 'All'
x = results_sorted.perfo - results_sorted.conso


fig = plt.figure(figsize=(12, 6))
ax = fig.add_subplot(111)
ax.plot(x, 'o', clip_on=False)
adjust_spines(ax, ['left', 'bottom'])
plt.plot([0,len(x)], [0,0], 'k-', linewidth=2.0)
plt.ylim(ymax = max(diff)+10, ymin = min(diff)-10)
plt.xlim(xmax = len(x), xmin = -1)
plt.xlabel('Time')
plt.ylabel('Performance - Consommation')
plt.title('Tout immat')
pp.savefig()

# for each Registration
for r in Regist:
	print '\n\n' + r

	tmp = results[results.Regist == r]
	
	for i,z in enumerate(tmp.Time):
		print i,z

	x = tmp.perfo - tmp.conso
	fig = plt.figure(figsize=(12, 6))
	ax = fig.add_subplot(111)
	ax.plot(x, 'o', clip_on=False)
	adjust_spines(ax, ['left', 'bottom'])
	plt.plot([0,len(x)], [0,0], 'k-', linewidth=2.0)
	plt.ylim(ymax = max(diff)+10, ymin = min(diff)-10)
	plt.xlim(xmax = len(x), xmin = -1)
	plt.xlabel('Time')
	plt.ylabel('Performance - Consommation')
	plt.title(r)
	plt.grid(True)
	pp.savefig()

pp.close()





# t1 = Time[0]
# t1_format = datetime(2015, int(t1[0:2]), int(t1[2:4]), int(t1[4:6]), 0, 0, 0)

# t2 = Time[1]
# t2_format = datetime(2015, int(t2[0:2]), int(t2[2:4]), int(t2[4:6]), 0, 0, 0)





# mae = np.mean(np.fabs(results.perfo - results.conso))
# print 'Mean Absolute Error = ' + str(round(mae,2)) + ' kg.'



# plt.plot(results.perfo, results.conso, '.')
# line = np.arange(min(results.perfo), max(results.perfo), 100)
# plt.plot(line, line, 'k-')
# plt.xlabel('Performance')
# plt.ylabel('Consommation')
# plt.title('Mean Absolute Error = ' + str(round(mae,2)) + ' kg.')

# plt.show()
