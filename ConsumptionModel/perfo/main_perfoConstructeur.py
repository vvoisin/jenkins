import sys, csv, math, os, traceback
import numpy as np
import pandas as pd

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import Rbf

# from src.utils import *
from PerfoConstructeur import *


if int(sys.argv[1]):
	print 'Get files and compute performances'
	dataPath = '/home/bapt/Documents/Flight data/TVF/extract/'
	mounth = '05'

	Files_tmp = np.array(os.listdir(dataPath))

	mounth_all = np.array([s[7:9] for s in Files_tmp])
	idx = np.where(mounth_all == mounth)[0]

	Files = Files_tmp[idx]

	print 'Get fuel factors'
	fuel_factor = pd.read_csv('./data/FuelFactor_01-06-2015.csv', delimiter=',')

	perf = PerfoConstructeur('R/')
	perf.run_files(Files=Files, folder=dataPath, fuel_factor_df=fuel_factor)


	print 'Get Flight infos'
	infosPath = 'data/'
	Infos = pd.read_csv(infosPath+'FlightInfos.csv', delimiter=',')


	print 'Print results'
	results = []
	for j,f in enumerate(Infos.File):
		try:
			perfo = perf.consumption[f]['perfo']
			GW = perf.consumption[f]['GW']
			ISA = perf.consumption[f]['ISA']
			ToC = perf.consumption[f]['ToC']
			if perfo > 0:
				results.append((f, perfo, GW, ISA, ToC, Infos.Consumption[j]))
			# print f + ' - Performance = ' + str(perf.consumption[f]['perfo']) + ' - Consommation = ' + str(Infos.Consumption[j])
		except KeyError:
			print f + ' not found.'

	# results = np.array(results, dtype=[('file', 'S18'), ('perfo', 'float64'), ('conso', 'float64')])
	results = pd.DataFrame(results, columns=['file', 'perfo', 'GW', 'ISA', 'ToC', 'conso'])
	results.to_csv('Perfo_constructeur.csv', sep=',', header=True, index=False)
else:
	results = pd.read_csv('Perfo_constructeur.csv', sep=',')


mae = np.mean(np.fabs(results.perfo - results.conso))
print 'Mean Absolute Error = ' + str(round(mae,2)) + ' kg.'




plt.plot(results.perfo, results.conso, '.')
line = np.arange(min(results.perfo), max(results.perfo), 100)
plt.plot(line, line, 'k-')
plt.xlabel('Performance')
plt.ylabel('Consommation')
plt.title('Mean Absolute Error = ' + str(round(mae,2)) + ' kg.')

sys.exit(0)



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(results.perfo, results.conso, results.ISA, c=results.ISA)
ax.set_xlabel('Performance')
ax.set_ylabel('Consommation')
ax.set_zlabel('Delta ISA')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(results.perfo, results.conso, results.GW, c=results.GW)
ax.set_xlabel('Performance')
ax.set_ylabel('Consommation')
ax.set_zlabel('GW')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(results.perfo, results.conso, results.ToC, c=results.ToC)
ax.set_xlabel('Performance')
ax.set_ylabel('Consommation')
ax.set_zlabel('ToC')


fig = plt.figure()
diff = np.array(results.perfo - results.conso)
m = round(np.mean(diff),2)
sd = round(np.std(diff),2)

plt.hist(diff)
s = 'Histogram of Performance - Consommation.'
s2 = 'Mean = ' + str(m) + ' Std deviation = ' + str(sd)
plt.title(s)
plt.xlabel(s2)


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(results.ISA, results.GW, results.ToC, c=diff)
ax.set_xlabel('Delta ISA')
ax.set_ylabel('GW')
ax.set_zlabel('ToC')



# x = results.ISA
# y = results.GW
# z = diff

# xi,yi = np.linspace(x.min(), x.max(), 100), np.linspace(y.min(), y.max(), 100)
# xi,yi = np.meshgrid(xi, yi)

# rbf = Rbf(x, y, z, function='linear')
# zi = rbf(xi, yi)

# fig = plt.figure()
# plt.imshow(zi, vmin=z.min(), vmax=z.max(), origin='lower', extent=[x.min(), x.max(), y.min(), y.max()])
# # plt.scatter(x, y, c=z, alpha=.5)
# plt.colorbar()
# plt.xlabel('Delta ISA')
# plt.ylabel('GW')

plt.show()
