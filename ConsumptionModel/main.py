# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from src.Learner import *
from src.GridSearch import *

import time, json
import pandas as pd

import matplotlib.pyplot as plt




# DONNÉES DE AVRIL MAI 2015


if int(sys.argv[1]):

	from src.FeatureExtraction import *

else:
	print 'Loading data'
	DesignMatrix = np.loadtxt('DesignMatrix.csv', delimiter=',')
	Y = np.loadtxt('Consumption.csv', delimiter=',')
	with open('groupsInfos.json', 'r') as f:
		groupsInfos = json.load(f)	






# POUR TESTER
# n_estimators = 10
# n_iter = 1
# threshold=0

ntr = 700
nval = 300
ntest = 100
Xtr, tmp, Ytr, Ytmp = train_test_split(DesignMatrix, Y, train_size = ntr, test_size=nval+ntest)
Xval, Xte, Yval, Yte = train_test_split(tmp, Ytmp, train_size = nval, test_size=ntest)



print '---'
print '---\tComputing importances'
print '---'
from src.FeatureSelection import *
rf_model = RandomForestRegressor(n_estimators=1000, oob_score=1, n_jobs=-1)
rf_model.fit(DesignMatrix,Y)

grImp = Importance(ngroups=len(groupsInfos['nvarGroup']), nvarGroup=groupsInfos['nvarGroup'], groupNames=groupsInfos['Names'], normalize=True)
grImp.run(rf_model, DesignMatrix, Y)
# grImp.run_parallel(rf_model, DesignMatrix, Y, n_iter=5)

# # grImp.plot(m=10)

Dat = numpy.array([(grImp.groupNames[i], grImp.importance[i]) for i in range(len(grImp.groupNames))], dtype=[('name', 'S10'), ('value', 'float64')])
DatOrdered = numpy.sort(Dat, order='value')[::-1]

importanceToPlot = [z[1] for z in DatOrdered][0:20]
namesToPlot = [z[0] for z in DatOrdered][0:20]

# print [(z, round(importanceToPlot[i], 2)) for i,z in enumerate(namesToPlot)]
for i,z in enumerate(namesToPlot):
	print z, round(importanceToPlot[i], 2)



print '\nTest SVR'

start = time.time()
modelSVR = Learner(regressor='SVR', C=1e7, epsilon=1, gamma=1e-7)
maeSVR = modelSVR.KfoldCV(Xval, Yval, K=5)
print str(round(time.time() - start, 2)) + ' seconds'
print 'CV Mean Absolute Error = ' + str(np.mean(maeSVR)) + ' kg.'



print '\nTest ETR'

start = time.time()
modelETR = Learner(regressor='ETR', n_estimators=1000, n_jobs=-1)
maeETR = modelETR.KfoldCV(Xval, Yval, K=5)
print str(round(time.time() - start, 2)) + ' seconds'
print 'CV Mean Absolute Error = ' + str(np.mean(maeETR)) + ' kg.'




print '\nTest RF'

start = time.time()
modelRF = Learner(regressor='RF', n_estimators=1000, n_jobs=-1)
maeRF = modelRF.KfoldCV(Xval, Yval, K=5)
print str(round(time.time() - start, 2)) + ' seconds'
print 'CV Mean Absolute Error = ' + str(np.mean(maeRF)) + ' kg.'




print '\nTest GBR'

start = time.time()
modelGBR = Learner(regressor='GBR', n_estimators=1000)
maeGBR = modelGBR.KfoldCV(Xval, Yval, K=5)
print str(round(time.time() - start, 2)) + ' seconds'
print 'CV Mean Absolute Error = ' + str(np.mean(maeGBR)) + ' kg.'




print '\nTest k-NN'

selected_features_names = ['t_ToC', 'Gw', 'AltToC', 'ALTITUDE', 'CAS']
idx_selected = []
for z in selected_features_names:
	for zz in grImp.idx_output(z):
		idx_selected.append(zz)



print '\t3-NN'
start = time.time()
model_3NN = Learner(regressor='KNN', n_neighbors=3)
# model_3NN.train(Xtr[:,idx_selected], Ytr)
mae3NN = model_3NN.KfoldCV(Xval[:,idx_selected], Yval)
print str(round(time.time() - start, 2)) + ' seconds'
print 'CV Mean Absolute Error = ' + str(np.mean(mae3NN)) + ' kg.'





print '\t10-NN'
start = time.time()
model_10NN = Learner(regressor='KNN', n_neighbors=10)
# model_10NN.train(Xtr[:,idx_selected], Ytr)
mae10NN = model_10NN.KfoldCV(Xval[:,idx_selected], Yval)
print str(round(time.time() - start, 2)) + ' seconds'
print 'CV Mean Absolute Error = ' + str(np.mean(mae10NN)) + ' kg.'




# print '\nTest Combined Learner'
# Machines = {'SVR':modelSVR, 'RF':modelRF, 'ETR':modelETR, 'GBR':modelGBR, '3NN':model_3NN, '10NN':model_10NN}

# print '\tMean'
# start = time.time()
# CLmean = CombinedLearner('mean', Machines)
# maeCLmean = CLmean.KfoldCV(Xval, Yval)
# print str(round(time.time() - start, 2)) + ' seconds'
# print 'CV Mean Absolute Error = ' + str(np.mean(maeCLmean)) + ' kg.'

# # print '\tWeighted Mean'
# # W = np.array([1/np.mean(maeSVR), 1/np.mean(maeETR), 1/np.mean(maeRF), 1/np.mean(maeGBR), 1/np.mean(mae3NN), 1/np.mean(mae10NN)])
# # CLWmean = CombinedLearner('Wmean', Machines, W)
# # maeCLWmean = CLWmean.KfoldCV(Xval, Yval)
# # print 'CV Mean Absolute Error = ' + str(np.mean(maeCLWmean)) + ' kg.'

# print '\tMedian'
# start = time.time()
# CLmedian = CombinedLearner('median', Machines)
# maeCLmedian = CLmedian.KfoldCV(Xval, Yval)
# print str(round(time.time() - start, 2)) + ' seconds'
# print 'CV Mean Absolute Error = ' + str(np.mean(maeCLmedian)) + ' kg.'




# MAE = np.concatenate((	np.array(maeRF).reshape((len(maeRF),1)), np.array(maeETR).reshape((len(maeRF),1)), 
# 						np.array(maeGBR).reshape((len(maeRF),1)), np.array(maeSVR).reshape((len(maeRF),1)),
# 						np.array(mae3NN).reshape((len(maeRF),1)), np.array(mae10NN).reshape((len(maeRF),1)),
# 						np.array(maeCLmean).reshape((len(maeRF),1)), np.array(maeCLWmean).reshape((len(maeRF),1)), 
# 						np.array(maeCLmedian).reshape((len(maeRF),1))), axis=1)

# plt.boxplot(MAE, labels=('RF', 'ETR', 'GBR', 'SVR', '3-NN', '10-NN', 'Mean', 'Wmean', 'Median'))


MAE = np.concatenate((	np.array(maeRF).reshape((len(maeRF),1)), np.array(maeETR).reshape((len(maeRF),1)), 
						np.array(maeGBR).reshape((len(maeRF),1)), np.array(maeSVR).reshape((len(maeRF),1)),
						np.array(mae3NN).reshape((len(maeRF),1)), np.array(mae10NN).reshape((len(maeRF),1))), axis=1)

plt.boxplot(MAE, labels=('RF', 'ETR', 'GBR', 'SVR', '3-NN', '10-NN'))

plt.savefig('compare_methods_tuned_24-9-15.png')
