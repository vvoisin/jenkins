require(COBRA)

machines <- as.matrix(read.table('machines.csv', sep=',', header=FALSE))
machines.names <- c("SVR","RF", "ETR")

xtrain <- as.matrix(read.table('xtrain.csv', sep=',', header=FALSE))
ytrain <- as.matrix(read.table('ytrain.csv', sep=',', header=FALSE))
xtest <- as.matrix(read.table('xtest.csv', sep=',', header=FALSE))
ytest <- as.matrix(read.table('ytest.csv', sep=',', header=FALSE))

res <- COBRA(train.design = xtrain,
           train.responses = ytrain,
           test = xtest,
           machines = machines,
           machines.names = machines.names, plots=TRUE)


# print(cbind(res2$predict,test.responses))
plot(ytest,res$predict,xlab="Responses",ylab="Predictions",pch=3,col=2)
abline(0,1,lty=2)






