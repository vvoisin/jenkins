# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from src.Learner import *
from src.GridSearch import *

import time, json
import pandas as pd

import matplotlib.pyplot as plt




# DONNÉES DE MAI 2015


if int(sys.argv[1]):

	from src.FeatureExtraction import *

else:
	print 'Loading data'
	DesignMatrix = np.loadtxt('DesignMatrix.csv', delimiter=',')
	Y = np.loadtxt('Consumption.csv', delimiter=',')
	with open('groupsInfos.json', 'r') as f:
		groupsInfos = json.load(f)	






# POUR TESTER
# n_estimators = 10
# n_iter = 1
# threshold=0

ntr = 700
nval = 300
ntest = 100
Xtr, tmp, Ytr, Ytmp = train_test_split(DesignMatrix, Y, train_size = ntr, test_size=nval+ntest)
Xval, Xte, Yval, Yte = train_test_split(tmp, Ytmp, train_size = nval, test_size=ntest)



print '---'
print '---\tComputing importances'
print '---'
from src.FeatureSelection import *
rf_model = RandomForestRegressor(n_estimators=10, oob_score=1, n_jobs=-1)
rf_model.fit(DesignMatrix,Y)

grImp = Importance(ngroups=len(groupsInfos['nvarGroup']), nvarGroup=groupsInfos['nvarGroup'], groupNames=groupsInfos['Names'], normalize=True)
grImp.run(rf_model, DesignMatrix, Y)
# grImp.run_parallel(rf_model, DesignMatrix, Y, n_iter=5)

# # grImp.plot(m=10)

Dat = numpy.array([(grImp.groupNames[i], grImp.importance[i]) for i in range(len(grImp.groupNames))], dtype=[('name', 'S10'), ('value', 'float64')])
DatOrdered = numpy.sort(Dat, order='value')[::-1]

importanceToPlot = [z[1] for z in DatOrdered][0:20]
namesToPlot = [z[0] for z in DatOrdered][0:20]

# print [(z, round(importanceToPlot[i], 2)) for i,z in enumerate(namesToPlot)]
for i,z in enumerate(namesToPlot):
	print z, round(importanceToPlot[i], 2)

# print '---'
# print '---\tSelection step'
# print '---'

# print 'nr of groups: ' + str(len(groupsInfos['nvarGroup']))
# pars = {'nvarGroup':groupsInfos['nvarGroup'], 'groupNames':groupsInfos['Names'], 'normalize':True}
# RFsel = RandomForestSelector(selector_type='screening', parameters=pars, RFestimator='RF', n_iter=n_iter, threshold=threshold, doPlot=False, n_estimators=n_estimators)
# start = time.time()
# RFsel.run(Xtr, Ytr)
# print str(round(time.time() - start, 2)) + ' seconds'

# # selected_features_names = RFsel.selected_features_names
# selected_features_names = ['t_ToC', 'Gw', 'AltToC', 'ALTITUDE', 'CAS', 'F-GZHV', 'N11', 'N12', 'F-GZHN', 'NR_LO']

# idx_selected = []
# for z in selected_features_names:
# 	for zz in RFsel.GrImp.idx_output(z):
# 		idx_selected.append(zz)



print '---'
print '---\tTuning step'
print '---'





print '\nTest SVR'

C_range = np.logspace(-2, 10, 13)
gamma_range = np.logspace(-9, 3, 13)
eps_range = [.01, .1, 1]
param_grid = dict(C=C_range, kernel=['rbf'], gamma=gamma_range, epsilon=eps_range)

start = time.time()
bestLearnerSVR, best_params = gridSearch('SVR', Xtr, Ytr, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

# pred, mse, mae = bestLearnerSVR.predict(Xval, Yval)
# print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeSVR = bestLearnerSVR.KfoldCV(Xval, Yval, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeSVR)) + ' kg.'


print '\nTest ETR'

mtry_range = ['auto'] #['auto', 'log2', 'sqrt', 1]
depth_range = [None] # [None, 5]
param_grid = dict(max_features=mtry_range, max_depth=depth_range, n_estimators=[500])

start = time.time()
bestLearnerETR, best_params = gridSearch('ETR', Xtr, Ytr, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

# pred, mse, mae = bestLearnerETR.predict(Xval, Yval)
# print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeETR = bestLearnerETR.KfoldCV(Xval, Yval, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeETR)) + ' kg.'




print '\nTest RF'

mtry_range = ['auto'] #['auto', 'log2', 'sqrt', 1]
depth_range = [None] # [None, 5]
param_grid = dict(max_features=mtry_range, max_depth=depth_range, n_estimators=[500])

start = time.time()
bestLearnerRF, best_params = gridSearch('RF', Xtr, Ytr, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

# pred, mse, mae = bestLearnerRF.predict(Xval, Yval)
# print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeRF = bestLearnerRF.KfoldCV(Xval, Yval, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeRF)) + ' kg.'




print '\nTest GBR'

mtry_range = ['auto'] #['auto', 'log2', 'sqrt', 1]
depth_range = [3]
loss_range = ['ls'] # ['ls', 'lad', 'huber']
param_grid = dict(max_features=mtry_range, max_depth=depth_range, n_estimators=[500], loss=loss_range)

start = time.time()
bestLearnerGBR, best_params = gridSearch('GBR', Xtr, Ytr, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

# pred, mse, mae = bestLearnerGBR.predict(Xval, Yval)
# print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeGBR = bestLearnerGBR.KfoldCV(Xval, Yval, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeGBR)) + ' kg.'




print '\nTest k-NN'

selected_features_names = ['t_ToC', 'Gw', 'AltToC', 'ALTITUDE', 'CAS']
idx_selected = []
for z in selected_features_names:
	for zz in grImp.idx_output(z):
		idx_selected.append(zz)



print '\t3-NN'
model_3NN = Learner(regressor='KNN', n_neighbors=3)
model_3NN.train(Xtr[:,idx_selected], Ytr)
mae3NN = model_3NN.KfoldCV(Xval[:,idx_selected], Yval)
print 'CV Mean Absolute Error = ' + str(np.mean(mae3NN)) + ' kg.'




print '\t10-NN'
model_10NN = Learner(regressor='KNN', n_neighbors=10)
model_10NN.train(Xtr[:,idx_selected], Ytr)
mae10NN = model_10NN.KfoldCV(Xval[:,idx_selected], Yval)
print 'CV Mean Absolute Error = ' + str(np.mean(mae10NN)) + ' kg.'




# print '\nTest Combined Learner'
# Machines = {'SVR':bestLearnerSVR, 'RF':bestLearnerRF, 'ETR':bestLearnerETR, 'GBR':bestLearnerGBR, '3NN':model_3NN, '10NN':model_10NN}
# print '\tMean'
# CLmean = CombinedLearner('mean', Machines)
# maeCLmean = CLmean.KfoldCV(Xval, Yval)
# print 'CV Mean Absolute Error = ' + str(np.mean(maeCLmean)) + ' kg.'

# # print '\tWeighted Mean'
# # W = np.array([1/np.mean(maeSVR), 1/np.mean(maeETR), 1/np.mean(maeRF), 1/np.mean(maeGBR), 1/np.mean(mae3NN), 1/np.mean(mae10NN)])
# # CLWmean = CombinedLearner('Wmean', Machines, W)
# # maeCLWmean = CLWmean.KfoldCV(Xval, Yval)
# # print 'CV Mean Absolute Error = ' + str(np.mean(maeCLWmean)) + ' kg.'

# print '\tMedian'
# CLmedian = CombinedLearner('median', Machines)
# maeCLmedian = CLmedian.KfoldCV(Xval, Yval)
# print 'CV Mean Absolute Error = ' + str(np.mean(maeCLmedian)) + ' kg.'




MAE = np.concatenate((	np.array(maeRF).reshape((len(maeRF),1)), np.array(maeETR).reshape((len(maeRF),1)), 
						np.array(maeGBR).reshape((len(maeRF),1)), np.array(maeSVR).reshape((len(maeRF),1)),
						np.array(mae3NN).reshape((len(maeRF),1)), np.array(mae10NN).reshape((len(maeRF),1))), axis=1)

plt.boxplot(MAE, labels=('RF', 'ETR', 'GBR', 'SVR', '3-NN', '10-NN'))
plt.savefig('compare_methods_tuned_24-9-15.png')
