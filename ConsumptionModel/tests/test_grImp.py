
import numpy as np
from src.FeatureSelection import *
import matplotlib.pyplot as plt

X = np.loadtxt('tests/gaussX.csv', delimiter=',', skiprows=1)
Y = np.loadtxt('tests/gaussY.csv', delimiter=',', skiprows=1)


rf_model = RandomForestRegressor(n_estimators=500, oob_score=1)
rf_model.fit(X,Y)

grImp = Importance(ngroups=3, nvarGroup=[2,1,6], groupNames=['G1', 'G2', 'G3'], normalize=False)
grImp.run_parallel(rf_model, X, Y, n_iter=20)


print grImp.importance_mean




# pars = {'nvarGroup':[2,1,6], 'groupNames':['G1', 'G2', 'G3'], 'normalize':True}
# RFsel = RandomForestSelector(selector_type='screening', parameters=pars, RFestimator='RF', n_iter=20, threshold=threshold, doPlot=False, n_estimators=n_estimators)
# start = time.time()
# RFsel.run(Xtr, Ytr)

# En R avec RFgroove
       # data(toyReg)
       # attach(toyReg)
     
       # rf <- randomForest(x=X,y=Y,keep.forest=TRUE, keep.inbag=TRUE, ntree=500)
       # ngroups <- 3
       # nvarGroup <- c(2,1,6)
       # idxGroup <- 0:8
       # grImp <- varImpGroup(rf, X, ngroups, nvarGroup, idxGroup, NULL, normalize=FALSE )
       # cat("Group importance\n", grImp, "\n")
     
       # detach(toyReg)
