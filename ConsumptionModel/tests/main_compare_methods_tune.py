# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from src.Fpca import *
from src.utils import *
from src.Learner import *
from src.GridSearch import *

import time
import pandas as pd
from sklearn import preprocessing

# PARAMETRES IMPROTANTS
# nr de points dans chaque séries (N=1000)
# 


# DONNÉES DE AVRIL MAI 2015
dataPath = 'data/Pre-processed_data/'
infosPath = 'data/'

if int(sys.argv[1]):

	print 'Pre-processing data'

	Infos = pd.read_csv(infosPath+'FlightInfos.csv', delimiter=',')
	n = Infos.shape[0]

	Regist_dummy = dummy(Infos.Regist)
	From_dummy = dummy(Infos.From)
	To_dummy = dummy(Infos.To)
	Gw = np.array(Infos.GWFL100.tolist()).reshape((n,1))
	AltToC = np.array(Infos.AltToC.tolist()).reshape((n,1))
	NR_LO = np.array(Infos.NR_LO.tolist()).reshape((n,1))
	t_ToC = np.array(Infos.t_ToC.tolist()).reshape((n,1))
	Other = np.concatenate((Regist_dummy[0], From_dummy[0], To_dummy[0], Gw, AltToC, NR_LO, t_ToC), axis=1)
	Other = preprocessing.scale(Other)

	# IMPORT DES VARIABLES ET FPCA. C'EST JUSTE DU NUMÉRIQUE
	FilesVars = os.listdir(dataPath)
	# pars = {}
	DesignMatrix = Other
	List_Fpca = []
	nr_PCs = []
	for f in FilesVars:
		infile = dataPath+f
		print '\tProcessing ' + f
		# tmpDat = pd.read_csv(infile, delimiter=',').T
		tmpDat = np.loadtxt(infile, delimiter=',', skiprows=1).T
		
		# INTEGRER LA NORMALISATION DES SIGNAUX
		fpca = FPCA(normalize(tmpDat), .999, 64)
		fpca.fit()
		DesignMatrix = np.concatenate((DesignMatrix, fpca.PCs), axis = 1)
		List_Fpca.append(fpca)
		nr_PCs.append(fpca.PCs.shape[1])

	nvarGroup = [1]*Other.shape[1] + nr_PCs
	
	Y = Infos.Consumption

	np.savetxt('DesignMatrix.csv', DesignMatrix, delimiter=',')
	np.savetxt('Consumption.csv', Infos.Consumption, delimiter=',')
else:
	print 'Loading data'
	DesignMatrix = np.loadtxt('DesignMatrix.csv', delimiter=',')
	Y = np.loadtxt('Consumption.csv', delimiter=',')


idx = np.where(Y>500)
# plt.hist(Y[idx]); plt.show()
DesignMatrix = DesignMatrix[idx]
Y = Y[idx]




# POUR TESTER
ntr = 1000
nval = 500
ntest = 100
Xtr, tmp, Ytr, Ytmp = train_test_split(DesignMatrix, Y, train_size = ntr, test_size=nval+ntest)
Xval, Xte, Yval, Yte = train_test_split(tmp, Ytmp, train_size = nval, test_size=ntest)



print '\nTest SVR'

C_range = np.logspace(-2, 10, 13)
gamma_range = np.logspace(-9, 3, 13)
eps_range = [.01, .1, 1]
param_grid = dict(C=C_range, kernel=['rbf'], gamma=gamma_range, epsilon=eps_range)

start = time.time()
bestLearnerSVR, best_params = gridSearch('SVR', Xtr, Ytr, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

pred, mse, mae = bestLearnerSVR.predict(Xval, Yval)
print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeSVR = bestLearnerSVR.KfoldCV(Xval, Yval, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeSVR)) + ' kg.'

# plt.plot(Yval, pred, 'o')
# plt.xlabel('Observed')
# plt.ylabel('Predicted')
# plt.title('SVR')
# xline = np.arange(np.min(Yval), np.max(Yval), 100)
# plt.plot(xline, xline, 'k-')
# plt.show()



print '\nTest ETR'

mtry_range = ['auto', 'log2', 'sqrt', 1]
depth_range = [None, 5]
param_grid = dict(max_features=mtry_range, max_depth=depth_range, n_estimators=[500])

start = time.time()
bestLearnerETR, best_params = gridSearch('ETR', Xtr, Ytr, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

pred, mse, mae = bestLearnerETR.predict(Xval, Yval)
print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeETR = bestLearnerETR.KfoldCV(Xval, Yval, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeETR)) + ' kg.'




print '\nTest RF'

mtry_range = ['auto', 'log2', 'sqrt', 1]
depth_range = [None, 5]
param_grid = dict(max_features=mtry_range, max_depth=depth_range, n_estimators=[500])

start = time.time()
bestLearnerRF, best_params = gridSearch('RF', Xtr, Ytr, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

pred, mse, mae = bestLearnerRF.predict(Xval, Yval)
print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeRF = bestLearnerRF.KfoldCV(Xval, Yval, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeRF)) + ' kg.'




print '\nTest GBR'

mtry_range = ['auto', 'log2', 'sqrt', 1]
depth_range = [3]
loss_range = ['ls', 'lad', 'huber']
param_grid = dict(max_features=mtry_range, max_depth=depth_range, n_estimators=[500], loss=loss_range)

start = time.time()
bestLearnerGBR, best_params = gridSearch('GBR', Xtr, Ytr, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

pred, mse, mae = bestLearnerGBR.predict(Xval, Yval)
print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeGBR = bestLearnerGBR.KfoldCV(Xval, Yval, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeGBR)) + ' kg.'




# print '\nTest KNN'

# param_grid = dict(n_neighbors=range(10))

# start = time.time()
# bestLearnerKNN, best_params = gridSearch('KNN', Xtr, Ytr, param_grid=param_grid)
# print str(round(time.time() - start, 2)) + ' seconds'
# print 'Optimized parameters :' + str(best_params)

# print 'Best model test set 1'
# pred, mse, mae = bestLearnerKNN.predict(Xval, Yval)
# print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

# print 'CV error on Xval'
# maeKNN = bestLearnerKNN.KfoldCV(Xval, Yval, K=5)
# print 'CV Mean Absolute Error = ' + str(np.mean(maeSVR)) + ' kg.'


MAE = np.concatenate((	np.array(maeRF).reshape((len(maeRF),1)), np.array(maeETR).reshape((len(maeRF),1)), 
						np.array(maeGBR).reshape((len(maeRF),1)), np.array(maeSVR).reshape((len(maeRF),1))), axis=1)

plt.boxplot(MAE, labels=('RF', 'ETR', 'GBR', 'SVR'))
plt.savefig('compare_methods_tuned.png')
