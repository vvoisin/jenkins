from utils import *



############# TESTS ##############

def test_level_off():

	dataPath = '../Data/Aout2015/vols-sans-consignes-FMC/'
	Files = os.listdir(dataPath)[0:100]
	
	# f = Files[int(sys.argv[2])]
	# f = 'F-GZHB-0807170.csv'
	# f = 'F-GZHB-0808070.csv'
	f = 'F-GZHG-0819050.csv'
	infile = dataPath + f

	tmpDat = pd.read_csv(dataPath + f, delimiter=';')
	
	N = len(tmpDat.index)
	tmpDat = tmpDat.iloc[1:N]


	Alt = np.asarray([float(z) for z in tmpDat.ALTITUDE])
	AltToC = np.max(Alt)
	idxFL100 = np.where(Alt >= 10000)[0][0]
	idxToC = np.where(Alt >= AltToC)[0][0]

	a,b,c,d = level_off_detection(Alt[idxFL100:idxToC+1], 300, int(sys.argv[1]))
	print 'nrLO = ' + str(a)


test_level_off()


def test_extractOnes():
	try:
		print 'Test0'
		print extractOnes(np.asarray([2,1,1,1,36]))
	except:
		print 'Test0 fails'
	try:
		print 'Test1'
		print extractOnes(np.asarray([2,1,1,1]))
	except:
		print 'Test1 fails'
	try:
		print 'Test2'
		print extractOnes(np.asarray([1,1,1,2]))
	except:
		print 'Test2 fails'
	try:
		print 'Test3'
		print extractOnes(np.asarray([1,1,1,2,1,1]))
	except:
		print 'Test3 fails'
	try:
		print 'Test4'
		print extractOnes(np.asarray([1,1,1,2,1,1,1,1,1,2,1,1]))
	except:
		print 'Test4 fails'
	try:
		print 'Test5'
		print extractOnes(np.asarray([1,1,1]))
	except:
		print 'Test5 fails'
	try:
		print 'Test6'
		print extractOnes(np.asarray([11,11,11]))
	except:
		print 'Test6 fails'

# test_extractOnes()