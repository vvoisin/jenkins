import numpy as np
import pandas as pd
from sklearn import preprocessing
import sys, time
from src.Fpca import *
from src.utils import *


from sklearn.neighbors import NearestNeighbors
import matplotlib.pyplot as plt

Alt = normalize(np.loadtxt('data/Pre-processed_data/ALTITUDE.csv', delimiter=',', skiprows=1).T)
Cas = normalize(np.loadtxt('data/Pre-processed_data/CAS.csv', delimiter=',', skiprows=1).T)

n = 10


if False:
	print '1- kNN direct'
	x = Alt
	
	start = time.time()
	
	knn = NearestNeighbors(n_neighbors=3)
	knn.fit(x)
	distances, indices = knn.kneighbors()
	
	print str(round(time.time() - start, 2)) + ' seconds'

	for i in range(n):
		print 'Flight ' + str(i)
		print '\tDistances to k-NN = ' + str(distances[i])
		print '\tMean distance = ' + str(np.mean(distances[i]))
		plt.plot(x[i], '-')
		plt.plot(x[indices[i][0]], '-')
		plt.plot(x[indices[i][1]], '-')
		plt.plot(x[indices[i][2]], '-')
		plt.show()

	# sys.exit(0)



print '2- kNN sur PCs'

start = time.time()

fpca = FPCA(Alt, .99, 64)
fpca.fit()
PCs_Alt = fpca.PCs

fpca = FPCA(Cas, .999, 64)
fpca.fit()
PCs_Cas = fpca.PCs

Design = np.concatenate((PCs_Alt, PCs_Cas), axis=1)


knn2 = NearestNeighbors(n_neighbors=3)
knn2.fit(Design)
distances, indices = knn2.kneighbors()

print str(round(time.time() - start, 2)) + ' seconds'



for i in range(n):
	print 'Flight ' + str(i)
	print '\tDistances to k-NN = ' + str(distances[i])
	print '\tMean distance = ' + str(np.mean(distances[i]))
	
	plt.subplot(211)
	plt.plot(Alt[i], '-')
	plt.plot(Alt[indices[i][0]], '-')
	plt.plot(Alt[indices[i][1]], '-')
	plt.plot(Alt[indices[i][2]], '-')

	plt.subplot(212)
	plt.plot(Cas[i], '-')
	plt.plot(Cas[indices[i][0]], '-')
	plt.plot(Cas[indices[i][1]], '-')
	plt.plot(Cas[indices[i][2]], '-')
	plt.show()
