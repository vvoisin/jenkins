# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from src.Fpca import *
from src.utils import *
from src.Learner import *
from src.GridSearch import *
from src.FeatureSelection import *

import time, json
import pandas as pd
from sklearn import preprocessing



# PARAMETRES IMPROTANTS
# nr de points dans chaque séries (N=1000)
# 


# DONNÉES DE AVRIL MAI 2015
dataPath = 'data/Pre-processed_data/'
infosPath = 'data/'


if int(sys.argv[1]):

	print 'Pre-processing data'

	Infos = pd.read_csv(infosPath+'FlightInfos.csv', delimiter=',')
	n = Infos.shape[0]

	Regist_dummy = dummy(Infos.Regist)
	From_dummy = dummy(Infos.From)
	# To_dummy = dummy(Infos.To) AU LIEU DE METTRE TO, METTRE LA DISTANCE ENTRE LES DEUX
	Gw = np.array(Infos.GWFL100.tolist()).reshape((n,1))
	AltToC = np.array(Infos.AltToC.tolist()).reshape((n,1))
	NR_LO = np.array(Infos.NR_LO.tolist()).reshape((n,1))
	t_ToC = np.array(Infos.t_ToC.tolist()).reshape((n,1))
	Other = np.concatenate((Regist_dummy[0], From_dummy[0], Gw, AltToC, NR_LO, t_ToC), axis=1) #, To_dummy[0]
	Other = preprocessing.scale(Other)

	NamesOther = list(Regist_dummy[1]) + list(From_dummy[1]) + ['Gw', 'AltToC', 'NR_LO', 't_ToC'] # + list(To_dummy[1])

	# IMPORT DES VARIABLES ET FPCA. C'EST JUSTE DU NUMÉRIQUE
	FilesVars = os.listdir(dataPath)
	DesignMatrix = Other
	List_Fpca = []
	nr_PCs = []
	for f in FilesVars:
		infile = dataPath+f
		print '\tProcessing ' + f
		tmpDat = np.loadtxt(infile, delimiter=',', skiprows=1).T
		
		# INTEGRER LA NORMALISATION DES SIGNAUX
		fpca = FPCA(normalize(tmpDat), .999, 64)
		fpca.fit()
		DesignMatrix = np.concatenate((DesignMatrix, fpca.PCs), axis = 1)
		List_Fpca.append(fpca)
		nr_PCs.append(fpca.PCs.shape[1])

	Names = [s.split('.')[0] for s in FilesVars]
	Names_all = NamesOther + Names
	nvarGroup_list = [1]*Other.shape[1] + nr_PCs
	nvarGroup = {Names_all[i]:z for i,z in enumerate(nvarGroup_list)}
	groupsInfos = {'Names':Names_all, 'nvarGroup':nvarGroup_list}

	Y = Infos.Consumption

	np.savetxt('DesignMatrix.csv', DesignMatrix, delimiter=',')
	np.savetxt('Consumption.csv', Infos.Consumption, delimiter=',')

	with open('groupsInfos.json', 'w') as f:
		json.dump(groupsInfos, f)

else:
	print 'Loading data'
	DesignMatrix = np.loadtxt('DesignMatrix.csv', delimiter=',')
	Y = np.loadtxt('Consumption.csv', delimiter=',')
	with open('groupsInfos.json', 'r') as f:
		groupsInfos = json.load(f)	

idx = np.where(Y>500)[0]
# plt.hist(Y[idx]); plt.show()
DesignMatrix = DesignMatrix[idx]
Y = Y[idx]


# POUR TESTER
n_estimators = 10
n_iter = 1
threshold=0

ntr = 1000
nval = 500
ntest = 100
Xtr, tmp, Ytr, Ytmp = train_test_split(DesignMatrix, Y, train_size = ntr, test_size=nval+ntest)
Xval, Xte, Yval, Yte = train_test_split(tmp, Ytmp, train_size = nval, test_size=ntest)

print 'nr of groups: ' + str(len(groupsInfos['nvarGroup']))




print '---'
print '---\tSelection step'
print '---'

pars = {'nvarGroup':groupsInfos['nvarGroup'], 'groupNames':groupsInfos['Names'], 'normalize':True}
RFsel = RandomForestSelector(selector_type='screening', parameters=pars, RFestimator='RF', n_iter=n_iter, threshold=threshold, doPlot=False, n_estimators=n_estimators)
start = time.time()
RFsel.run(Xtr, Ytr)
print str(round(time.time() - start, 2)) + ' seconds'

# selected_features_names = RFsel.selected_features_names
selected_features_names = ['t_ToC', 'Gw', 'AltToC', 'ALTITUDE', 'CAS', 'F-GZHV', 'N11', 'N12', 'F-GZHN', 'NR_LO']

print '---'
print '---\tTuning step'
print '---'

idx_selected = []
for z in selected_features_names:
	for zz in RFsel.GrImp.idx_output(z):
		idx_selected.append(zz)

# Xval = Xval[:,idx_selected]
# Xte = Xte[:,idx_selected]




""" juste un model + error avant et apres selection """

print '\nTest SVR'

print 'KFold before selection'
model = Learner(regressor='SVR', C=100000, gamma=.001, epsilon=1)
print np.mean(model.KfoldCV(Xval, Yval, verbose=True))


print 'KFold after selection'
model = Learner(regressor='SVR', C=100000, gamma=.001, epsilon=1)
print np.mean(model.KfoldCV(Xval[:,idx_selected], Yval, verbose=True))




# print '\nTest RF'

# print 'KFold before selection'
# model = Learner(regressor='RF', n_estimators=n_estimators, n_jobs=-1)
# print np.mean(model.KfoldCV(Xval, Yval))

# print 'KFold after selection'
# model = Learner(regressor='RF', n_estimators=n_estimators, n_jobs=-1)
# print np.mean(model.KfoldCV(Xval[:,idx_selected], Yval))





sys.exit(0)





C_range = np.logspace(-2, 10, 13)
gamma_range = np.logspace(-9, 3, 13)
eps_range = [.01, .1, 1]
param_grid = dict(C=C_range, kernel=['rbf'], gamma=gamma_range, epsilon=eps_range)

start = time.time()
bestLearnerSVR, best_params = gridSearch('SVR', Xval, Yval, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

pred, mse, mae = bestLearnerSVR.predict(Xte, Yte)
print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeSVR = bestLearnerSVR.KfoldCV(Xte, Yte, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeSVR)) + ' kg.'

sys.exit(0)


print '\nTest ETR'

mtry_range = ['auto', 'log2', 'sqrt', 1]
depth_range = [None, 5]
param_grid = dict(max_features=mtry_range, max_depth=depth_range, n_estimators=[1000])

start = time.time()
bestLearnerETR, best_params = gridSearch('ETR', Xval, Yval, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

pred, mse, mae = bestLearnerETR.predict(Xte, Yte)
print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeETR = bestLearnerETR.KfoldCV(Xte, Yte, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeETR)) + ' kg.'




print '\nTest RF'

mtry_range = ['auto', 'log2', 'sqrt', 1]
depth_range = [None, 5]
param_grid = dict(max_features=mtry_range, max_depth=depth_range, n_estimators=[1000])

start = time.time()
bestLearnerRF, best_params = gridSearch('RF', Xval, Yval, param_grid=param_grid)
print str(round(time.time() - start, 2)) + ' seconds'
print 'Optimized parameters :' + str(best_params)

pred, mse, mae = bestLearnerRF.predict(Xte, Yte)
print 'Mean Absolute Error = ' + str(np.mean(mae)) + ' kg.'

maeRF = bestLearnerRF.KfoldCV(Xte, Yte, K=5)
print 'CV Mean Absolute Error = ' + str(np.mean(maeRF)) + ' kg.'


