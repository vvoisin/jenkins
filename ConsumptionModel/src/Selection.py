"""!@module Launch the variable selection process, builds mean profiles and saves usefull results for the prediction.
"""

from Flight_Scanner_Python.learning.splitting_sample import split_sample_balanced, split_sample
from Flight_Scanner_Python.learning.RandomForest import RandomForest
from Flight_Scanner_Python.imports import *

class Selection:
	"""!@brief Object that contains the caracteristics of one learning, and the main results.
	"""

	def __init__(self, n_iteration, n_estimators, nb_vars_selected, percent_selection, decision_type, risk, percentile_mean, percentile_bound, f):
		"""!@brief Instantiation. 

		@param n_iteration : int. Defines how many times we launch the method in parallel.
		@param n_estimators : int. Number of the trees in a forest.
		@param nb_vars_selected : int. Number of variables the most selected we want to return.
		@param percent_selection : float. Boundary of the percentage. If there are variables (after the nb_vars_selected returned) that are selected with a higher percentage, they are printed.
		@param decision_type : string. Takes 'classifier' for a classification, 'regressor' for a regression.
		@param risk : string. Defines the risk of the learning. Takes 'HARD_LANDING', 'LONG_LANDING', 'SHORT_LANDING'.
		"""
		## file where commentaries are written
		self.f = f
		## int. Defines how many times we launch the method in parallel.
		self.n_iteration = n_iteration
		## int. Number of the trees in a forest.
		self.n_estimators = n_estimators
		## int. Number of variables the most selected we want to return.
		self.nb_vars_selected = nb_vars_selected
		## float. Boundary of the percentage. If there are variables (after the nb_vars_selected returned) that are selected with a higher percentage, they are printed.
		self.percent_selection = percent_selection
		## string. Takes 'classifier' for a classification, 'regressor' for a regression.
		self.decision_type = decision_type
		## string. Defines the risk of the learning. Takes 'HARD_LANDING', 'LONG_LANDING', 'SHORT_LANDING'.
		self.risk = risk
		## int. Determines the flights we consider to build the type profile.
		self.percentile_mean = percentile_mean
		## int. Determines the upper bound and lower bound around the type profile.
		self.percentile_bound = percentile_bound
		## list of dictionaries. Each dictionary has the results of one selection.
		self.resultSelection = []
		## numpy array. Average of the error returned by selection.
		self.erreur_mean = None
		## dict. For all variables, counts how many time they are selected in the minimum error.
		self.counting = None
		## numpy array. Average of the prediction returned by selection.
		self.names_err_min_list = None
		## List of list of strings. Names of the selected variables of the minimum error for each instance.
		self.predict = None
		## list of float. Percentages of the n most selected variables.
		self.counting_ord = None
		## list of string. Names of the n most selected variables.
		self.variable_names_selected = None
		## list of numpy arrays. Values of the selected variables of the "best" flights.
		self.dataRef_list = None
		## list of numpy arrays. Averages of each flight variable on the "best" flights.
		self.meanProfile_list = None
		## list of numpy arrays. Upper bounds of the type profile for each variable.
		self.upperBound_list = None
		## list of numpy arrays. Lower bounds of the type profile for each variable.
		self.lowerBound_list = None
		## json containing types profiles
		self.json_profileType = None


	def launch(self, X, y, variable_names, n_coeff_list, id_coeur):
		"""!@brief Method called in parallel that determines the variables of minimum error, these variables are used to construct a forest (which later will be called for the prediction).

		@param X : numpy array. FPCA coefficients of the flight variables.
		@param y : numpy array. Target values.
		@param variable_names : list of strings. Names of all the flight variables.
		@param n_coeff_list : list in int. Number of coefficient for each variable.
		@param id_coeur : int. The identification of the computing in parallel.
		""" 
		from Flight_Scanner_Python.learning.RandomForest import RandomForest

		# get the attributes
		risk = self.risk
		n_estimators = self.n_estimators
		decision_type = self.decision_type
		results = collections.defaultdict(lambda: None)

		nb_variables = len(variable_names)
		nb_vol = X.shape[0]
		erreur_nb_vars = numpy.empty(nb_variables)
		predict = numpy.zeros(nb_vol)
		erreur_val = 0 # erreur oob commise qu on garde en memoire dans fichier txt erreur val

		if decision_type == 'classifier':
		    X_split, y_split, idx_split = split_sample_balanced(X, y, 3)
		else:
		    X_split, y_split, idx_split = split_sample(X, y, 3)
		X_tr = X_split[0]
		y_tr = y_split[0]
		idx_tr = idx_split[0]
		X_te = X_split[1]
		y_te = y_split[1]
		idx_te = idx_split[1]
		X_val = X_split[2]
		y_val = y_split[2]
		idx_val = idx_split[2]

		# renvoie erreur classe RamdomForest
		names = list(variable_names)
		coeff = list(n_coeff_list)
		X_tr_cop = numpy.copy(X_tr)
		X_te_cop = numpy.copy(X_te)
		for i in range(nb_variables):
		    # detection
		    Forest = RandomForest(X_tr_cop, y_tr, names, n_estimators, coeff, decision_type)
		    if decision_type == 'classifier':
		        erreur, conf = Forest.error(X_te_cop, y_te)
		    else:
		        erreur = Forest.error(X_te_cop, y_te)
		    erreur_nb_vars[nb_variables - 1 - i] = erreur
		    I = Forest.importance_forest_all()
		    I_min = min(I)
		    index_I_min = I.index(I_min)
		    var_name_I_min = names[index_I_min]
		    if erreur == numpy.min(erreur_nb_vars[nb_variables - 1 - i:]):
		        names_err_min = list(names)
		        if decision_type == 'classifier':
		            confusion = conf
		    # elimination
		    features_id = Forest.idx_output(var_name_I_min)
		    X_tr_cop = numpy.delete(X_tr_cop, features_id, 1)
		    X_te_cop = numpy.delete(X_te_cop, features_id, 1)
		    name_err_min = names[index_I_min]
		    coefficient = coeff[index_I_min]
		    names.remove(name_err_min)
		    coeff.remove(coefficient)

		# on construit les samples de prediction
		X_fit_predict = numpy.concatenate((X_tr, X_te), axis = 0)
		y_fit_predict = numpy.concatenate((y_tr, y_te), axis = 0)
		X_val_predict = numpy.copy(X_val)
		names = list(variable_names)
		coeff = list(n_coeff_list)

		n_coeff = 0
		for name in variable_names:
		    try:
		        names_err_min.index(name)
		    except ValueError:
		        name_idx = variable_names.index(name)
		        n_coeff += n_coeff_list[name_idx]
		        # calcul des indices a enlever
		        idx = []
		        index_variable_name = names.index(name)
		        for i in range(coeff[index_variable_name]):
					bound = sum(coeff[:index_variable_name])
					idx.append(bound + i)
		        # on enleve les indices des variables qui ne sont pas selectionnees pour le minimum d erreur
		        X_fit_predict = numpy.delete(X_fit_predict, idx, 1)
		        X_val_predict = numpy.delete(X_val_predict, idx, 1)
		        names.remove(name)
		        coeff.remove(coeff[index_variable_name])

		# test sur elimination
		if n_coeff != (X_tr.shape[1] - X_fit_predict.shape[1]):
		    self.f.write('erreur dans la construction de X_fit_predict (algo_selection)\n')
		    sys.exit(1)

		Forest_Predict = RandomForest(X_fit_predict, y_fit_predict, names_err_min, n_estimators, n_coeff_list, decision_type)
		forest_predict = Forest_Predict.forest
		if decision_type == 'classifier':
		    erreur_val, confusion = Forest_Predict.error(X_val_predict, y_val)
		else:
		    erreur_val = Forest_Predict.error(X_val_predict, y_val)

		# save forest
		with open(risk+'/Forests/'+risk+'_'+str(id_coeur), 'wb') as f:
			cPickle.dump(forest_predict, f)

		if decision_type == 'classifier':
		    predict[idx_val] += forest_predict.predict_proba(X_val_predict)[:, 1]
		else:
		    predict[idx_val] += forest_predict.predict(X_val_predict)

		# return
		if decision_type == 'classifier':    
			return erreur_nb_vars, predict, idx_val, names_err_min, id_coeur, erreur_val, confusion
		else:
			return erreur_nb_vars, predict, idx_val, names_err_min, id_coeur, erreur_val


	def launch_parallel(self, X, y, variable_names, n_coeff_list):
		"""!@brief Method that calls in parallel the launch function.

		@param X : numpy array. Wavelets coefficients of the flight variables.
		@param y : numpy array. Target values.
		@param variable_names : list of strings. Names of all the flight variables.
		@param n_estimators : int. Number of the trees in a forest.
		@param n_coeff_list : int. Number of wavelet coefficients for one variable (not for variable point).
    	"""
		# get the attributes
		decision_type = self.decision_type
		n_estimators = self.n_estimators
		risk = self.risk
		n_iteration = self.n_iteration

		nb_variables = len(variable_names)
		nb_vol = X.shape[0]

		counting = defaultdict(lambda: None) # pour une clef "nom d une variable", renvoie le nombre d apparition dans le cas erreur minimale 
		erreur = np.empty((n_iteration, nb_variables)) 
		error_val = np.empty(n_iteration)
		predict_mean = np.zeros(nb_vol)
		names_err_min_list = [] # liste des noms de variables selectionnees pour la foret du minimum d erreur
		for i in range(n_iteration):
		    names_err_min_list.append([])
		count = np.zeros(nb_vol) # compte combien de fois un vol est dans X_val (pour faire la moyenne des predicts in fine)

		ppservers = () # liste des coeurs avec lesquels on se connectera
		job_server = pp.Server(ppservers = ppservers) # cree des job_server avec le nombre de coeurs disponibles
		ncpus = job_server.get_ncpus()
		job_server.set_ncpus(ncpus)
		modules = ('numpy', 'cPickle', 'sys', 'collections')
		depfuncs = (split_sample_balanced, split_sample)

		jobs = []
		for i in range(n_iteration):
		    jobs.append(job_server.submit(func=self.launch, args=(X, y, variable_names, n_coeff_list, i), modules=modules, depfuncs=depfuncs))

	    # recuperation des jobs
		for job in jobs:
			results = defaultdict(lambda: None)
			if decision_type == 'classifier':
				results['erreur_nb_vars'], results['predict'], results['idx_val'], results['names_err_min'], results['id_coeur'], results['erreur_val'], results['confusion'] = job()
			else:
				results['erreur_nb_vars'], results['predict'], results['idx_val'], results['names_err_min'], results['id_coeur'], results['erreur_val'] = job()
			self.resultSelection.append(results)
			erreur[results['id_coeur'], :] = results['erreur_nb_vars']
			error_val[results['id_coeur']] = results['erreur_val']
			count[results['idx_val']] += 1.
			names_err_min_list[results['id_coeur']] = results['names_err_min']
			predict_mean += results['predict']
			for name in results['names_err_min']:
				try:
					counting[name] += 1
				except TypeError:
					counting[name] = 1


		# moyenne des erreurs + pedict 
		erreur_mean = np.mean(erreur, axis = 0)
		predict_mean = predict_mean / count

		# return
		self.erreur_mean = erreur_mean
		self.counting = counting
		self.names_err_min_list = names_err_min_list
		self.predict = predict_mean


	def most_selected_var(self, variable_names):
		"""!@brief Returns the n variable names the most selected for the minimum error and their selection percentage.

		@param variable_names : list of strings. Names of all the flight variables.

		@return counting_ord_n : list of float. Percentages of the n most selected variables.
		@return names_var_n : list of string. Names of the n most selected variables.
		"""
		# get the attributes
		counting_dic = self.counting
		n = self.nb_vars_selected
		bound = self.percent_selection
		n_iteration = self.n_iteration

		counting = copy(counting_dic)
		nb_variables = len(variable_names)
		names_var_ord = []
		counting_ord = []
		for i in range(nb_variables):
		    count_max = max(counting.values())
		    names_var_ord.append(str(counting.keys()[counting.values().index(count_max)]))
		    counting_ord.append(count_max / float(n_iteration))
		    counting[str(counting.keys()[counting.values().index(count_max)])] = 0
		if counting_ord[n+1]>=bound:
		    test = 1
		    self.f.write("Warning, these variables are mostly selected:\n")
		else:
		    test = 0
		i = 1
		while test==1:
		    if counting_ord[n+i]>=bound:
		        self.f.write(str((names_var_ord[n+i], counting_ord[n+i])) + '\n')
		    else:
		        test = 0
		    i += 1
		counting_ord_n = counting_ord[:n]
		names_var_n = names_var_ord[:n]

		# return
		self.counting_ord = counting_ord_n
		self.variable_names_selected = names_var_n


	def profile_type(self, json_learning, variable_names_points, curves, P, nb_t):
		"""!@brief Builds and save the json file for the profiles type.

		@param json_learning : json. Json object of the learning.
		@param variable_names_points : list of string. Names of the flight variables that are not variables points. Takes None if there is no variable point.
		@param curves : numpy array. Values of flight variables for each flight.
		@param P : numpy array. Values of variables points for each flight. Takes None if there is no.
		@param nb_t : int. Determines how many values we have for each curve.
		"""
		# get attributes
		risk = self.risk
		predict = self.predict
		variable_names_selected = self.variable_names_selected
		percentile_mean = self.percentile_mean
		percentile_bound = self.percentile_bound


		##################################
		# CONSTRUCTIONS DES PROFILES TYPES
		##################################
		# profile type, consideration du premier quartile des courbes de vol reelles
		nb_vol_r = P.shape[0]
		predict_r = predict[:nb_vol_r]
		q25 = np.percentile(predict_r, percentile_mean)
		indices_q25 = predict_r <= q25
		nb_vol_q25 = np.sum(indices_q25)

		dataRef_list = []
		meanProfile_list = []
		upperBound_list = []
		lowerBound_list = []
		i = 0
		for name in variable_names_selected:
		    # detection si name est une variable point
		    self.f.write('ligne 338 modification a faire une fois qu on a les unites\n')
		    # type_var = json_learning['parametersType'][name][0]
		    type_var = json_learning['parametersType'][name]

		    if type_var == 'DISCRETE' or type_var == 'CONTINUE':
		        idx = variable_names_points.index(name)

		        dataRef_list.append( P[indices_q25, idx] )
		        # detection si la variable est continue ou discrete
		        if type_var == 'DISCRETE':
		            # variable discrete, on comparera la valeur du vol par rapport a un histogramme, donc ici le mean prend l ensemble des valeurs
		            meanProfile_list.append(dataRef_list[i])
		            # par defaut on remplit par 0
		            upperBound_list.append(0)
		            lowerBound_list.append(0)
		        else:
		            # variable continue, on comparera la valeur du vol par rapport a la densite, le mean prend la densite 
		            dist_space = np.linspace( min(dataRef_list[i]), max(dataRef_list[i]), nb_vol_q25)
		            density = gaussian_kde(dataRef_list[i])
		            meanProfile_list.append([dist_space, density(dist_space)]) # plt.plot( dist_space, kde(dist_space) )
		            # par defaut on remplit par 0
		            upperBound_list.append(0)
		            lowerBound_list.append(0)
		    else:
		        idx = variable_names_selected.index(name)
		        dataRef_list.append( curves[:, idx].reshape(nb_vol_r, nb_t)[indices_q25, :] )
		        meanProfile_list.append(np.mean(dataRef_list[i], axis = 0))
		        upperBound = np.empty(nb_t)
		        lowerBound = np.empty(nb_t)
		        for t in range(nb_t):
		            upperBound[t] = np.percentile(dataRef_list[i][:, t], percentile_bound)
		            lowerBound[t] = np.percentile(dataRef_list[i][:, t], 100-percentile_bound)
		        upperBound_list.append(upperBound)
		        lowerBound_list.append(lowerBound)
		    i += 1

		self.dataRef_list = dataRef_list
		self.meanProfile_list = meanProfile_list
		self.upperBound_list = upperBound_list
		self.lowerBound_list = lowerBound_list

		#######################
		# CONSTRUCTION DU JSON
		#######################
		json_profileType = {}
		json_profileType['parameters'] = {}
		self.f.write('frequence egale a une seconde\n')
		mask = range(0, nb_t, 1) # on a les donnees sur 31 secondes 8Hz, on ne considere que les donnees a chaque seconde
		i = 0
		for name in variable_names_selected:
		    self.f.write('attention a l unite ligne 525 after_learning\n')
		    unit = 'kg'
		    type_var = json_learning['parametersType'][name]
		    json_profileType['parameters'][name] = {}
		    json_profileType['parameters'][name]["type"] = type_var
		    json_profileType['parameters'][name]["unit"] = unit
		    if type_var == 'CURVE':
		        json_profileType['parameters'][name]["vars"] = list(meanProfile_list[i][mask])
		        boundary = np.empty((len(mask), 2))
		        boundary[:,0] = lowerBound_list[i][mask]
		        boundary[:,1] = upperBound_list[i][mask]
		        json_profileType['parameters'][name]["data"] = boundary.tolist()
		    else:
		        if type_var == 'DISCRETE':
		            precision = 1000
		            counts, bins = np.histogram(meanProfile_list[i],  bins = precision*(max(meanProfile_list[i]-min(meanProfile_list[i]))))
		            norm = sum(counts)
		            counts = counts / float(norm)
		            bins = bins[counts != 0]
		            counts = counts[counts != 0]
		            data = np.empty((len(bins), 2))
		            data[:, 0] = bins
		            data[:, 1] = counts
		            json_profileType['parameters'][name]["data"] = data.tolist()
		        else:
		            data = np.empty((len(meanProfile_list[i][0]), 2))
		            data[:,0] = meanProfile_list[i][0]
		            data[:,1] = meanProfile_list[i][1]
		            json_profileType['parameters'][name]["data"] = data.tolist()
		    i += 1
		with open(risk+'/Results/profilType.json', 'w') as outfile:
		    json.dump(json_profileType, outfile)

		json_profileType = json.dumps(json_profileType)

		#return
		self.json_profileType = json.dumps(json_profileType)

	def serialize(self, n_coeff_list, variable_names, variable_names_points, fpca_list, nb_t, meanNorm_list):
		"""!@brief Saves all elements used for prediction.

		@param n_coeff_list : list in int. Number of coefficient for each variable.
		@param variable_names : list of strings. Names of all the flight variables.
		@param variable_names_points : list of strings. Names of the point flight variables.
		@param fpca_list : list of FPCA objects (built from the curves). Usefull for projections of new curves.
		@param nb_t : int. Number of values of one curve.
		@param meanNorm_list : list of float. Average of the norm2 along the new coefficients of one variable.
		"""
		risk = self.risk
		parametersFORprediction = {}

		project_list = []
		for i in range(len(fpca_list)):
			project_list.append(dill.dumps(fpca_list[i].project))
		with open(risk+'/Results/project_listFORprediction', 'wb') as f:
			pickle.dump(project_list, f)
		# parametersFORprediction['project_list'] = project_list
		parametersFORprediction['names_err_min_list'] = self.names_err_min_list
		parametersFORprediction['variable_names_selected'] = self.variable_names_selected
		parametersFORprediction['variable_names'] = variable_names
		parametersFORprediction['variable_names_points'] = variable_names_points
		parametersFORprediction['n_coeff_list'] = n_coeff_list
		parametersFORprediction['n_iteration'] = self.n_iteration
		parametersFORprediction['nb_t'] = nb_t
		with open(risk+'/Results/parametersFORprediction.json', 'w') as outfile:
			json.dump(parametersFORprediction, outfile)
		with open(risk+'/Results/meanNorm_list', 'wb') as f:
			pickle.dump(meanNorm_list, f)
		# sauvegarde des profils types
		with open(risk+'/Results/dataRef_list', 'wb') as f:
			pickle.dump(self.dataRef_list, f)
		with open(risk+'/Results/meanProfile_list', 'wb') as f:
			pickle.dump(self.meanProfile_list, f)
		with open(risk+'/Results/upperBound_list', 'wb') as f:
			pickle.dump(self.upperBound_list, f)
		with open(risk+'/Results/lowerBound_list', 'wb') as f:
			pickle.dump(self.lowerBound_list, f)


