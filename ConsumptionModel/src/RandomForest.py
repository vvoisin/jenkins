"""!@module RandomForest class.
"""

from sklearn.ensemble import *
from sklearn.metrics import confusion_matrix
import numpy

class RandomForest:
    """!@brief Object that can computes important tools along the learning such as the importance of a flight variable and out of bag error on the forest.
    """
    
    def __init__(self, X, y, variable_names, n_estimators, n_coeff_list, decision_type):
        """!@brief Instantiation. 
        
        @param X : numpy array. FPCA coefficients of the flight variables.
        @param y : numpy array. Target values.
        @param variable_names : list of strings. Names of all the flight variables.
        @param n_estimators : int. Number of the trees in a forest.
        @param n_coeff_list : int. Number of wavelet coefficients for one variable (not for variable point).
        @param decision_type : string. Takes 'classifier' for a classification, 'regressor' for a regression.
        """
        ## numpy array, target values.        
        self.y = y
        ## int, number of wavelets coefficients.
        self.n_coeff_list = n_coeff_list
        ## string, takes 'classifier' for a classification, 'regressor' for a regression.
        self.decision_type = decision_type
        ## numpy array, wavelets coefficients of the flight variables.
        self.X = X
        ## list of strings, names of all the flight variables.
        self.variable_names = variable_names
        if decision_type == 'classifier':
            forest = RandomForestClassifier(n_estimators, oob_score = 1)
        else:
            forest = RandomForestRegressor(n_estimators, oob_score = 1)
        forest.fit(X, y)
        ## forest object of sklearn
        self.forest = forest
        
    def idx_output(self, name):
        """!@brief Computes the indice(s) of the flight variable given in argument.

        @param name : string. Name of the variable.
        
        @return list of X-indices of the variable.
        """
        idx = []
        n_coeff_list = self.n_coeff_list
        variable_names = self.variable_names
        index_variable_name = variable_names.index(name)
        for i in range(n_coeff_list[index_variable_name]):
            bound = sum(n_coeff_list[:index_variable_name])
            idx.append(bound + i)
        return idx
    

    def random_permutation(self, x, indices):
        """!@brief Computes the numpy array argument that is switched on the lignes given in argument.

        @param x : numpy array. Array that we want to permute.
        @param indices : numpy array of booleans : 1 if the line is selection, 0 if not.
        
        @return numpy array. The switched array.
        """
        x_perm = numpy.copy(x)
        # creation indices_id
        indices_id = []
        for i in range(len(indices)):
            if indices[i] == 0:
                indices_id.append(i)
        indices_id_perm = numpy.random.permutation(indices_id)
        while (indices_id_perm == indices_id).all():
            indices_id_perm = numpy.random.permutation(indices_id)
        try:
            x_perm[indices_id, :] = x[indices_id_perm, :]
        except IndexError:
            x_perm[indices_id] = x[indices_id_perm]
        return x_perm
        
    def perm_X(self, indices, features_id):
        """!@brief Random permutation of the attribute X, on the colomns and lines given in argument.

        @param features_id : list of intergers. Indices of the columns we want to permute.
        @param indices : numpy array of booleans. 1 if the line is selected, 0 if not.

        @return numpy array switched.
        """
        X = self.X       
        X_perm = numpy.copy(X)
        # permutation
        X_perm[:,features_id] = self.random_permutation(X[:,features_id], indices)
        return X_perm
        
    def importance_tree(self, tree, features_id):
        """!@brief Normalized grouped importance by permutation of the colonms given in argument, in a tree.
        
        @param features_id : list of intergers. Indices of the columns we want to permute.
        @param tree : sklearn object.       
        
        @return float. Grouped importance of the colonms.
        """
        X = self.X
        indices = tree.indices_
        X_perm = self.perm_X(indices, features_id)
        importance_tree = self.error_oob(tree, X_perm) - self.error_oob(tree, X)
        return importance_tree / float(len(features_id))
        
    def importance_forest(self, features_id):
        """!@brief Normalized grouped importance by permutation of the xolomns given in argument, in the forest attribute.
   
        @param features_id : list of intergers. Indices of the columns we want to permute.
        
        @return float. Importance in the forest.
        """
        forest = self.forest
        estimators = forest.estimators_
        n_tree = forest.n_estimators
        I = []
        I.append(([self.importance_tree(tree, features_id) for tree in estimators]))
        return  numpy.sum(I) / float(n_tree)
    
    def importance_forest_all(self):
        """!@brief Normalized grouped importance by permutation of all variables attribute.
        
        @return list. List of the importance of all flight variables.
                """
        I = []
        i = 0
        for names in self.variable_names:
            features_id = self.idx_output(names)
            I.append(self.importance_forest(features_id))
            i += 1
        return I
    
 