""" selector = (grouped importance, group lasso, )
	learner = (RF, Boosting, SVM, knn)
"""


import pp, sys, numpy, warnings
warnings.simplefilter('ignore', UserWarning)

from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor
from sklearn.linear_model import Lasso, lasso_path, enet_path, ElasticNetCV
# from utils import *

class FeatureSelection:
	"""docstring for FeatureSelection"""

	SELECTOR = {
		'RF': lambda: RandomForestSelector(),
		'LASSO': lambda: LassoSelector()
	}



	def __init__(self, selector, **kwargs):
		self.selector = selector
		self.kwargs = kwargs
		
		self.selected_features = None

	def run(self, xtrain, ytrain):
		try:
			selector = self.SELECTOR[self.selector](**self.kwargs)
		except KeyError:
			sys.exit('Unkown selector')

		selector.run(xtrain, ytrain)
		self.selected_features = selector.selected_features



class RandomForestSelector():
	"""docstring for RandomForestSelector"""

	SELECTOR_TYPE = {
		'screening': lambda: self.screening(),
		'rfe': lambda: self.rfe()
	}

	RF_ESTIMATOR = {
		'RF': RandomForestRegressor,
		'ETR': ExtraTreesRegressor
	}

	def __init__(self, selector_type, parameters, RFestimator='RF', n_iter=20, threshold=None, doPlot=False, **kwargs):
		self.n_iter = n_iter
		self.threshold = threshold
		self.selector_type = selector_type
		self.RFestimator = RFestimator
		self.doPlot = doPlot
		self.kwargs = kwargs
		
		# Group importance parameters
		self.nvarGroup=parameters['nvarGroup'] #LIST OF GROUPS LENGTH
		self.groupNames=parameters['groupNames']
		self.normalize=parameters['normalize']
		self.ngroups=len(self.nvarGroup)

		self.selected_features = None
		self.selected_features_names = None
		self.GrImp = None


	def run(self, xtrain, ytrain):
	
		if self.selector_type == 'screening':
			self.screening(xtrain, ytrain)

		elif self.selector_type == 'rfe':
			self.rfe(xtrain, ytrain)

		else:
			sys.exit('Unkown selector type')
		



	def screening(self, xtrain, ytrain):
		# print self.kwargs, self.n_iter, self.threshold is None, self.RFestimator
		print 'Train RF'
		model = self.RF_ESTIMATOR[self.RFestimator](oob_score=1, n_jobs=-1, **self.kwargs)
		model.fit(xtrain, ytrain)
		print 'Compute importance'
		GrImp = Importance(ngroups=self.ngroups, nvarGroup=self.nvarGroup, groupNames=self.groupNames, normalize=self.normalize)
		
		if self.n_iter > 0:
			print '\tParallel execution'
			GrImp.run_parallel(model, xtrain, ytrain, self.n_iter)
			importance = GrImp.importance_mean
		else:
			GrImp.run(model, xtrain, ytrain)
			importance = GrImp.importance

		self.GrImp = GrImp

		if self.threshold is None:
			self.selected_features = numpy.where(importance > 0)[0]
		else:
			self.selected_features = numpy.where(importance > self.threshold)[0]

		self.selected_features_names = [self.groupNames[z] for z in self.selected_features]


		Dat = numpy.array([(self.groupNames[z], round(importance[z], 2)) for z in self.selected_features], dtype=[('name', 'S10'), ('value', 'float64')])
		DatOrdered = numpy.sort(Dat, order='value')[::-1]
		
		print 'Selected features:' 
		print DatOrdered

		if self.doPlot:
			GrImp.plot()

	def rfe(self, xtrain, ytrain):
		print self.kwargs, self.n_iter, self.threshold is None, self.RFestimator
		


class Importance():
	"""docstring for Importance"""
	def __init__(self, ngroups, nvarGroup, groupNames, normalize=True):
		
		self.ngroups = ngroups
		self.nvarGroup = nvarGroup
		self.groupNames = groupNames
		self.normalize = normalize
		
		self.importance = None #2D ARRAY IF PARALLEL COMPUTING AND 1D ARRAY IF NOT
		self.importance_mean = None
		self.importance_sd = None
		self.n_iter = None


	def run_parallel(self, model, xdata, ydata, n_iter=20):
		# print '--- Init'
		job_server = pp.Server(ppservers = ()) # cree des job_server avec le nombre de coeurs disponibles
		ncpus = job_server.get_ncpus()
		job_server.set_ncpus(ncpus)
		modules = ('numpy', 'sklearn.ensemble')

		# print '--- Submit'
		jobs = [job_server.submit(func=self.run, args=(model, xdata, ydata, i), modules=modules) for i in range(n_iter)]

		# print '--- Eval'
		IMP = numpy.array([job() for job in jobs])
		self.importance = IMP
		self.importance_mean = numpy.mean(IMP, axis=0)
		self.importance_sd = numpy.std(IMP, axis=0)
		self.n_iter = n_iter


	def trucAlaCon(self, model, xdata, ydata, i):
		return (i+1, model.n_estimators, xdata.shape, ydata.shape)


	def run(self, model, xdata, ydata, id_core=-1):
		
		
		
		importance_forest = numpy.zeros(len(self.groupNames))

		# LOOP OVER THE TREES
		for tree in model:

			# COMPUTE ERROR OF THE CURRENT TREE ON THE OOB SAMPLES
			X_oob = xdata[tree.indices_ == 0]
			Y_oob = ydata[tree.indices_ == 0]
			error_before = numpy.mean((tree.predict(X_oob) - Y_oob)**2)
		
			# LOOP OVER THE GROUPS
			for j,z in enumerate(self.groupNames):

				idx_current_group = self.idx_output(z)

				# MAKE A COPY OF THE OOB DATA AND PERMUTE IT
				Xperm = numpy.copy(X_oob)
				Xperm[:,idx_current_group] = numpy.random.permutation(Xperm[:,idx_current_group])
				
				# COMPUTE THE ERROR
				error_after = numpy.mean((tree.predict(Xperm) - Y_oob)**2)
				importance_tree = error_after - error_before
				importance_forest[j] = importance_forest[j] + importance_tree
	
		importance = importance_forest / model.n_estimators
		if self.normalize:
			importance = importance / self.nvarGroup

		if id_core == -1:
			self.importance = importance
		else:
			return importance



	def idx_output(self, name):
		idx = []
		nvarGroup = self.nvarGroup
		groupNames = self.groupNames
		index_variable_name = groupNames.index(name)
		for i in range(nvarGroup[index_variable_name]):
			bound = sum(nvarGroup[:index_variable_name])
			idx.append(bound + i)
		return idx


	def plot(self, m=5):
		import matplotlib.pyplot as plt

		importance = self.importance
		groupNames = self.groupNames

		if self.n_iter is None:
	
			Dat = numpy.array([(groupNames[i], importance[i]) for i in range(len(groupNames))], dtype=[('name', 'S10'), ('value', 'float64')])
			DatOrdered = numpy.sort(Dat, order='value')[::-1]
			
			importanceToPlot = [z[1] for z in DatOrdered[0:m]]
			namesToPlot = [z[0] for z in DatOrdered[0:m]]

			print importanceToPlot
			print namesToPlot
	
			fig = plt.figure()
			ax = fig.add_subplot(111)
			rect = ax.bar(numpy.arange(m), importanceToPlot, width=.25, error_kw=dict(elinewidth=5))
			xtickNames = ax.set_xticklabels(namesToPlot)
			plt.setp(xtickNames, rotation=45, fontsize=10)
			plt.show()
		else:
			importance_mean = self.importance_mean
			importance_sd = self.importance_sd

			Dat = numpy.array([(groupNames[i], importance_mean[i]) for i in range(len(groupNames))], dtype=[('name', 'S10'), ('value', 'float64')])
			DatOrdered = numpy.sort(Dat, order='value')[::-1]
			
			importanceToPlot = [z[1] for z in DatOrdered[0:m]]
			namesToPlot = [z[0] for z in DatOrdered[0:m]]

			print [(z, round(importanceToPlot[i], 2)) for i,z in enumerate(namesToPlot)]

			fig = plt.figure()
			ax = fig.add_subplot(111)
			rect = ax.bar(numpy.arange(m), importanceToPlot, width=.25, error_kw=dict(elinewidth=5))
			xtickNames = ax.set_xticklabels(namesToPlot)
			plt.setp(xtickNames, rotation=45, fontsize=10)
			plt.show()

class LassoSelector():
	
	"""docstring for LassoSelector"""
	
	def __init__(self, alpha=None, n_iter=20):
		self.n_iter = n_iter
		self.alpha = alpha

		self.selected_features = None

	def run(self, xtrain, ytrain):
		
		selector = Lasso(alpha=self.alpha)
		selector.fit(xtrain, ytrain)

		self.selected_features = numpy.where(selector.coef_ != 0)
		# print '\t' + str(sum(selector.coef_ != 0)) + ' selected features over ' + str(len(selector.coef_))


	def reg_path(self, xtrain, ytrain):

		# alphas, coefs, _ = lasso_path(xtrain, ytrain, 5e-11, fit_intercept=False)
		alphas, coefs, _ = lasso_path(xtrain, ytrain, fit_intercept=True)

		import matplotlib.pyplot as plt
		ax = plt.gca()
		ax.set_color_cycle(2 * ['b', 'r', 'g', 'c', 'k'])
		l1 = plt.plot(-numpy.log10(alphas), coefs.T)

		plt.xlabel('-Log(alpha)')
		plt.ylabel('coefficients')
		plt.axis('tight')
		plt.show()



class EnetSelector():
	
	"""docstring for LassoSelector"""
	
	def __init__(self, alpha=None, l1_ratio=.5, n_iter=20):
		self.n_iter = n_iter
		self.alpha = alpha
		self.l1_ratio = l1_ratio

		self.selected_features = None

	def run(self, xtrain, ytrain):
		
		if self.alpha is None:
			selector = ElasticNet()
		else:
			selector = ElasticNet(alpha=self.alpha)
		selector.fit(xtrain, ytrain)

		self.selected_features = numpy.where(selector.coef_ != 0)
		# print '\t' + str(sum(selector.coef_ != 0)) + ' selected features over ' + str(len(selector.coef_))


	def reg_path(self, xtrain, ytrain):

		# alphas, coefs, _ = lasso_path(xtrain, ytrain, 5e-11, fit_intercept=False)
		alphas, coefs, _ = enet_path(xtrain, ytrain, fit_intercept=True)

		import matplotlib.pyplot as plt
		ax = plt.gca()
		ax.set_color_cycle(2 * ['b', 'r', 'g', 'c', 'k'])
		l1 = plt.plot(-numpy.log10(alphas), coefs.T)

		plt.xlabel('-Log(alpha)')
		plt.ylabel('coefficients')
		plt.axis('tight')
		plt.show()


	def CV(self, xtrain, ytrain):
		cv = ElasticNetCV(self.l1_ratio)
		cv.fit(xtrain, ytrain)
		return cv.alpha_, cv.coef_
