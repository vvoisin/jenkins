# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.linear_model import SGDRegressor, Lasso
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import make_scorer, mean_squared_error
from sklearn.cross_validation import train_test_split, KFold

from src.utils import *

import sys, traceback
import numpy as np




REGRESSOR_DEFAULT = {
	'RF': lambda: RandomForestRegressor(),
	'ETR': lambda: ExtraTreesRegressor(),
	'GBR': lambda: GradientBoostingRegressor(),
	'KNN': lambda: KNeighborsRegressor(),
	'SVR': lambda: SVR()
}


class Learner():
	"""Class for learning method"""
	
	# REGRESSOR = {
	# 	'RF': lambda n_estimators: RandomForestRegressor(n_jobs=-1, n_estimators=n_estimators),
	# 	'ETR': lambda n_estimators: ExtraTreesRegressor(n_jobs=-1, n_estimators=n_estimators),
	# 	'GBR': lambda n_estimators: GradientBoostingRegressor(n_estimators=n_estimators, loss='huber'),
	# 	'KNN': lambda k: KNeighborsRegressor(n_neighbors=k, weights='distance'),
	# 	'SVR': lambda C: SVR(kernel='rbf', C=C),
	# 	'SGD': lambda: SGDRegressor(loss='epsilon_insensitive', penalty='l2'),
	# 	'L1SGD': lambda: SGDRegressor(loss='epsilon_insensitive', penalty='l1')
	# }

	REGRESSOR = {
		'RF': RandomForestRegressor,
		'ETR': ExtraTreesRegressor,
		'GBR': GradientBoostingRegressor,
		'KNN': KNeighborsRegressor,
		'SVR': SVR,
		'SGD': SGDRegressor,
		'L1SGD': SGDRegressor
	}

	# DEFAULTS_PARAMETERS = {'RF': 500, 'ETR': 500, 'GBR': 500, 'KNN': 5, 'SVR': 1}

	def __init__(self, regressor, model=None, **kwargs):
		self.regressor = regressor
		self.kwargs = kwargs
		self.model = model
		self.predictions = None


	def __class__(self):
		pass

	# def getClass(self):
	# 	return 'Learner'

	def train(self, xtrain, ytrain):
		# print self.kwargs
		# if len(self.kwargs) == 1:
		model = self.REGRESSOR[self.regressor](**self.kwargs)
		model.fit(xtrain, ytrain)
		self.model = model


	def predict(self, xtest, ytest=None):
		pred = self.model.predict(xtest)
		# self.predictions = pred
		if ytest is not None:
			# self.mse = np.mean((ytest - pred)**2)
			# self.mae = np.mean(np.fabs(ytest - pred))
			mse = np.mean((ytest - pred)**2)
			mae = np.mean(np.fabs(ytest - pred))
			return pred, mse, mae
		return pred


	def KfoldCV(self, xdata, ydata, K=5, verbose=False):
		""" Je peux prendre la fct 'cross_val_score' de sklearn mais je prefere recoder moi meme pour etre sur. Voir pour calculer en parallèle 
			+ suffle = False dans KFold ? Toujours les meme fold alors ..."""
		n = len(ydata)
		kf = KFold(n, n_folds=K)
		mae = []
		if self.model is None:
			model = self.REGRESSOR[self.regressor](**self.kwargs)
		else:
			model = self.model

		for idxtr, idxte in kf:
			model.fit(xdata[idxtr], ydata[idxtr])
			# if verbose: print '\t' + str(len(model.support_)) + ' support vectors over ' + str(len(idxtr))
			pred = model.predict(xdata[idxte])
			mae.append(np.mean(np.fabs(ydata[idxte] - pred)))
		return mae


	# def gridSeach(self, xval, yval, param_grid):
	# 	# pars = self.DEFAULTS_PARAMETERS[self.regressor]
	# 	estimator = self.REGRESSOR_DEFAULT[self.regressor]()
	# 	grid = GridSearchCV(estimator, param_grid=param_grid)
	# 	grid.fit(xval, xval)
	# 	return grid.best_estimator_, grid.best_params_


	def serialize(self, path):
		pass


class CombinedLearner():
	"""Class for a combined learning machine"""


	def __init__(self, method, machines, W=None):
	
		# ICI TEST LA LISTE DES MACHINES : METHODES FIT ET PREDICT
		if machines.__class__ is not dict:
			sys.exit('The machines must be a dictionary: {<Name>:<Learner instance>, ...}')
		self.machines = machines

		if len(machines) != 0 and not self.control():
			sys.exit('The machines must be a dictionary of Learner instances')


		# if method.__class__.__name__ != 'function':
		# 	sys.exit('The agregation method must be a function. Ex. numpy.mean or numpy.median.')
		self.method = method
		self.W = W

		self.combined_predictions = None


	def control(self):
		cont = [m.__class__.__name__ == 'Learner' for m in self.machines.values()]
		return all(cont)

	def add_machine(self, machine):
		# ICI TEST LA VALIDITE DE LA MACHINE : METHODES FIT ET PREDICT
		self.machine.append(machine)


	def train(self, xtrain, ytrain, verbose=True):
		for m in iter(self.machines):
			try:
				if verbose: print 'Training machine ' + str(m)
				self.machines[m].train(xtrain, ytrain)
			except:
				print 'Fail in training machine ' + str(m)
				traceback.print_exc()
				sys.exit('Ending')


	def predict(self, xtest, ytest=None):
		
		predictions = np.array([self.machines[m].predict(xtest) for m in iter(self.machines)]).T

		if self.method == 'mean':
			self.combined_predictions = np.mean(predictions, axis=1)

		if self.method == 'Wmean':
			if self.W is None:
				sys.exit('Please enter the weight vector')
			self.combined_predictions = Wmean(predictions, self.W)

		if self.method == 'median':
			self.combined_predictions = np.median(predictions, axis=1)

		if ytest is not None:
			return self.combined_predictions, np.mean(np.fabs(ytest - self.combined_predictions))
		else:
			return self.combined_predictions


	def KfoldCV(self, xdata, ydata, K=5, verbose=False):
		""" Je peux prendre la fct 'cross_val_score' de sklearn mais je prefere recoder moi meme pour etre sur. Voir pour calculer en parallèle 
			+ suffle = False dans KFold ? Toujours les meme fold alors ..."""
		n = len(ydata)
		kf = KFold(n, n_folds=K)
		mae = []
		
		for idxtr, idxte in kf:
			self.train(xdata[idxtr], ydata[idxtr], verbose=False)
			pred = self.predict(xdata[idxte])
			mae.append(np.mean(np.fabs(ydata[idxte] - pred)))
		return mae


# 
# ROUTINES
# 

def gridSearch(regressor, xdata, ydata, param_grid, n_jobs=8):
	""" Grid search by cross validation """
	estimator = REGRESSOR_DEFAULT[regressor]()
	grid = GridSearchCV(estimator, param_grid=param_grid, n_jobs=n_jobs)
	grid.fit(xdata, ydata)

	return Learner(regressor=regressor, model=grid.best_estimator_), grid.best_params_
