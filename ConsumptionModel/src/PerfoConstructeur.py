import sys, traceback, rpy, os, json
import numpy as np
import pandas as pd

class PerfoConstructeur():
	def __init__(self, R_path):
		
		self.R_path = R_path
		self.consumption = {}
		

	def run(self, fileName, ToC, GW, ISA, fuel_factor=0):
		
		command = "source('" + self.R_path + "interp3D_donnees_construct.R')"
		fct = rpy.r(command)

		perfo_FL100 = fct['value'](100, GW, ISA)
		perfo_ToC = fct['value'](ToC, GW, ISA)

		perfo = {'ToC':ToC, 'GW':GW, 'ISA':ISA, 'perfo':(perfo_ToC - perfo_FL100) * (1 + fuel_factor / 100)}
		self.consumption[fileName] = perfo


	def run_folder(self, folder, mounth=None, n=None):
		
		Files_tmp = np.array(os.listdir(dataPath))
		
		mounth_all = np.array([s[7:9] for s in Files_tmp])
		
		if mounth is None:
			Files = Files_tmp
		else:
			idx = np.where(mounth_all == mounth)[0]
			Files = Files_tmp[idx]

		if n is not None:
			Files = Files[0:n]


		for j,f in enumerate(Files):

			# print folder + f
			if j % 100 == 0:
				print str(j) + ' files processed ' + str(round(float(j) / len(Files) * 100, 2)) + '%'

			try:
				dat = pd.read_csv(folder + f, delimiter=';')
				N = len(dat.index)
				dat = dat.iloc[1:N]
				GW = np.array(dat.GW[0:5], dtype=float)[0] / 1000
				GW = GW / 2.2
				Alt = np.array(dat.ALTITUDE, dtype='float')

				LEVEL_OFF = dat['LEVEL OFF']
				if 3 not in LEVEL_OFF:
					raise IndexError
	
				# GET ALTITUDE AND ToC
				idxToC = np.where(LEVEL_OFF == '3.0')[0][0] # The first indice for cruise
				ToC = int(Alt[idxToC] / 100)
				if ToC < 300 or ToC > 400:
					raise Warning

				# COMPUTE DELTA ISA FROM TAT
				idxFL200 = np.where(Alt >= 20000)[0][0]
				TAT_FL200 = dat.TAT[idxFL200]
				MACH_FL200 = dat.MACH[idxFL200]
				gamma = 1.4
				SAT_FL200 = (TAT_FL200 + 274.15) / (1 + MACH_FL200**2 * (gamma - 1) / 2)
				SAT_STD_FL200 = 288.15 - 6.5e-3 * Alt[idxFL200] * .3048
				ISA = SAT_FL200 - SAT_STD_FL200

				self.run(f, ToC, GW, ISA)
			
			except IndexError:
				print '\nWarning: cruise not detected.'
			except Warning:
				print '\nWarning: ToC out of bounds.'
			except:
				print '\nFail to process ' + dataPath+f
				print '\tWhy ? ' + str(sys.exc_info()[1])
				traceback.print_exc()
				sys.exit(0)



	def run_files(self, Files, folder, fuel_factor_df=None):
		
		for j,f in enumerate(Files):

			# print folder + f
			if j % 100 == 0:
				print str(j) + ' files processed ' + str(round(float(j) / len(Files) * 100, 2)) + '%'

			try:
				dat = pd.read_csv(folder + f, delimiter=';')
				N = len(dat.index)
				dat = dat.iloc[1:N]
				GW = np.array(dat.GW[0:5], dtype=float)[0] / (1000 * 2.2)

				Alt = np.array(dat.ALTITUDE, dtype='float')

				LEVEL_OFF = dat['LEVEL OFF']
				if 3 not in LEVEL_OFF:
					raise IndexError
	
				# GET ALTITUDE AND ToC
				idxToC = np.where(LEVEL_OFF == '3.0')[0][0] # The first indice for cruise
				ToC = int(Alt[idxToC] / 100)
				if ToC < 300 or ToC > 400:
					raise Warning

				# COMPUTE DELTA ISA FROM TAT
				idxFL200 = np.where(Alt >= 20000)[0][0]
				TAT_FL200 = dat.TAT[idxFL200]
				MACH_FL200 = dat.MACH[idxFL200]
				gamma = 1.4
				SAT_FL200 = (TAT_FL200 + 274.15) / (1 + MACH_FL200**2 * (gamma - 1) / 2)
				SAT_STD_FL200 = 288.15 - 6.5e-3 * (Alt[idxFL200] * .3048)
				ISA = SAT_FL200 - SAT_STD_FL200


				# Fuel Factor
				if fuel_factor_df is not None:
					Regist = f[0:6]
					ffactor = float(fuel_factor_df.FuelFactor[fuel_factor_df.REGIST == Regist])

					if np.isnan(ffactor):
						raise ValueError
					self.run(f, ToC, GW, ISA, fuel_factor=ffactor)
				else:
					self.run(f, ToC, GW, ISA, fuel_factor=0)


			except ValueError:
				print '\nNo fuel factor found for flight ' + Regist
			except IndexError:
				print '\nWarning: cruise not detected.'
			except Warning:
				print '\nWarning: ToC out of bounds.'
			except SystemExit:
				sys.exit(0)
			except:
				print '\nFail to process ' + folder+f
				print '\tWhy ? ' + str(sys.exc_info()[1])
				traceback.print_exc()
				sys.exit(0)


	# def serialize(self):
	# 	with open('')

# TESTS

# dataPath = '/home/bapt/Documents/Flight data/TVF/extract/'
# perf = PerfoConstructeur('R/')
# perf.run_folder(dataPath, n=200)

# for e in iter(perf.consumption):
# 	print e, perf.consumption[e]

# Test run
# dataPath = '/home/bapt/Documents/Flight data/TVF/extract/'
# Files = np.array(os.listdir(dataPath))

# perf = PerfoConstructeur('R/')
# A = [[40, 10], [40, 20], [60, 10], [60, 20]]
# for j,f in enumerate(Files[0:4]):
# 	perf.run(f, 380, A[j][0], A[j][1])
# 	print j, perf.consumption

