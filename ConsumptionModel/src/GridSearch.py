# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.grid_search import GridSearchCV
from src.Learner import *






# class GridSearch():
# 	"""Class for learning method"""
	
# 	REGRESSOR_DEFAULT = {
# 		'RF': lambda: RandomForestRegressor(),
# 		'ETR': lambda: ExtraTreesRegressor(),
# 		'GBR': lambda: GradientBoostingRegressor(),
# 		'KNN': lambda: KNeighborsRegressor(),
# 		'SVR': lambda: SVR(kernel='rbf')
# 	}


# 	def __init__(self, regressor, param_grid):
# 		self.regressor = regressor
# 		self.param_grid = param_grid
# 		self.best_params = None


# 	def run(self, xval, yval):
# 		regressor = self.REGRESSOR_DEFAULT[self.regressor]()
# 		grid = GridSearchCV(regressor, param_grid=self.param_grid)
# 		grid.fit(xval, xval)
# 		self.best_params = grid.best_params_

# 		return Learning(regressor=regressor, model=grid.best_estimator_)

