# !/usr/bin/env python
# -*- coding:utf-8 -*-

import json, cherrypy, sys, pkg_resources
import numpy as np
from sys import exit
# from OptiClimbModel.OptiClimbModel import *

# ENLEVER DES QUE C EST OK
sys.path.append('../OC-OptiClimb-model-package/')
from OptiClimbModel.OptiClimbModel import *



reload(sys)
sys.setdefaultencoding('utf8')

class WebService(object):


	def __init__(self):

		print '\n*** Import Evaluation object'
		
		# IMPORT THE LAST OBJECT

		# 1- STANDARD
		path = 'models/standard/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		if listdir == []:
			msg = 'No models found in "models/standard/" folder'
			print msg
			exit(msg)

		with open(path+listdir[-1], 'r') as f:
			model = cPickle.load(f)

		print path+listdir[-1] + ' imported'

		if model.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load-and-learn-standard.py'."
			print msg
			exit(msg)
		else:
			print 'Model trained with ' + str(model.nr_flights) + ' flights.\n'

		self.standard_model = model


		# 1- OPTICLIMB
		path = 'models/opticlimb/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		if listdir == []:
			msg = 'No models found in "models/opticlimb/" folder'
			print msg
			exit(msg)

		with open(path+listdir[-1], 'r') as f:
			model = cPickle.load(f)

		print path+listdir[-1] + ' imported'

		if model.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load-and-learn-opticlimb'."
			print msg
			exit(msg)
		else:
			print 'Model trained with ' + str(model.nr_flights) + ' flights.\n'

		self.opticlimb_model = model


	@cherrypy.expose
	def index(self):
		if 'Content-Length' not in cherrypy.request.headers:
			return 'No data received. Use a POST request.'

		cl = cherrypy.request.headers['Content-Length']
		request = cherrypy.request.body.read(int(cl))
		cherrypy.response.headers['Content-Type'] = 'application/json'
		return self.process(request)


	@cherrypy.expose
	def test(self):
		return 'Test ok'


	@cherrypy.expose
	def get_version(self):
		return 'OptiClimbModel version v' + pkg_resources.require('OptiClimbModel')[0].version


	def process(self, request):

		print '\n\t*** Request into dict'
		request_dict = eval(request)
		tow,delta_isa,toc,registration,origin = self.check_request(request_dict)

		print '\t*** Prediction of OptiClimb consumption'
		opticlimb_pred = self.opticlimb_model.predict(tow, delta_isa, toc, registration, origin)
		print '\t*** Prediction of Standard consumption'
		standard_pred = self.standard_model.predict(tow, delta_isa, toc, registration, origin)

		print '\t*** dict into Response\n'
		result = {'standard':float(standard_pred), 'opticlimb':float(opticlimb_pred), 'pkg_version':pkg_resources.require('OptiClimbModel')[0].version}
		return json.dumps(result)


	def check_request(self, request_dict):
		
		keys = ['tow', 'delta_isa', 'toc', 'registration', 'origin']
		if sum([z in request_dict.keys() for z in keys]) != 5:
			msg = 'Parameter missing in request. Must be ' + str(keys)
			print msg
			raise cherrypy.HTTPError(400, msg)

		tow = request_dict['tow']
		delta_isa = np.array(request_dict['delta_isa'])
		# time_to_toc = np.array(request_dict['time_to_toc'])
		toc = np.array(request_dict['toc'])
		registration = request_dict['registration']
		origin = request_dict['origin']

		if tow < 10000 or tow > 100000:
			print '\t\t tow cannot be lower than 10000 and larger than 100000\n'
			raise cherrypy.HTTPError(400, 'tow cannot be lower than 10000 and larger than 100000')
		if delta_isa < -30 or delta_isa > 30:
			print '\t\t delta_isa cannot be lower than -30 and larger than 30\n'
			raise cherrypy.HTTPError(400, 'delta_isa cannot be lower than -30 and larger than 30')
		# if time_to_toc < 200 or time_to_toc > 3000:
		# 	print '\t\t time_to_toc cannot be lower than 200 and larger than 3000\n'
		# 	raise cherrypy.HTTPError(400, 'time_to_toc cannot be lower than 200 and larger than 3000')
		if toc < 200 or toc > 450:
			print '\t\t toc cannot be lower than 200 and larger than 450\n'
			raise cherrypy.HTTPError(400, 'toc cannot be lower than 200 and larger than 450')
		if registration not in self.opticlimb_model.registrations:
			print '\t\t registration not in available registrations list\n'
			print self.opticlimb_model.registrations			
			raise cherrypy.HTTPError(400, 'registration not in available registrations list')		
		if registration not in self.standard_model.registrations:
			print '\t\t registration not in available registrations list\n'
			print self.standard_model.registrations			
			raise cherrypy.HTTPError(400, 'registration not in available registrations list')		
		if origin not in self.opticlimb_model.origins:
			print '\t\t origin not in available origins list\n'
			print self.opticlimb_model.origins			
			raise cherrypy.HTTPError(400, 'origin not in available origins list')
		if origin not in self.standard_model.origins:
			print '\t\t origin not in available origins list\n'
			print self.standard_model.origins			
			raise cherrypy.HTTPError(400, 'origin not in available origins list')

		return tow,delta_isa,toc,registration,origin


	@cherrypy.expose
	def reload(self):
		print '*** RELOAD'
		print '*** Import OptiClimbModel object'
		
		# IMPORT THE LAST OBJECT

		# 1- STANDARD
		path = 'models/standard/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			model = cPickle.load(f)

		print path+listdir[-1] + ' imported\n'

		if model.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load-and-learn-standard.py'."
			print msg
			return msg

		self.standard_model = model

		# 1- OPTICLIMB
		path = 'models/opticlimb/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			model = cPickle.load(f)

		print path+listdir[-1] + ' imported\n'

		if model.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load-and-learn-opticlimb'."
			print msg
			return msg

		self.opticlimb_model = model





if len(sys.argv) == 3:
	host = sys.argv[1]
	port = int(sys.argv[2])
else:
	print 'Initialize web service at localhost'
	host = '127.0.0.1'
	port = 8080



if __name__ == '__main__':
	cherrypy.config.update({
		'server.socket_host' : host,
		'server.socket_port' : port,
		'server.thread_pool' : 5,
		'tools.sessions.on' : True,
		'tools.encode.encoding' : 'Utf-8'
	})
	cherrypy.quickstart(WebService())

