import sys, csv, math, os, pkg_resources
import numpy as np
from sys import exit

if len(sys.argv) != 4:
	print 'Usage: python load-and-learn-opticlimb.py [data_path] [nr_flights] [ncpus]'
	exit(0)

sys.path.append('../OC-OptiClimb-model-package/')
from OptiClimbModel.OptiClimbModel import OptiClimbModel

# 
# OFFLINE : load data and learn model
# 

# INPUT PARAMETERS
data_path = sys.argv[1]
nr_flights = int(sys.argv[2])
ncpus = int(sys.argv[3])
regressor = 'SVR'					# regressor in ('ETR', 'RF', 'SVR')
output_path = 'models/standard/'	# path for models objects


print '\n\t\t--- Standard consumption model version v' + pkg_resources.require('OptiClimbModel')[0].version
print '\t\t--- data_path ' + str(data_path)
print '\t\t--- nr_flights ' + str(nr_flights)
print '\t\t--- ncpus ' + str(ncpus)
print '\t\t--- regressor ' + str(regressor)
print '\t\t--- output_path ' + str(output_path) + '\n'


# MODEL OBJECT
OCmodel = OptiClimbModel(data_path, nr_flights, output_path, regressor, ncpus)

# LEARNING
OCmodel.load_and_learn()
OCmodel.serialize('standard-'+regressor)

