import sys, traceback
import numpy as np
from sys import exit
sys.path.append('../OC-OptiClimb-model-package/OptiClimbModel/')
from OptiClimbModel import OptiClimbModel


# INPUT PARAMETERS
data_path = '../../Data/TVF/TVF-data/'
nr_flights = int(sys.argv[1])
regressor = 'SVR'
output_path = 'models/standard/'


# EVALUATION OBJECT
OCmodel = OptiClimbModel(data_path, nr_flights, output_path, regressor, ncpus=-1, fold=5)
OCmodel.load_and_learn()
