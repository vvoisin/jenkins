#!/usr/bin/env python
# from distutils.core import setup
from setuptools import setup
from pkgutil import iter_modules
import sys, os


if '--v' in sys.argv:
	idx = sys.argv.index('--v')
	version = sys.argv[idx+1]
	sys.argv.remove('--v')
	sys.argv.remove(version)
	
	print '\n---------------------------------------------------------'
	print '--- Building OC-OptiClimb-model-package version ' + version + ' ---'
	print '---------------------------------------------------------\n\n'

	setup(
		name='OptiClimbModel',
		version=version,
		author='SafetyLine',
		packages=['OptiClimbModel'],
		license='LICENSE.txt',
		description='Python package for OptiClimb project -- OptiClimbModel',
		long_description=open('README.txt').read(),
		install_requires=['scikit-learn', 'pandas', 'cherrypy', 'numpy', 'scipy']
		)

	if 'clean' in sys.argv:
		s = 'OptiClimbModel-' + version + '.tar.gz'
		command = 'rm dist/'+s
		print '\n' + command
		os.system(command)

	installed_pkg = [z[1] for z in iter_modules()]
	if 'ConsumptionModel' not in installed_pkg:
		print '\n\n!!! Warning !!!\n'
		print '\t"ConsumptionModel" pkg must be installed locally before running "OptiClimb-model" pkg'
		print '\n!!! Warning !!!'

else:
	print 'Error: version missing'

