# !/usr/bin/env python
# -*- coding:utf-8 -*-

import sys, os, traceback, cPickle, multiprocessing
import numpy as np

from numpy.random import randint
from sys import exit
from ConsumptionModel.Learning.Learner import Learner, gridSearch
from DataExtractor import *
from sklearn import preprocessing

class OptiClimbModel():
	""" Regression model: prediction of consumption for OC flights """

	
	def __init__(self, data_path, nr_flights, output_path, regressor, ncpus=-1, fold=None, FLmin=50):
		
		self.data_path = data_path
		self.nr_flights = nr_flights
		self.output_path = output_path
		self.regressor = regressor
		self.ncpus = ncpus
		self.fold = fold
		self.FLmin = FLmin

		self.origins = None
		self.registrations = None
		self.value = None
		self.model = None
		self.status = 0 # state = 1 si le modèle est construit et 0 sinon

		
	def load_data(self, folder, files):

		print 'Loading data from FL'+ str(self.FLmin) +' to TOC ...'

		extractor = DataExtractor(params=['ALTITUDE', 'SAT', 'FF1', 'FF2', 'TOW', 'MACH', 'FROM', 'REGIST'], flight_phase='climb', FLmin=self.FLmin)
		Time, data_list, files_keep = extractor.run(files, folder, ncpus=self.ncpus)


		ALTITUDE = np.array([z['ALTITUDE'] for z in data_list])
		SAT = np.array([z['SAT'] for z in data_list])
		FF1 = np.array([z['FF1'] for z in data_list]) / (2.2*3600)	# pph --> kg/s
		FF2 = np.array([z['FF2'] for z in data_list]) / (2.2*3600)	# pph --> kg/s
		FF = FF1 + FF2

		MACH = np.array([z['MACH'] for z in data_list])

		FROM = np.array([z['FROM'] for z in data_list])
		From_dummy = dummy(FROM)
		self.origins = From_dummy[1]


		Regist = np.array([z['REGIST'] for z in data_list])
		Regist_dummy = dummy(Regist)
		self.registrations = Regist_dummy[1]

		### Step 2: BUILD TRAIN SET
		
		# COMPUTE TOC
		TOC = np.array([ int( round(np.max(z) / 1000) * 10 ) for z in ALTITUDE ])

		# COMPUTE DELTA ISA
		deltaISA = np.array([ compute_delta_isa(z + 273.15, ALTITUDE[i], MACH[i]) for i,z in enumerate(SAT) ])

		# TOW
		TOW = np.array([z['TOW'] for z in data_list])		# in kg

		# Time to TOC
		# t_to_TOC = np.array([len(z['Time']) for z in data_list])

		designMatrix = np.concatenate((
						TOW.reshape((-1,1)), 
						deltaISA.reshape((-1,1)), 
						# t_to_TOC.reshape((-1,1)), 
						TOC.reshape((-1,1)), 
						Regist_dummy[0], 
						From_dummy[0] ), axis=1)


		### Step 3: consumption at TOC
		consumption = np.array([np.sum(z) - z[0] for z in FF])

		return designMatrix, consumption


	def load_and_learn(self):
		"""
			Importation des données QAR et apprentissage de modèle pour la prédiction de la consommation et de la distance 
			en fonction des données d'entrée (ias, altitude, masse)

			Return : état
		"""

		# IMPORT FLIGHT DATA
		print 'Load data'
		files = np.array(os.listdir(self.data_path))
		
		nr_flights = self.nr_flights
		if nr_flights > 0:
			files = files[0:nr_flights]

		designMatrix, consumption = self.load_data(self.data_path, files)


		print 'Scaling data'
		means = np.mean(designMatrix, axis=0)
		stds = np.std(designMatrix, axis=0)
		self.scale_pars = {'means':means, 'stds':stds}
		designMatrix = preprocessing.scale(designMatrix)


		### REGRESSION MODELS
		# INSTANTIATE
		print '\tConsumption model'
		if self.regressor in ['ETR', 'RF']:
			model_consumption = Learner(self.regressor, n_estimators=500, n_jobs=self.ncpus)
			model_consumption.train(designMatrix, consumption)
			
		elif self.regressor == 'SVR':

			# TRAIN
			print 'Consumption model calibration'
			C_range = np.logspace(-2, 10, 3)
			gamma_range = np.logspace(-9, 3, 3)
			eps_range = [.01, .1, 1]
			param_grid = dict(C=C_range, kernel=['rbf'], gamma=gamma_range, epsilon=eps_range)

			if self.ncpus == -1: n_jobs = multiprocessing.cpu_count()
			else: n_jobs = self.ncpus

			model_consumption, best_params = gridSearch('SVR', designMatrix, consumption, param_grid=param_grid, n_jobs=n_jobs)
			print 'Best parameters:'
			print best_params


		print 'Done'
		self.model = model_consumption
		
		if self.fold is not None:
			print 'K-fold cross validation'
			mae, mse = model_consumption.KfoldCV(designMatrix, consumption, K=self.fold)
			mae = np.mean([np.mean(z) for z in mae])
			rmse = np.sqrt(np.mean([np.mean(z) for z in mse]))

			print 'Mean Absolute Error = ' + str(mae) + ' kg'
			print 'Root Mean Squared Error = ' + str(rmse) + ' kg'

		self.status = 1


	def serialize(self, name):
		"""
			Fonction de sérialisation de l'objet courant. Dès que le web service est lancé et si le modèle est construit, 
			alors j'importe l'objet et j'attend les données pour la prédiction
		"""
		print 'Serialize current object'
		with open(self.output_path + name + '.object', 'w') as f:
			cPickle.dump(self, f)


	def predict(self, tow, delta_isa, toc, registration, origin):
		"""
			Predict consumption if using OC

			Input parameters: from flight plans

			Output values: consumption at TOC (computed between FL30 to TOC)
		"""

		### get models
		model_consumption = self.model

		# Get registration
		if registration not in self.registrations:
			print 'Warning: Registration not in historical data. The model may be not reliable.'

		registration_dummy = dummy_one(registration, self.registrations)[0].tolist()

		# Get origin
		if origin not in self.origins:
			print 'Warning: Origin not in historical data. The model may be not reliable.'

		origin_dummy = dummy_one(origin, self.origins)[0].tolist()


		newx = np.array([tow, delta_isa, toc] + registration_dummy + origin_dummy)

		# Scale data
		means = self.scale_pars['means']
		stds = self.scale_pars['stds']
		stds[np.where(stds == 0)[0]] = np.inf
		newx = (newx - means) / stds

		return model_consumption.predict(newx.reshape((1,-1)))


def dummy(x):
	cat = np.unique(x)
	M = []
	for z in cat:
		tmp = np.array([int(zz) for zz in (x==z)])
		M.append(tmp)
	return (np.array(M).T, cat)


def dummy_one(x, cat):

	M = [int(x == z) for z in cat]
	return (np.array(M), cat)


def compute_delta_isa(Sat, Alt, Mach):

	idxFL200 = np.where(Alt >= 20000)[0][0]
	SAT_FL200 = Sat[idxFL200]
	SAT_STD_FL200 = 288.15 - 6.5e-3 * Alt[idxFL200] * .3048
	return round(SAT_FL200 - SAT_STD_FL200)
