# !/usr/bin/env python
#  -*- coding: utf-8 -*-

import json, sys, os, traceback, pp, pandas, numpy

class DataExtractor():
	""" Flight data extractor """


	LEVEL_OFF_CODING = {
		'ON_GROUND':0,
		'CLIMB':1,
		'CLIMB_STEP':2,
		'CRUISE':3,
		'DESCENT':4,
		'DESCENT_STEP':5
	}

	
	def __init__(self, params, flight_phase, FLmin=100):
		self.params = params
		self.FLmin = FLmin

		if flight_phase not in ['takeoff', 'climb']:
			sys.exit('Unknown flight phase')
		self.flight_phase = flight_phase


	def run_one(self, file_name):
		""" Extract a list of flight parameters for a flight """

		try:
			fname = file_name.split('/')[-1]
			if fname == 'METADATA.txt':
				raise Exception('METADATA.txt found')

			tmpDat = pandas.read_csv(file_name, delimiter=',')
			if 'null' in list(tmpDat.LEVEL_OFF):
				idx_null = numpy.where(tmpDat.LEVEL_OFF == 'null')[0][0]
				tmpDat = tmpDat.iloc[0:idx_null]

			ALTITUDE = numpy.array(tmpDat.ALTITUDE, dtype=float)
			# GS = numpy.array(tmpDat.GS, dtype=float)
			LEVEL_OFF = numpy.array(tmpDat.LEVEL_OFF, dtype=int)

			if ALTITUDE[0] > 1e4:
				raise ValueError('Outlier')


			# GET FLmin AND FLmax
			if self.flight_phase == 'takeoff':
				# FLmin
				FLmin = 30
				idxFLmin = numpy.where(ALTITUDE >= 3000)[0][0]
				# FLmax
				FLmax = 100
				idxFLmax = numpy.where(ALTITUDE >= 10000)[0][0]

			if self.flight_phase == 'climb':
				# FLmin
				FLmin = self.FLmin
				idxFLmin = numpy.where(ALTITUDE >= self.FLmin * 100)[0][0]
				# FLmax
				# idxFLmax = numpy.where(LEVEL_OFF == 3.0)[0][0] # The first indice for cruise
				idxFLmax = numpy.where(LEVEL_OFF == self.LEVEL_OFF_CODING['CRUISE'])[0][0] # The first indice for cruise

				FLmax = int(ALTITUDE[idxFLmax] / 100)
				if FLmax < 200:
					raise ValueError('ToC must be larger than 200')


			# REDUCE DATA
			ALTITUDE = ALTITUDE[idxFLmin:idxFLmax+1]
			# GS = GS[idxFLmin:idxFLmax+1]
			LEVEL_OFF = LEVEL_OFF[idxFLmin:idxFLmax+1]
			

			#LEVEL-OFF DETECTION
			without_LO = numpy.where(LEVEL_OFF == self.LEVEL_OFF_CODING['CLIMB'])[0]
			# without_LO = numpy.where(LEVEL_OFF == 0)[0]
			# GS = GS[without_LO]
			ALTITUDE = ALTITUDE[without_LO]

			#TRAVELLED DISTANCE
			# GS = [z * .514 for z in GS] #GS in m/s
			# Distance = numpy.cumsum(GS) / 1000 #Distance in km

			DataDict = {}
			for v in self.params:

				if v not in ['FROM', 'TO', 'REGIST']:
					x = numpy.array(tmpDat[v], dtype=float)[idxFLmin:idxFLmax+1]
					x = x[without_LO]
					y = numpy.arange(len(x)) #TIME
					DataDict[v] = x

			DataDict['Time'] = y
			DataDict['file_name'] = fname

			if 'REGIST' in self.params: DataDict['REGIST'] = tmpDat['REGIST'][idxFLmin]
			if 'FROM' in self.params: DataDict['FROM'] = tmpDat['FROM'][idxFLmin]
			if 'TO' in self.params: DataDict['TO'] = tmpDat['TO'][idxFLmin]

			return DataDict

		except Exception as err:
			print fname
			print 'Line ' + str(sys.exc_info()[-1].tb_lineno) + ': ' + str(err)
			return None



	def run(self, Files, folder, ncpus=-1):

		if ncpus in [0,1]:
			print 'Sequential data extraction'
			data_list = [self.run_one(folder + f) for f in Files]

		elif ncpus == -1:
			
			job_server = pp.Server(ppservers=())
			ncp = job_server.get_ncpus()
			job_server.set_ncpus(ncp)
			modules = ('numpy', 'pandas')
			depfuncs = ()
			
			print 'Parallel data extraction with ncpus = ' + str(ncp)
			jobs = [job_server.submit(func=self.run_one, args=(folder + f,), modules=modules, depfuncs=depfuncs) for f in Files]
			data_list = numpy.array([job() for job in jobs])

		else:

			job_server = pp.Server(ppservers=())
			job_server.set_ncpus(ncpus)
			modules = ('numpy', 'pandas')
			depfuncs = ()
			
			print 'Parallel data extraction with ncpus = ' + str(ncpus)
			jobs = [job_server.submit(func=self.run_one, args=(folder + f,), modules=modules, depfuncs=depfuncs) for f in Files]
			data_list = numpy.array([job() for job in jobs])

		keep = [i for i,z in enumerate(data_list) if z is not None]
		print str(len(keep)) + ' files kept over ' + str(len(data_list))
		if len(keep) == 0:
			raise ValueError('No files imported. Error in the data folders ?')

		data_list = data_list[keep]

		lengths = [len(z['Time']) for z in data_list]
		n = len(data_list)
		n_params = len(self.params)

		
		# TIME
		idx_max_len = numpy.where(lengths == numpy.max(lengths))[0]
		Time = data_list[idx_max_len][0]['Time']

		return Time, data_list, keep



