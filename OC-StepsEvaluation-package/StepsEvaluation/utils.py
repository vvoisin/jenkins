# !/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
	Utils module: Aviation calculations and speed conversion

	Calculator:
		- temperature
		- pressure
		- vitesse_son (speed of sound)
		- get_crossover

	Convertor:
		- From MACH: mach_to_tas, mach_to_ias, mach_to_ias_approx (an approximated version of mach_to_ias)
		- From IAS: ias_to_tas, ias_to_tas_approx (an approximated version of ias_to_tas)
		- From TAS: tas_to_mach, tas_to_ias
"""




import sys, os, traceback, cPickle, time, numpy as np
from scipy.optimize import *
from sys import exit
from math import exp


max_troposphere = 36089.2


def temperature(h_ft):
	"""
		Input:
			altitude in feet

		Output:
			temperature in Kelvin
	"""

	T0 = 288.15
	alpha = -.0019812
	if np.max(h_ft) <= max_troposphere:
		return T0 + alpha * h_ft
	else:
		return 216.65

def pressure(h_ft):
	"""
		Input:
			altitude in feet

		Output:
			temperature in Kelvin
	"""

	p0 = 101325
	alpha = -.0019812
	T0 = 288.15
	g = 9.80665
	Rs = 287.053
	p1 = 22632

	return p0 * ( 1 + alpha / T0 * h_ft )**( (-g * .3048) / (alpha*Rs) )

def vitesse_son(h_ft):
	"""
		Input:
			altitude in feet

		Output:
			speed of sound in knots
	"""
	Rs = 287.053													# J/(kg K)
	lam = 1.4
	return np.sqrt( lam * Rs * temperature(h_ft) ) / .514444

def mach_to_tas(mach, h_ft):
	"""
		Input:
			mach: mach number
			h_ft: altitude in feet
		
		Output:
			tas in knots
	"""
	return vitesse_son(h_ft) * mach

def mach_to_ias(mach, h_ft):
	"""
		Input:
			mach: mach number
			h_ft: altitude in feet
		
		Output:
			ias in knots
	"""
	
	tas = mach_to_tas(mach, h_ft)
	p = pressure(h_ft)
	p0 = 101325														# Pa
	gamma = 1.4
	vson_kts = vitesse_son(h_ft)										# en knots

	#relation TAS - CAS, eq 10, docu Cindie
	a0_kts = 661.4787												# en knots
	rap = (gamma - 1) / gamma
	irap=1/rap
	C1 = tas**2 / ( 5 * vson_kts**2 ) + 1
	C2  = C1**irap - 1
	C3 = p / p0 * C2 + 1

	return np.sqrt(5) * a0_kts * np.sqrt( C3**rap - 1 )

def mach_to_ias_approx(mach, h_ft):
	tas = mach_to_tas(mach, h_ft)
	T_S = 15 - 2 * h_ft / 1000.
	SAT_C = temperature(h_ft) - 273.15
	return tas / ( 1 + .01 * (h_ft / 600. + (SAT_C - T_S)) )

def ias_to_tas(ias, h_ft):
	"""
		Input:
			ias: in knots
			h_ft: altitude in feet
		
		Output:
			tas in knots
	"""

	p = pressure(h_ft)
	vson_kts = vitesse_son(h_ft)	# in knots
	a0 = 661.4787					# in knots
	p0 = 101325
	gamma = 1.4
	
	C0 = gamma / (gamma - 1)
	C1 = ( ias**2 / (5 * a0**2) + 1 )**C0 - 1
	C2 = (p0 / p) * C1 + 1
	C3 = C2**(1/C0) - 1
	return np.sqrt(5) * vson_kts * np.sqrt(C3)

def ias_to_tas_approx(ias, h_ft):
	"""
		Input:
			ias: in knots
			h_ft: altitude in feet
		
		Output:
			tas in knots
	"""
	return ias + h_ft / 200

def get_crossover(ias, mach, lower, upper):
	"""
		Input:
			ias: in knots
			mach number
			lower, upper: bounds for altitude in feet
		
		Output:
			Crossover altitude in feet
	"""
	fct_to_eval = lambda h_ft, ias, mach: ias_to_tas(ias, h_ft) - mach_to_tas(mach, h_ft)
	return int(bisect(fct_to_eval, lower, upper, args=(ias, mach)))

def tas_to_mach(tas, h_ft):
	"""
		Input:
			tas in knots
			h_ft: altitude in feet
		
		Output:
			mach: mach number
	"""
	return tas / vitesse_son(h_ft)

def tas_to_ias(tas, h_ft):
	"""
		Input:
			tas in knots
			h_ft: altitude in feet
		
		Output:
			ias: in knots
	"""
	p = pressure(h_ft)
	vson_kts = vitesse_son(h_ft)	# in knots
	a0 = 661.4787					# in knots
	p0 = 101325
	gamma = 1.4

	C0 = (gamma - 1) / gamma
	C1 = tas**2 / ( 5 * vson_kts**2 )  + 1
	C2  = (p / p0) * ( C1**(1/C0) - 1 ) + 1
	C3 = C2**C0 - 1

	return np.sqrt(5) * a0 * np.sqrt(C3)


