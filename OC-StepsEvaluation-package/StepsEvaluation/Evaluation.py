# !/usr/bin/env python
#  -*- coding: utf-8 -*-

import sys, os, cPickle, multiprocessing
import numpy as np

from numpy.random import randint
from sys import exit

from ConsumptionModel.Learning.Learner import Learner, gridSearch
from DataExtractor import *
from itertools import combinations
from utils import *
from math import pi, cos

class Evaluation():
	""" Evaluation function for choosing the best IAS """

	
	def __init__(self, data_path, nr_flights, output_path, regressor, ncpus=-1):
		
		self.data_path = data_path
		self.nr_flights = nr_flights
		self.output_path = output_path
		self.regressor = regressor
		self.ncpus = ncpus

		self.registrations = None
		self.value = None
		self.model = None
		self.status = 0 # state = 1 si le modèle est construit et 0 sinon

		
	def load_data(self, folder, files):

		print 'Loading data...'
		# STEP 1: Extract
		extractor = DataExtractor(params=['ALTITUDE', 'IAS', 'FF1', 'FF2', 'GW', 'GS', 'REGIST'], flight_phase='climb', FLmin=30)
		Time, data_list, keep = extractor.run(files, folder, ncpus=self.ncpus)

		ALTITUDE = [z['ALTITUDE'] for z in data_list]
		IAS = [z['IAS'] for z in data_list]
		FF1 = [z['FF1'] for z in data_list]
		FF2 = [z['FF2'] for z in data_list]
		GW = [z['GW'] for z in data_list]
		GS = [z['GS'] for z in data_list]



		Regist = np.array([z['REGIST'] for z in data_list])
		Regist_dummy = dummy(Regist)
		self.registrations = Regist_dummy[1]
		nr_regist = len(Regist_dummy[1])

		nr_times = [len(z) for z in ALTITUDE]
		n = len(ALTITUDE)

		# Step 2: BUILD TRAIN SET
		# METHOD 1 : un temps pris à chaque vol
		indexes = [randint(i) for i in nr_times]

		designMatrix = np.concatenate((
			np.array([ALTITUDE[i][j] for i,j in enumerate(indexes)]).reshape((n,1)),
			np.array([IAS[i][j] for i,j in enumerate(indexes)]).reshape((n,1)),
			np.array([GW[i][j] / 2.2 for i,j in enumerate(indexes)]).reshape((n,1)),							# GW: lbs --> kg
			Regist_dummy[0] ), axis=1)

		fuel_flow = np.array([ FF1[i][j] / (2.2*3600) + FF2[i][j] / (2.2*3600) for i,j in enumerate(indexes) ]) # FF: pph --> kg/s
		distance = np.array([ np.cumsum(GS[i])[j] / 1000 for i,j in enumerate(indexes)])

		return designMatrix, fuel_flow, distance


	def load_and_learn(self):
		"""
			Importation des données QAR et apprentissage de modèle pour la prédiction de la consommation et de la distance 
			en fonction des données d'entrée (ias, altitude, masse)

			Return : état
		"""

		# IMPORT FLIGHT DATA
		files = np.array(os.listdir(self.data_path))
		
		nr_flights = self.nr_flights
		if nr_flights > 0:
			files = files[0:nr_flights]

		designMatrix, consumption, distance = self.load_data(self.data_path, files)

		### REGRESSION MODELS
		# INSTANTIATE
		print '\tConsumption model'
		if self.regressor in ['ETR', 'RF']:
			model_consumption = Learner(self.regressor, n_estimators=500, n_jobs=self.ncpus)
			model_consumption.train(designMatrix, consumption)
			
		elif self.regressor == 'SVR':

			# TRAIN
			print 'Consumption model calibration'
			C_range = np.logspace(-2, 10, 3)
			gamma_range = np.logspace(-9, 3, 3)
			eps_range = [.01, .1, 1]
			param_grid = dict(C=C_range, kernel=['rbf'], gamma=gamma_range, epsilon=eps_range)

			if self.ncpus == -1: n_jobs = multiprocessing.cpu_count()
			else: n_jobs = self.ncpus

			model_consumption, best_params = gridSearch('SVR', designMatrix, consumption, param_grid=param_grid, n_jobs=n_jobs)
			print 'Best parameters:'
			print best_params

		print 'Done'
		self.model = model_consumption
		self.status = 1


	def serialize(self, name):
		"""
			Fonction de sérialisation de l'objet courant. Dès que le web service est lancé et si le modèle est construit, 
			alors j'importe l'objet et j'attend les données pour la prédiction
		"""
		print 'Serialize current object'
		with open(self.output_path + name + '.object', 'w') as f:
			cPickle.dump(self, f)


	def evaluation(self, alt_min, alt_max, tas_profile, gamma_profile, time_profile, mass_flmin, registration, min_alt_step, ff_cruise, mach_cruise=None, ias_min=None, ias_max=None, mach_min=None, mach_max=None):
		"""
			Evaluation a set of steps (altitudes, IAS and Mach speeds) based on statistical models:
				- Step 1 : models importations
				- Step 2 : indicators evaluation

			Input parameters:	
			 
			 alt_min, alt_max: bounds for altitude in feet (from Bocop), float
			 tas_profile: TAS profile in knots (from Bocop), list
			 gamma_profile: gamma profile (from Bocop), list
			 time_profile: time profile (from Bocop), list
			 mass_flmin: mass at the first altitude in kg (from Bocop), float
			 registration: aircraft registration, str
			 min_alt_step: bound in altitude for steps, int
			 ff_cruise: fuel flow in cruise in kg/s, float
			 mach_cruise: mach number in cruise, set by the user, if not None, generate 3 IAS and the mach at crossover
			
			Output values:			
			 steps_opt_eval: optimal steps
			 index_opt_eval: evaluation index of optimal steps
			 list_of_steps: list of generated steps
			 list_of_indexes: indexes of generated steps
		"""

		### get models
		model_consumption = self.model

		N = len(tas_profile)

		# Get registrations
		if registration not in self.registrations:
			print 'Warning: Registration not in historical data. The model may be not reliable.'

		registration_dummy = dummy_one(registration, self.registrations)[0].tolist()
		registration_dummy = np.array([registration_dummy for _ in range(N)])


		### Bocop profile
		# Data
		alt_profile = np.linspace(alt_min, alt_max, N)
		ias_profile = tas_to_ias(tas_profile, alt_profile)
		mach_profile = tas_to_mach(tas_profile, alt_profile)

		if np.max(ias_profile) > ias_max:
			print np.max(ias_profile)
			raise ValueError('ias_max', 1)
		if np.min(ias_profile) < ias_min:
			print np.min(ias_profile)
			raise ValueError('ias_min', 1)
		if np.max(mach_profile) > mach_max:
			print np.max(mach_profile)
			raise ValueError('mach_max', 1)
		if np.min(mach_profile) < mach_min:
			print np.min(mach_profile)
			raise ValueError('mach_min', 1)
		if (mach_cruise is not None) and (mach_cruise > mach_max):
			print mach_cruise
			raise ValueError('mach_cruise', 1)
		if (mach_cruise is not None) and (mach_cruise < .7):
			print mach_cruise
			raise ValueError('mach_cruise', 1)

		GW = mass_flmin + np.zeros(N)
		newx_profile = np.concatenate((alt_profile.reshape((-1,1)), ias_profile.reshape((-1,1)), GW.reshape((-1,1)), registration_dummy), axis=1)

		# Consumption in kg
		pred_profile_consumption = model_consumption.predict(newx_profile)
		total_profile_consumption = get_total_consumption(pred_profile_consumption, time_profile)
		# Distance in m
		dist_profile = compute_distance(tas_profile * .5144444, gamma_profile, time_profile)
		total_profile_dist = dist_profile[-1]


		### Generate steps and evaluation
		initial_level = int(np.min(alt_profile) / 100)
		final_level = int(np.max(alt_profile) / 100)
		list_of_steps = self.generate_steps(alt_profile, ias_profile, mach_profile, initial_level, final_level, min_alt_step=min_alt_step, mach_cruise=mach_cruise, ias_min=ias_min, ias_max=ias_max, mach_min=mach_min, mach_max=mach_max)

		def eval_indexes(steps,i):
			### Consign profile
			# Data
			consign_profile, idx_mach = self.steps_to_profile(steps, alt_profile, ias_profile)
			alt_step_mach = max([z['alt_begin'] for z in steps])
			mach_step = steps[idx_mach]['mach']
			
			# Testing operating speeds
			if np.min(consign_profile) < ias_min or np.max(consign_profile) > ias_max or np.max(mach_step) > mach_max or np.min(mach_step) < mach_min:
				return [float('Inf')] * 5

			newx_consign = np.concatenate((	alt_profile.reshape((-1,1)), 
											consign_profile.reshape((-1,1)), 
											GW.reshape((-1,1)), 
											registration_dummy), axis=1)

			# Consumption in kg
			pred_consign_consumption = model_consumption.predict(newx_consign)
			total_consign_consumption = get_total_consumption(pred_consign_consumption, time_profile)
			# Distance in m
			tas_consign = ias_to_tas(consign_profile, alt_profile)
			dist_consign = compute_distance(tas_consign * .5144444, gamma_profile, time_profile)
			total_consign_dist = dist_consign[-1]

			# TRANSITION
			transition_consumption, transition_distance = get_transition_costs(steps, pred_profile_consumption, alt_profile)


			### EVALUATION CRITERIA
			# MODELS
			ff_cruise_recal = ff_cruise # .5
			V_cruise = tas_profile[-1] # 465

			# CONSUMPTION CRITERION
			delta_consumption = total_consign_consumption - total_profile_consumption
			crit_consumption = delta_consumption + transition_consumption

			# DISTANCE CRITERION
			delta_distance = total_consign_dist - total_profile_dist
			crit_distance = delta_distance + transition_distance
			K = ff_cruise_recal / (V_cruise * .5144444)

			# OVERALL EVALUATION INDEX
			eval_index_all = crit_consumption - crit_distance * K

			return [eval_index_all, delta_consumption, delta_distance, transition_consumption, transition_distance]

		list_of_indexes	= np.array([eval_indexes(z,i) for i,z in enumerate(list_of_steps)])

		# minimum indexes of all of the criterion
		idx_opt = np.argmin(list_of_indexes, axis=0)
		idx_opt_eval = idx_opt[0]
		index_opt_eval = list_of_indexes[idx_opt_eval,0]
		steps_opt_eval = list_of_steps[idx_opt_eval]
		
		return steps_opt_eval, index_opt_eval, list_of_steps, list_of_indexes



	def steps_to_profile(self, steps, alt_profile, ias_profile):
		"""
			Compute the steps profile, i.e. piecewise constant.

			Input parameters:	
			 steps: a list of steps, i.e. dictionaries with the format  {'alt_begin':..., 'alt_end':..., 'ias':...} for IAS steps and {'alt_begin':..., 'alt_end':..., 'mach':...} for Mach step.
			 alt_profile: altitude profile in feet (from Bocop), list

			Output value:			
			 piecewise constant profile, list
		"""
		
		# get IAS steps
		is_mach = ['mach' in z.keys() for z in steps]
		if True not in is_mach:
			raise ValueError('Mach step not found.')
		idx = is_mach.index(True)

		if idx < 2:
			raise KeyError('Error in "steps" list: it must contain at least 2 IAS steps and 1 Mach step ')

		ias_steps_list = steps[0:idx]
		# get altitude steps
		alt_steps = [z['alt_begin'] for z in steps]
		alt_steps.pop(0)
		ias_steps = [z['ias'] for z in ias_steps_list]
		
		# get numbers of points within the bounds
		n_cs = [np.sum(alt_profile <= z) for z in alt_steps]
		n_cs = [0] + n_cs
		n_cs = np.diff(n_cs)

		consign_profile = np.array([ [ias_steps[i]] * z for i,z in enumerate(n_cs)] + ias_profile[alt_profile > alt_steps[-1]].tolist())
		consign_profile = []
		for i,z in enumerate(n_cs):
			consign_profile = consign_profile + [ias_steps[i]] * z
		
		# ADD IAS FOR MACH STEP
		mach_step = steps[2]['mach']
		new_size = len(consign_profile)
		ias_mach_step = [mach_to_ias(mach_step, z) for z in alt_profile[new_size:]]
		consign_profile += ias_mach_step

		return np.array(consign_profile), idx
	

	def generate_steps(self, alt_profile, ias_profile, mach_profile, initial_level, final_level, min_alt_step, mach_cruise=None, ias_min=None, ias_max=None, mach_min=None, mach_max=None):
		"""
			Split steps

			Input parameters:	
			 ias_profile, mach_profile: IAS and Mach profile (from Bocop), list
			 initial_level, final_level: bounds for altitude, int
			 min_alt_step: bound in altitude for steps, int
			 mach_cruise: mach number in cruise, set by the user, if not None, generate 3 IAS and the mach at crossover
			 ias_min: lower bound for IAS speed
			 ias_max: upper bound for IAS speed (VMO)
			 mach_min: lower bound for MACH speed
			 mach_max: upper bound for MACH speed (MMO)

			Output values:		
			 a list of steps. A step is a list of dictionaries of the format {'alt_begin':..., 'alt_end':..., 'ias':...} for IAS and {'alt_begin':..., 'alt_end':..., 'mach':...} for Mach.
			 The number of dictionaries is given by the strategy. 
			 Ex: "steps": [ {"alt_begin" : 10000, "alt_end" : 13600, "ias" : 233},
	           				{"alt_begin" : 13600, "alt_end" : 31500, "ias" : 270},
	           				{"alt_begin" : 31500, "alt_end" : 36700, "mach" : 0.77} ]
		"""

		def fct(z):
			# ias_steps = [np.median(ias_profile[alt_profile < z[0]*100])] + [np.median(ias_profile[(alt_profile >= z[i-1]*100) & (alt_profile < zz*100)]) for i,zz in enumerate(z) if zz != z[0]]
			ias_step_1 = np.median(ias_profile[alt_profile < z[0]*100])
			ias_step_2 = np.median(ias_profile[ ( alt_profile >= z[0]*100 ) & (alt_profile < z[1]*100) ])
			mach_step = np.median(mach_profile[ alt_profile >= z[1]*100 ])


			# BOUNDS FOR MACH (STEP 3) AND IAS (STEP 1)
			# mach_step_min = .7
			if mach_step < mach_step_min:
				return None
			if (initial_level < 100) & (ias_step_1 > 250):
				ias_steps[0] = 250

			step = 	[{'alt_begin':initial_level*100, 'alt_end':z[0]*100, 'ias':ias_step_1}, {'alt_begin':z[0]*100, 'alt_end':z[1]*100, 'ias':ias_step_2}] + [{'alt_begin':z[1]*100, 'alt_end':final_level*100, 'mach':mach_step}]
			return step


		def fct_crossover(z, min_alt_step):
			ias_step_1 = np.median(ias_profile[alt_profile < z[0]*100])
			ias_step_2 = np.median(ias_profile[ ( alt_profile >= z[0]*100 ) & (alt_profile < z[1]*100) ])
			
			# BOUNDS FOR IAS (STEP 1)
			if (initial_level < 100) & (ias_step_1 > 250):
				ias_step_1 = 250

			try:
				crossover = get_crossover(ias=ias_step_2, mach=mach_cruise, lower=alt_profile[0], upper=alt_profile[-1])
			except ValueError as err:
				print '\nIn function get_crossover:'
				print err + '\n'
				raise ValueError('Fail to compute the crossover altitude...')
			if crossover <= (z[-1] + min_alt_step)*100 or crossover >= final_level*100:
				return None
			else:
				ias_step_2 = np.median(ias_profile[(alt_profile >= z[-1]*100) & (alt_profile < crossover)])
				# new_crossover = crossover
				new_crossover = get_crossover(ias=ias_step_2, mach=mach_cruise, lower=alt_profile[0], upper=alt_profile[-1])

				step = 	[{'alt_begin':initial_level*100, 'alt_end':z[0]*100, 'ias':ias_step_1}, {'alt_begin':z[0]*100, 'alt_end':new_crossover, 'ias':ias_step_2}, {'alt_begin':new_crossover, 'alt_end':final_level*100, 'mach':mach_cruise}]
				return step

		def fct_crossover_3_steps(z, min_alt_step):
			ias_steps = [np.mean(ias_profile[alt_profile < z[0]*100])] + [np.mean(ias_profile[(alt_profile >= z[i-1]*100) & (alt_profile < zz*100)]) for i,zz in enumerate(z) if zz != z[0]]
			ias_step_next = np.mean(ias_profile[(alt_profile >= z[-1]*100)])

			crossover = get_crossover(ias=ias_step_next, mach=mach_cruise, lower=alt_profile[0], upper=alt_profile[-1])
			if crossover <= (z[-1] + min_alt_step)*100 or crossover >= final_level*100:
				return None
			else:
				ias_step_next = np.mean(ias_profile[(alt_profile >= z[-1]*100) & (alt_profile < crossover)])
				new_crossover = get_crossover(ias=ias_step_next, mach=mach_cruise, lower=alt_profile[0], upper=alt_profile[-1])
				step = [{'alt_begin':initial_level*100, 'alt_end':z[0]*100, 'ias':ias_steps[0]}] + [{'alt_begin':z[i-1]*100, 'alt_end':z[i]*100, 'ias':ias_step} for i,ias_step in enumerate(ias_steps) if ias_step != ias_steps[0]] + [{'alt_begin':z[-1]*100, 'alt_end':new_crossover, 'ias':ias_step_next}] + [{'alt_begin':new_crossover, 'alt_end':final_level*100, 'mach':mach_cruise}]
				return step


		by = 5
		split_levels = [z for z in combinations(np.arange(initial_level + by, final_level - by, by), 2) if (z[1] - z[0] >= min_alt_step) & (z[0] - initial_level >= min_alt_step) & (final_level - z[1] >= min_alt_step)]

		if mach_cruise is None: 
			list_of_steps = [fct(z) for z in split_levels]
			list_of_steps = filter(None, list_of_steps)
		else:
			list_of_steps = [fct_crossover(z, min_alt_step) for z in split_levels]
			list_of_steps = filter(None, list_of_steps)

		if len(list_of_steps) == 0:
			raise ValueError('No steps generated. It may be due to the values of the values of operating speeds (mach) or to the crossover computation.', 1)

		print str(len(list_of_steps)) + ' generated steps'

		return list_of_steps


def dummy(x):
	cat = np.unique(x)
	M = []
	for z in cat:
		tmp = np.array([int(zz) for zz in (x==z)])
		M.append(tmp)
	return (np.array(M).T, cat)


def dummy_one(x, cat):

	M = [int(x == z) for z in cat]
	return (np.array(M), cat)


def compute_distance(tas, gamma, time):
	"""
		- tas in m/s
		- gamma in degrees
		- time in seconds
	"""
	cos_gamma = np.array([cos(z*pi/180) for z in gamma])
	V = tas * cos_gamma
	dist = [0] + []
	for i in range(1,len(time)):
		dist.append( (time[i] - time[i-1]) * V[i] )
	return np.cumsum(dist)


def get_total_consumption(fuel_flow, time):
	"""
		- fuel flow in kg/s
		- time in seconds
	"""
	total_ff = [0] + []
	for i in range(1,len(time)):
		total_ff.append( (time[i] - time[i-1]) * fuel_flow[i] )
	return np.sum(total_ff)


def get_transition_costs(steps, fuel_flow, altitude):
	
	# TIME (in fct of delta speed)
	a = 1.33235534281
	b = 0.604585267945
	transition_time_fct = lambda x: x * a + b

	# Step 1 to step 2
	transition_alt = steps[0]['alt_end']
	tas_st1 = ias_to_tas(steps[0]['ias'], transition_alt)
	tas_st2 = ias_to_tas(steps[1]['ias'], transition_alt)
	idx_1_to_2 = np.where(altitude >= transition_alt)[0][0]
	transition_time = transition_time_fct(tas_st2 - tas_st1)
	transition_consumption = transition_time * fuel_flow[idx_1_to_2]
	transition_distance = transition_time * (tas_st1 + tas_st2) / 2

	# Step 2 to step 3
	transition_alt = steps[1]['alt_end']
	tas_st2 = ias_to_tas(steps[1]['ias'], transition_alt)
	tas_st3 = mach_to_tas(steps[2]['mach'], transition_alt)
	idx_2_to_3 = np.where(altitude >= transition_alt)[0][0]
	transition_time = transition_time_fct(tas_st3 - tas_st2)
	transition_consumption += transition_time * fuel_flow[idx_2_to_3]
	transition_distance += transition_time * (tas_st2 + tas_st3) / 2

	return transition_consumption, transition_distance
