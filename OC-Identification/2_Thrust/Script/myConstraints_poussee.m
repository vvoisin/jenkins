function [ Aeq, Beq ] = myConstraints_poussee( Fpath, Fname,moteur, hmin)
%myConstraints Summary of this function goes here
%   Detailed explanation goes here

    % engine A constants 
        F0 = moteur(1);
        BPR = moteur(2);

        % atmosphere constants
        rho0 = 1.225;
        %rho0 = 1;
        T0 = 288.15;

    % read csv data in file name
    vol = readtable(strcat(Fpath,Fname));
    
    % now read table columns and convert them to vectors
    h_m = table2array(vol(:,'alt_stdc_m'));
    
    tmin = find(h_m >= hmin, 1, 'first');
    if(hmin < 10000*0.3048)
        hmax = 10000*0.3048;
    else
        hmax = max(h_m);
    end
    tmax = find(h_m >= hmax, 1, 'first');
    
    h_m = table2array(vol(tmin:tmax,'alt_stdc_m'));
    Mach = table2array(vol(tmin:tmax,'MACHlisse'));
    N1 = table2array(vol(tmin:tmax,'n11c_pc'));
    N2 = table2array(vol(tmin:tmax,'n12c_pc'));
    ff1 = table2array(vol(tmin:tmax,'ff1c_kgs'));
    ff2 = table2array(vol(tmin:tmax,'ff2c_kgs'));
   % SATk = table2array(vol(:,'SATkelvin')); % SAT standard
    SATk = table2array(vol(tmin:tmax,'sat')); % SAT = f(Mach, TAT)
   rho_SI = table2array(vol(tmin:tmax,'rho_SI'));% masse volumique de l'air humide
    %rho_SI = 1.292 * SATk/273.15; % masse volumique de l'air sec
    P_Pa_SI = table2array(vol(tmin:tmax,'P_Pa_SI'));
   % phi = 0.76;
   % rho_SI = 1./(287.06*SATk).*(P_Pa_SI - ... % masse volumique de l'air humide
   %          230.617 * phi * exp(17.5043*(SATk-273.15)./(241.2+SATk-273.15)));
   


        CSR = ((-3.35*1e-10 * h_m + 1.54*1e-5) .* Mach +...
              (-2.05*1e-11 * h_m - 4.54*1e-7) * BPR .* ones(length(Mach),1) +...
              (3.47*1e-10 * h_m + 1.08*1e-5)) .* sqrt(SATk/T0);

    va1 = CSR .* (N1 + N2) * F0 .* power(rho_SI/rho0,0.6) .* power(Mach,3);
    va2 = CSR .* (N1 + N2) * F0 .* power(rho_SI/rho0,0.6) .* ones(length(Mach),1);
    Aeq = [va1 va2];
    Beq = (ff1+ff2);
end

