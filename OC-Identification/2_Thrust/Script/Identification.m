%% This computes a set of Thrust parameters for each flight in the given DB

% USER INPUT
ROOT = './';
    %% engine A constants  B737-800 CFMI CFM56-7B27
    %    F0 = 121400; % in Newtons
    %    BPR = 5.1;
    % engine A constants  A320-200 CFMI CFM56-5B4/P
        F0 = 120100; % in Newtons
        BPR = 5.7;
        moteur = [F0, BPR];

%%
Step2 = '2_MatlabComp/2_Thrust/';
Path_STEP2 = strcat(ROOT,Step2);

Path_script = strcat(Path_STEP2,'Script/');
Path_annexe = strcat(Path_STEP2,'Annexe/');
Path_figure = strcat(Path_STEP2,'Figures/');
Path_output = strcat(Path_STEP2,'Output/');
Path_input  = strcat(Path_STEP2,'Input/');

%annexe_immat = strcat(Path_annexe,'Figures/by_immat/');
%output_immat = strcat(Path_output,'by_immat/');

%setenv(matlabpath, Path_script);
addpath(Path_script);

% Constants

        % atmosphere constants
        rho0 = 1.225;
        T0 = 288.15;

%% Set solver options

opts = optimset(@fmincon);
opts.Display = 'iter';
%opts.PlotFcns = 'on';
opts.TolX = 1e-4;
opts.TolFun = 1e-2;
%opts.Algorithm = ''; % for fmincon
%opts.Algorithm = 'sqp'; % for fminunc
%opts.ObjectiveLimit = -1e+6;
opts.MaxIter = 500;
%opts.GradObj = 'on';
opts.TolCon = 1e-1;
opts.MaxFunEvals = 600;

%% Declare variables
%immats_file = readtable(strcat(Path_input ,'Immats_TAF_B737-800.csv'));
%immats = (table2array(immats_file(:,'x')));
%err_total = zeros(length(immats),1);
tic
%for k = 1:1:length(immats)
        path = Path_input;%strcat(Path_input,'by_immat/',immats(k),'/'); % by DB immat
        % Immats_data = readtable(strcat(path, 'Flight_Immats_A320.txt'), 'ReadVariableNames', false, 'Delimiter',' ');
        % immat = Immats_data.Var2;
        
        % File_name_vector = cellstr(string_flids);
        File_path = char(path);
        
        % Get file names in the current directory
        string_flids = dir(strcat(File_path));
        string_flids = { string_flids.name };
        string_flids = string_flids(3:length(string_flids));
        
        % Set bounds
        lb = [-50 -50];
        ub = [50 50];
        
        % File_name = %by DB_immat
        Nfile=length(string_flids);fileStep=1;
        mPoussee0 = [-10 -10];
        parametres_poussee = zeros(Nfile,2);
        parametres_drag    = zeros(Nfile,9);
        parametres_lift    = zeros(Nfile,9);
                       
                NfileTest = Nfile;
               % RMSE_P7 = zeros(NfileTest,NfileTest);
                err_app = zeros(NfileTest,NfileTest);
                err_all = zeros(NfileTest,1);
                
        for i = 1:1:Nfile
                %% Thrust identification
                % identify a set of thrust parameters per flight, for the given DB
                %File_name = char(File_name_vector(i));
                %File_name = strcat('climb-',num2str(i-1),'.csv');
                File_name = string_flids{i};
                
                % Solve for Thrust parameters
                % My contraints for finding x, with Ax = b
                [Aeq_p, Beq_p] = myConstraints_poussee(File_path, File_name,moteur);
                
                % Calculate the new optimization problem
                %mPoussee0 = coeffs_poussee;
                MyObjectivePoussee = @(coeffs_poussee) minPoussee(mPoussee0, File_path, File_name,moteur);
                [coeffs_poussee,objVal,flag] = fmincon( MyObjectivePoussee, mPoussee0,[],[],Aeq_p,Beq_p,lb,ub,@tiltellipse, opts);
                
                % Store parameters
                mPoussee0 = coeffs_poussee; % update future start with current sol
                parametres_poussee(i,:) = coeffs_poussee;
                

                %for file_nr = 0: 1 :NfileTest-1

                [ RMSE_P7 ] = modelValidation(mPoussee0, File_path,moteur)';

                % end

                err_app(:,i) = RMSE_P7;
                err_all(i) = min(err_app(:,i));

                %  err_app = median(RMSE_P7(1:5))
                %
                %                 plot((1: fileStep : NfileTest),RMSE_P7,'DisplayName','RMSE Poussee');
                %                 xlabel('File number');
                %                 ylabel('RMSE');ylim([-0.2 0.4]);
                %                 hold on;
                %                 plot((1: fileStep : NfileTest),RMSE_P7(i) * ones(1,length(1: fileStep :NfileTest)));
                %                 legend('RMSE variation',strcat('RMSE app', num2str(err_app(i))))
                %                 title('RMSE Variation')
                %                 hold off
                %                 saveas( gcf, strcat('fig/',char(immats(k)),'_RMSE-on_',strtok(File_name,'.')), 'jpg');

        end
        
        err_total(k) = min(err_all);
        
        index = find(err_all == err_total(k));
        p_chosen = parametres_poussee(index(1),:);
        
        plot(err_all,'b');%xlim([1,NfileTest]);
        title(strcat('Thrust error for ',File_name))
        hold on
        plot(err_total(k)*ones(NfileTest,1),'r')
        legend('All DB err for given params set','Minimum error that gives the most suitable params','Location','NorthWest')
        hold off
        saveas( gcf, strcat(annexe_immat,char(immats(k)),'/Thrust-RMSE_',char(immats(k))), 'jpg');
%         
%         err_all(~err_all)=Inf;
%         index = find(err_all == min(err_all(3:NfileTest)));
%         p_chosen = parametres_poussee(index(1),:);
%         dlmwrite(strcat('fig/params_Thrust_',char(immats(k)),'.txt'),parametres_poussee(3:Nfile-1,:));
%         
%         plot((1: fileStep : NfileTest),err_all,'DisplayName','RMSE Poussee');
%         xlabel('File number');
%         ylabel('RMSE');ylim([-0.2 0.4]);
%         hold on;
%         plot((1: fileStep : NfileTest),min(err_all) * ones(1,length(1: fileStep :NfileTest)));
%         legend('RMSE variation',strcat('RMSE min global',num2str(min(err_all))))
%         title('RMSE Variation')
%         hold off
%         saveas( gcf, strcat('fig/',char(immats(k)),'_RMSE-global'), 'jpg');
%         
%         err_all(~err_all)=Inf;
%         chosen_index = find(err_all == min(err_all));
%         %parametres_poussee(chosen_index)
         dlmwrite(strcat(output_immat,'chosen_params_Thrust_',char(immats(k)),'.txt'),p_chosen,' '); 
         dlmwrite(strcat(annexe_immat,char(immats(k)),'/all_params_Thrust_',char(immats(k)),'.txt'),parametres_poussee,' ');
         dlmwrite(strcat(annexe_immat,char(immats(k)),'/all_medianRMSE_Thrust_',char(immats(k)),'.txt'),err_all,' ');

%end
toc

%File_path='/home/anamaria/Desktop/AigleAzur/';
%dlmwrite(strcat('fig/v02.18_poussee_idALL.txt'),parametres_poussee,' ');
%dlmwrite(strcat(File_path,'v02.04_drag.txt'),parametres_drag,' ');
%dlmwrite(strcat(File_path,'v02.04_lift.txt'),parametres_lift,' ');

