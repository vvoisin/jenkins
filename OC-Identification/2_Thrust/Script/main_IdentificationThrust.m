%% This computes a set of Thrust parameters for each flight in the given DB

% USER INPUT
%ROOT = '/home/anamaria/MATLAB/Analyse_FL030-TOC/';
%ROOT = '/home/anamaria/Desktop/Test_STEPS/';
ROOT = './';
   %% engine A constants  B737-800 CFMI CFM56-7B27
    %    F0 = 121400; % in Newtons
    %    BPR = 5.1;
    % engine A constants  A320-200 CFMI CFM56-5B4/P
        F0 = 120100; % in Newtons
        BPR = 5.7;
        moteur = [F0, BPR];
%date = '23.11_hinfFL150';%'23.11_hsup'
%%
Step2 = '2_Thrust/';
Path_STEP2 = strcat(ROOT,Step2);
%Path_STEP2 = '';

Path_script = strcat(Path_STEP2,'Script/');
Path_annexe = strcat(Path_STEP2,'Annexe/');
Path_output = strcat(Path_STEP2,'Output/');
Path_figure = strcat(Path_STEP2,'Figures/');
Path_input  = strcat(Path_STEP2,'Input/');
%thrust_id_real_atmosphere_sans_palier

%annexe_immat = strcat(Path_annexe,'Figures/thrust_id_real_atmosphere_sans_palier/');
output_immat = strcat(Path_output);

%setenv(matlabpath, Path_script);
addpath(Path_script);

% Constants

        % atmosphere constants
        rho0 = 1.225;
        T0 = 288.15;

%% Set solver options

opts = optimset(@fmincon);
opts.Display = 'iter';
%opts.PlotFcns = 'on';
opts.TolX = 1e-4;
opts.TolFun = 1e-2;
%opts.Algorithm = ''; % for fmincon
%opts.Algorithm = 'sqp'; % for fminunc
%opts.ObjectiveLimit = -1e+6;
opts.MaxIter = 500;
%opts.GradObj = 'on';
opts.TolCon = 1e-1;
opts.MaxFunEvals = 600;

%% Declare variables
% immats_file = readtable(strcat(Path_input ,'Immats_TAF_B737-800.csv'));
% immats = (table2array(immats_file(:,'x')));
% err_total = zeros(length(immats),1);
% tic
% for k = 1:1:length(immats)
        path = strcat(Path_input); % by DB immat
        % Immats_data = readtable(strcat(path, 'Flight_Immats_A320.txt'), 'ReadVariableNames', false, 'Delimiter',' ');
        % immat = Immats_data.Var2;
        
        % File_name_vector = cellstr(string_flids);
        File_path = char(path);
        
        % Get file names in the current directory
        string_flids = dir(strcat(File_path));
        string_flids = { string_flids.name };
        string_flids = string_flids(3:length(string_flids));
        
        % Set bounds
        lb = [-50 -50];
        ub = [50 50];
        
        % File_name = %by DB_immat
        Nfile=length(string_flids);fileStep=1;
        mPoussee0 = [1 1];
        parametres_poussee = zeros(Nfile,2);
        parametres_drag    = zeros(Nfile,9);
        parametres_lift    = zeros(Nfile,9);
                       
                NfileTest = Nfile;
               % RMSE_P7 = zeros(NfileTest,NfileTest);
                err_app = zeros(NfileTest,NfileTest);
                err_all = zeros(NfileTest,1);
% Id FL030-FL100_____________________________________________________________________________
hmin =  3000 * 0.3048;
%hmin = 15000 * 0.3048; 
printok = false;               
        for i = 1:1:Nfile
                %% Thrust identification
                % identify a set of thrust parameters per flight, for the given DB
                File_name = string_flids{i};
                
                % Solve for Thrust parameters
                % My contraints for finding x, with Ax = b
                [Aeq_p, Beq_p] = myConstraints_poussee(File_path, File_name,moteur,hmin);
                
                % Calculate the new optimization problem
                MyObjectivePoussee = @(coeffs_poussee) minPoussee(mPoussee0, File_path, File_name,moteur,hmin);
                [coeffs_poussee,objVal,flag] = fmincon( MyObjectivePoussee, mPoussee0,[],[],Aeq_p,Beq_p,lb,ub,@tiltellipse, opts);
                
                % Store parameters
                mPoussee0 = coeffs_poussee; % update future start with current sol
                parametres_poussee(i,:) = coeffs_poussee;

                [ RMSE_P7 ] = modelValidation(mPoussee0, Path_figure, File_path,moteur,File_name,hmin,printok)';

                err_app(:,i) = RMSE_P7;
                err_all(i) = mean(err_app(:,i));
        
        end
        
        err_total = min(err_all);
        
        index = find(err_all == err_total);
        p_chosen = parametres_poussee(index(1),:);
        
        printok = true;
        [ RMSE_P7 ] = modelValidation(p_chosen, Path_figure, File_path,moteur,File_name,hmin,printok)';

        plot(err_all,'b');%xlim([1,NfileTest]);
        title(strcat('Thrust error for ',File_name))
        hold on
        plot(err_total*ones(NfileTest,1),'r')
        legend('All DB err for given params set','Minimum error that gives the most suitable params FL030-TOC','Location','NorthWest')
        uicontrol('Style', 'text',...
                'String', strcat('Min( ',char(949),') =',num2str(err_total,3)),... %replace something with the text you want
                'Units','normalized',...
                'Position', [0.7 0.5 0.1 0.1]); 
        hold off
        saveas( gcf, strcat(Path_figure,'Thrust-RMSE_FL030-TOC'),'jpg');
  
        plot((1: fileStep : NfileTest),RMSE_P7,'DisplayName','RMSE Poussee');
        xlabel('File number');
        ylabel('RMSE');ylim([-0.2 0.4]);
        hold on;
        plot((1: fileStep : NfileTest),mean(RMSE_P7) * ones(1,length(1: fileStep :NfileTest)));
        legend('RMSE variation',strcat('RMSE mean global',num2str(mean(RMSE_P7))))
        title('RMSE Variation')
        hold off
        saveas( gcf, strcat(Path_figure,'chosen','_RMSE-global_FL030-TOC'), 'jpg');

         dlmwrite(strcat(Path_output,'chosen_params_Thrust_FL030-FL100.txt'),p_chosen,' '); 
         dlmwrite(strcat(Path_annexe,'all_params_Thrust_FL030-FL100.txt'),parametres_poussee,' ');
         dlmwrite(strcat(Path_annexe,'all_medianRMSE_Thrust_FL030-FL100.txt'),err_all,' ');


% Id FL100-TOC_____________________________________________________________________________
        Nfile=length(string_flids);fileStep=1;
        mPoussee0 = [1 1];
        parametres_poussee = zeros(Nfile,2);
                       
                NfileTest = Nfile;
                err_app = zeros(NfileTest,NfileTest);
                err_all = zeros(NfileTest,1);
hmin = 10000 * 0.3048; 
printok = false;               
        for i = 1:1:Nfile
                %% Thrust identification
                % identify a set of thrust parameters per flight, for the given DB
                File_name = string_flids{i};
                
                % Solve for Thrust parameters
                % My contraints for finding x, with Ax = b
                [Aeq_p, Beq_p] = myConstraints_poussee(File_path, File_name,moteur,hmin);
                
                % Calculate the new optimization problem
                MyObjectivePoussee = @(coeffs_poussee) minPoussee(mPoussee0, File_path, File_name,moteur,hmin);
                [coeffs_poussee,objVal,flag] = fmincon( MyObjectivePoussee, mPoussee0,[],[],Aeq_p,Beq_p,lb,ub,@tiltellipse, opts);
                
                % Store parameters
                mPoussee0 = coeffs_poussee; % update future start with current sol
                parametres_poussee(i,:) = coeffs_poussee;

                [ RMSE_P7 ] = modelValidation(mPoussee0, Path_figure, File_path,moteur,File_name,hmin,printok)';

                err_app(:,i) = RMSE_P7;
                err_all(i) = mean(err_app(:,i));

        end
        
        err_total = min(err_all);
        
        index = find(err_all == err_total);
        p_chosen = parametres_poussee(index(1),:);
        
        printok = true;
        [ RMSE_P7 ] = modelValidation(p_chosen, Path_figure, File_path,moteur,File_name,hmin,printok)';

        plot(err_all,'b');%xlim([1,NfileTest]);
        title(strcat('Thrust error for ',File_name))
        hold on
        plot(err_total*ones(NfileTest,1),'r')
        legend('All DB err for given params set','Minimum error that gives the most suitable params','Location','NorthWest')
        uicontrol('Style', 'text',...
                'String', strcat('Min( ',char(949),') =',num2str(err_total,3)),... %replace something with the text you want
                'Units','normalized',...
                'Position', [0.7 0.5 0.1 0.1]); 
        hold off
        saveas( gcf, strcat(Path_annexe,'Thrust-RMSE_FL100-TOC'),'jpg');

        plot((1: fileStep : NfileTest),RMSE_P7,'DisplayName','RMSE Poussee');
        xlabel('File number');
        ylabel('RMSE');ylim([-0.2 0.4]);
        hold on;
        plot((1: fileStep : NfileTest),mean(RMSE_P7) * ones(1,length(1: fileStep :NfileTest)));
        legend('RMSE variation',strcat('RMSE mean global',num2str(mean(RMSE_P7))))
        title('RMSE Variation')
        hold off
        saveas( gcf, strcat(Path_figure,'chosen','_RMSE-global_FL100_TOC'), 'jpg');
  
         dlmwrite(strcat(Path_output,'chosen_params_Thrust_FL100-TOC.txt'),p_chosen,' '); 
         dlmwrite(strcat(Path_annexe,'all_params_Thrust_FL100-TOC.txt'),parametres_poussee,' ');
         dlmwrite(strcat(Path_annexe,'all_medianRMSE_Thrust_FL100-TOC.txt'),err_all,' ');

% end
%toc

%File_path='/home/anamaria/Desktop/AigleAzur/';
%dlmwrite(strcat('fig/v02.18_poussee_idALL.txt'),parametres_poussee,' ');
%dlmwrite(strcat(File_path,'v02.04_drag.txt'),parametres_drag,' ');
%dlmwrite(strcat(File_path,'v02.04_lift.txt'),parametres_lift,' ');

