function [ RMSE ] = modelValidation( mPoussee, Fig,Fpath,moteur,File_id, hmin,printok)

m1 = mPoussee(1);
m2 = mPoussee(2);

F0 = moteur(1);
BPR= moteur(2);
        
%Fpath=File_path
        % atmosphere constants
        rho0 = 1.225;
        %rho0 = 1;
        T0 = 288.15;
           
        % Get file names in the current directory
        string_flids = dir(strcat(Fpath));
        string_flids = { string_flids.name };
        string_flids = string_flids(3:length(string_flids));
        
        RMSE = zeros(length(string_flids),1);
        for i = 1:1:length(string_flids)
            %% Thrust identification
            % identify a set of thrust parameters per flight, for the given DB
            Fname = string_flids{i};
            vol = readtable(strcat(Fpath,Fname));
            
            % now read table columns and convert them to vectors
            h_m = table2array(vol(:,'alt_stdc_m'));
            
            tmin = find(h_m >= hmin, 1, 'first');
            if(hmin < 10000*0.3048)
                hmax = 10000*0.3048;
            else
                hmax = max(h_m);
            end
            tmax = find(h_m >= hmax, 1, 'first'); 
            
            h_m = table2array(vol(tmin:tmax,'alt_stdc_m'));
            len = length(h_m);
           % Ci = zeros(len,1);
            Mach = table2array(vol(tmin:tmax,'MACHlisse'));
            N1 = table2array(vol(tmin:tmax,'n11c_pc'));
            N2 = table2array(vol(tmin:tmax,'n12c_pc'));
            ff1 = table2array(vol(tmin:tmax,'ff1c_kgs'));
            ff2 = table2array(vol(tmin:tmax,'ff2c_kgs'));
            P_Pa_SI = table2array(vol(tmin:tmax,'P_Pa_SI'));
            %SATk = table2array(vol(:,'SATkelvin'));
            rho_SI = table2array(vol(tmin:tmax,'rho_SI'));% masse volumique de l'air humide
   
            SATk = table2array(vol(tmin:tmax,'sat')); % SAT = f(Mach, TAT)
            %rho_SI = 1.292 * SATk/273.15; % rho measured - rho de l'air sec
           phi = 0.76;
           %rho_SI = 1./(287.06*SATk).*(P_Pa_SI - ... % masse volumique de l'air humide
           % 230.617 * phi * exp(17.5043*(SATk-273.15)./(241.2+SATk-273.15)));
            
            
            % implement objective function
            Tmax = F0 * power(rho_SI/rho0,0.6) .*  ([power(Mach,3) ones(length(Mach),1)] * [m1; m2]) ;
            %Tmax = F0 * power(rho_SI_all/rho0,0.6) * mPoussee(1) .* ( mPoussee(2) * power(Mach_all,3)+1);
            T = (N1 + N2) .* Tmax;
            
            CSRi = ((-3.35*1e-10 * h_m + 1.54*1e-5) .* Mach +...
                (-2.05*1e-11 * h_m - 4.54*1e-7) * BPR .* ones(length(Mach),1) +...
                (3.47*1e-10 * h_m + 1.08*1e-5)) .* sqrt(SATk/T0);
            
            Ci = T .* CSRi;
           
            RMSE(i) = median(round(sqrt(sum(power((ff1+ff2-Ci),2))/length(Ci)),4));
          if(printok) 
            plot(ff1+ff2,'b');%xlim([1,NfileTest]);
            title(strcat('Comparison for ',Fname, ' with ',File_id, 'parameters'))
            hold on
            plot(Ci,'r')
            legend('Measured Fuel Flow','Modeled Consumption Ci=f(T,CSR)','Location','NorthEast')
%             text('SouthWest' ,strcat('Error = ',num2str(RMSE(i),3)));
            uicontrol('Style', 'text',...
                'String', strcat('Error = ',num2str(RMSE(i),3)),... %replace something with the text you want
                'Units','normalized',...
                'Position', [0.7 0.5 0.1 0.1]); 
            hold off
            saveas( gcf, strcat(Fig,'Thrust_',num2str(i),'_',File_id,'.jpeg'),'jpg');
          end
        end

end
