####################################################################################################################
Calc.CoeffDrag_pousseeFL100<-function(Path_work_dir, file_names,poussee){
  p_infFL100 = poussee[1:2];
  p_supFL100 = poussee[3:4];
  vol    = read.csv(paste(Path_work_dir,file_names,sep=""), header = TRUE ,stringsAsFactors = TRUE,sep=",",na.strings='null')
  # vol = vol[170:nrow(vol),]
  
  F0 = 121400; rho0 = 1.225;lam=1.4;Rs = 287.053;g=9.81
#   phi = 0.76;
  
  tFL100 = head(which(vol$alt_stdc_ft>=10000),1)
  
#   rho_SI = 1/(287.06*vol$sat)*(vol$P_Pa_SI -  # masse volumique de l'air humide
#                                  230.617 * phi * exp(17.5043*(vol$sat-273.15)/(241.2+vol$sat-273.15)));
  
  
  Tmax_infFL100 = (vol$n11c_pc[1:(tFL100-1)] + vol$n12c_pc[1:(tFL100-1)]) * F0 * (vol$rho_SI[1:(tFL100-1)]/rho0)^0.6 *  ((vol$mach[1:(tFL100-1)]^3) * p_infFL100[1] + p_infFL100[2]) ;
  Tmax_supFL100 = (vol$n11c_pc[tFL100:nrow(vol)] + vol$n12c_pc[tFL100:nrow(vol)]) * F0 * (vol$rho_SI[tFL100:nrow(vol)]/rho0)^0.6 *  ((vol$mach[tFL100:nrow(vol)]^3) * p_supFL100[1] + p_supFL100[2]) ;
  
  T =  c(Tmax_infFL100,Tmax_supFL100)
  
#   vson_ms = sqrt(lam*Rs*vol$sat)
  TAS_ms = vol$mach * vol$vson_ms
  # lissage de la vitesse
  Vspline = smooth.spline(as.numeric(as.character(vol$numrow)),as.numeric(as.character(TAS_ms)),spar=0.5)
  TASms_lisse = Vspline$y
  vol$TASlisse_ms = TASms_lisse
  # dérivée de la vitesse TAS
  TASms_dot = predict(Vspline,deriv=1)[["y"]]
  vol$TASdot_lisse_ms = TASms_dot
  
  SCxObs = (T* cos(vol$aoa_rad) - vol$gw_kg * g * sin(vol$GAMMAlisse_rad) - vol$gw_kg * TASms_dot)/(0.5 * lam * vol$P_Pa_SI * (vol$mach^2));
  aoa_rad = vol$aoa_rad
  mach = vol$mach
  df = as.data.frame(cbind(aoa_rad, mach, SCxObs))
  
  return(df)
}
####################################################################################################################
####################################################################################################################
Choose.Segment_5<-function(segment,mach, Mcoup){
  index = 1:length(mach)
  if(segment==1){
    index = which(mach<Mcoup[1]) # segm 1
    if(length(index)==0)
      cat("\nSegment 1 doesn't exist.")
  }
  if( segment==2){
    index = which(mach>=Mcoup[1])
    index = index[which(mach[index]<Mcoup[2])]
    if(length(index)==0)
      cat("\nSegment 2 doesn't exist.")
  }
  if( segment==3){
    index = which(mach>=Mcoup[2])
    index = index[which(mach[index]<Mcoup[3])]
  }
  if( segment==4){
    index = which(mach>=Mcoup[3])
    index = index[which(mach[index]<Mcoup[4])]
  }
  if( segment ==5){
    index = which(mach>=Mcoup[4])
    if(length(index)==0)
      cat("\nSegment 5 doesn't exist.")
  }
  return(index)
}
####################################################################################################################
####################################################################################################################
Calc_Segm.DRAG_model2_5segm<-function(file_names, Path_work_dir, Mcoup, segment,poussee){
  
  vol = Calc.CoeffDrag_pousseeFL100(Path_work_dir, file_names,poussee)
  df = vol[,cbind('aoa_rad','mach','SCxObs')]
  
  # Cut by Mach
  index = Choose.Segment_5(segment,vol$mach,Mcoup)
  if(length(index)==0)
    return(rep(NA,4)) # si le segm (3) n'existe pas, on renvoie des NA
  
  df = df[index,]
  
  # Order by alpha
  df = df[with(df, order(df[,1])), ]
  
  # do fitting
  fit = glm(df$SCxObs~df$mach*df$aoa_rad ,data=df) 
  
  term_libre = fit$coefficients[1] 
  coef_mach = fit$coefficients[2] 
  coef_aoa = fit$coefficients[3] 
  coef_aoa_mach = fit$coefficients[4] 
  
  p = c(coef_aoa,coef_mach,coef_aoa_mach,term_libre)
  return(p)
}
####################################################################################################################
####################################################################################################################
ValidationDRAG_model2_5segments<-function(file_names, Path_work_dir, Mcoup, segment, param_drag, param_poussee){
  error<-vector()
  p = param_drag
  for (i in 1:length(file_names)){
    vol = Calc.CoeffDrag_pousseeFL100(Path_work_dir, file_names[i], param_poussee)
    index = Choose.Segment_5(segment,vol$mach, Mcoup)
    SCx_model = p[4*segment-3] * vol$aoa[index] + p[4*segment-2] * vol$mach[index] + p[4*segment-1] * vol$aoa[index] * vol$mach[index] + p[4*segment]
    
    error[i] = round(sqrt(sum((vol$SCxObs[index]-SCx_model)^2))/length(vol$SCxObs[index]),4)
  }  
  return(error)
}
####################################################################################################################
####################################################################################################################
Calc_SCx_model2<-function(file_names, Path_work_dir, Mcoup, param_drag, param_poussee){
  vol =Calc.CoeffDrag_pousseeFL100(Path_work_dir, file_names, param_poussee)
  p = param_drag
  scx_crt<-vector()
  
  index = Choose.Segment_5(segment=1,vol$mach, Mcoup)
  SCx_crt_model21 = p[1] * vol$aoa[index] + p[2]*vol$mach[index] + p[3]*vol$aoa[index]*vol$mach[index] + p[4]
  index = Choose.Segment_5(segment=2,vol$mach, Mcoup)
  SCx_crt_model22 = p[5] * vol$aoa[index] + p[6]*vol$mach[index] + p[7]*vol$aoa[index]*vol$mach[index] + p[8]
  index = Choose.Segment_5(segment=3,vol$mach, Mcoup)
  SCx_crt_model23 = p[9] * vol$aoa[index] + p[10]*vol$mach[index] + p[11]*vol$aoa[index]*vol$mach[index] + p[12]
  index = Choose.Segment_5(segment=4,vol$mach, Mcoup)
  SCx_crt_model24 = p[13] * vol$aoa[index] + p[14]*vol$mach[index] + p[15]*vol$aoa[index]*vol$mach[index] + p[16]
  index = Choose.Segment_5(segment=5,vol$mach, Mcoup)
  SCx_crt_model25 = p[17] * vol$aoa[index] + p[18]*vol$mach[index] + p[19]*vol$aoa[index]*vol$mach[index] + p[20]
  
  scx_crt = c(SCx_crt_model21,SCx_crt_model22,SCx_crt_model23,SCx_crt_model24,SCx_crt_model25)
  
  return(scx_crt)
}
####################################################################################################################

