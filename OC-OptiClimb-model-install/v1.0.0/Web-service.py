# !/usr/bin/env python
# -*- coding:utf-8 -*-

import json, cherrypy, sys, traceback
import numpy as np
from sys import exit
from OptiClimbModel.OptiClimbModel import *




reload(sys)  
sys.setdefaultencoding('utf8')

class WebService(object):


	def __init__(self):

		print '\n*** Import Evaluation object'
		
		# IMPORT THE LAST OBJECT

		# 1- STANDARD
		path = 'models/standard/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		if listdir == []:
			msg = 'No models found in "models/standard/" folder'
			print msg
			exit(msg)

		with open(path+listdir[-1], 'r') as f:
			model = cPickle.load(f)

		print path+listdir[-1] + ' imported'

		if model.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load-and-learn-standard.py'."
			print msg
			exit(msg)

		self.standard_model = model

		# 1- OPTICLIMB
		path = 'models/opticlimb/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		if listdir == []:
			msg = 'No models found in "models/opticlimb/" folder'
			print msg
			exit(msg)

		with open(path+listdir[-1], 'r') as f:
			model = cPickle.load(f)

		print path+listdir[-1] + ' imported\n'

		if model.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load-and-learn-opticlimb'."
			print msg
			exit(msg)

		self.opticlimb_model = model


	@cherrypy.expose
	def index(self):
		if 'Content-Length' not in cherrypy.request.headers:
			return 'No data received. Use a POST request.'

		cl = cherrypy.request.headers['Content-Length']
		request = cherrypy.request.body.read(int(cl))
		return self.process(request)


	@cherrypy.expose
	def test(self):
		return 'Test ok'


	def process(self, request):

		print '\n\t*** Request into dict'
		request_dict = eval(request)



		keys = ['tow', 'delta_isa', 'time_to_toc', 'toc', 'registration', 'origin']
		if sum([z in request_dict.keys() for z in keys]) != 6:
			msg = 'Parameter missing in request. Must be ' + str(keys)
			print msg
			return msg

		tow = request_dict['tow']
		delta_isa = np.array(request_dict['delta_isa'])
		time_to_toc = np.array(request_dict['time_to_toc'])
		toc = np.array(request_dict['toc'])
		registration = request_dict['registration']
		origin = request_dict['origin']


		print '\t*** Prediction of OptiClimb consumption'
		opticlimb_pred = self.opticlimb_model.predict(tow, delta_isa, time_to_toc, toc, registration, origin)
		print '\t*** Prediction of Standard consumption'
		standard_pred = self.standard_model.predict(tow, delta_isa, time_to_toc, toc, registration, origin)

		print '\t*** dict into Response\n'
		return json.dumps({'standard':float(standard_pred), 'opticlimb':float(opticlimb_pred)})




	@cherrypy.expose
	def reload(self):
		print '*** RELOAD'
		print '*** Import OptiClimbModel object'
		
		# IMPORT THE LAST OBJECT

		# 1- STANDARD
		path = 'models/standard/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			model = cPickle.load(f)

		print path+listdir[-1] + ' imported\n'

		if model.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load-and-learn-standard.py'."
			print msg
			return msg

		self.standard_model = model

		# 1- OPTICLIMB
		path = 'models/opticlimb/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			model = cPickle.load(f)

		print path+listdir[-1] + ' imported\n'

		if model.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load-and-learn-opticlimb'."
			print msg
			return msg

		self.opticlimb_model = model





if len(sys.argv) == 3:
	host = sys.argv[1]
	port = int(sys.argv[2])
else:
	print 'Initialize web service at localhost'
	host = '127.0.0.1'
	port = 8080



if __name__ == '__main__':
	cherrypy.config.update({
		'server.socket_host' : host,
		'server.socket_port' : port,
		'server.thread_pool' : 5,
		'tools.sessions.on' : True,
		'tools.encode.encoding' : 'Utf-8'
	})
	cherrypy.quickstart(WebService())

