

echo 'Extract ConsumptionModel package'
tar -xzvf ConsumptionModel-1.0.0.tar.gz
cd ConsumptionModel-1.0.0/

echo 'Installing ConsumptionModel package, v1.0.0'
python setup.py install --v 1.0.0


cd ..

echo 'Extract OptiClimbModel package'
tar -xzvf OptiClimbModel-1.0.0.tar.gz
cd OptiClimbModel-1.0.0/

echo 'Installing OptiClimbModel package, v1.0.0'
python setup.py install --v 1.0.0

cd ..

echo 'Installing main directory'
mkdir ../OptiClimbModel-1.0.0-ws/
mkdir ../OptiClimbModel-1.0.0-ws/models/
mkdir ../OptiClimbModel-1.0.0-ws/models/standard/
mkdir ../OptiClimbModel-1.0.0-ws/models/opticlimb/
cp load-and-learn-standard.py load-and-learn-opticlimb.py Web-service.py ../OptiClimbModel-1.0.0-ws/


echo 'Cleaning temporary files'
rm -rf ConsumptionModel-1.0.0/ OptiClimbModel-1.0.0/
