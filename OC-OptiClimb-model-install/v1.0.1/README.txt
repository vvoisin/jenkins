---------------------------------------
---									---
---		Package OptiClimbModel		---
---									---
---------------------------------------


---	RÉSUMÉ ---


Modèles statistiques pour la prédiction de la consommation : 

- Modèle standard : consommation prévue si le pilote ne suit pas les consignes, basé sur les vols non OptiClimb
- Modèle OptiClimb : consommation prévue si le pilote suit bien les consignes, basé sur les vols OptiClimb


Deux étapes pour chaque modèle :
	
	1- (Offline Learning Step) Extraction des données de vol et construction d'un modèle de régression pour la prédiction de la consommation en fonction de la masse à l'atterrissage (tow), du delta ISA, du temps de montée (time_to_toc), du top of climb (toc), de l'immatriculation de l'avion (registration) et de l'aérport d'origin.

	2- (Online Prediction) Prédiction des modèles pour chaque nouveau vol reçu.

	En pratique, l'interface entre l'application (en Java) et les modèles statistiques (en Python) est effectuée via un web service Python (package cherrypy). L'application Java envoie des requètes à Python via ce web service et selon un format JSON. Les données réçues sont traitées et renvoyées à Java en JSON également.



---	PRE-REQUIS ---

- Installation des paquets "python-dev", "python-numpy", "python-scipy" et "python-setuptools" via apt

- Installation manuelle de Parallel Python :

	wget http://www.parallelpython.com/downloads/pp/pp-1.5.7.tar.gz
	tar xvzf pp-1.5.7.tar.gz
	cd pp-1.5.7 && python setup.py install


---	INSTALLATION ---

L'installation se fait en local en executant le script bash 'install.sh' : 

sudo sh install.sh

Cette commande installe dans un premier temps le package 'ConsumptionModel' contenant la librairie statistique pour construire les modèles puis dans un second temps la librarie principale 'OptiClimbModel'. Les autres dépendances sont automatiquement installées via les dépots distants de Python. 

Liste des dépendances :
	- cherrypy
	- pickle / cPickle
	- numpy, scipy
	- pandas


Ensuite, le script installe le dossier principal (web service) dans le dossier parent.


--- UTILISATION ---


Architecture de la librairie :

OptiClimb-model/
		
		(Module) OptiClimbModel.py
			(Class) OptiClimbModel
				Construction d'un modèle de consommation (méthode 'load_and_learn') et prédiction de la consommation (méthode 'predict').

		(Module) DataExtractor.py
			
			(Class) DataExtractor
				Utilitaire d'extraction des données de vol pour les modèles statistiques. Utilisé uniquement dans la classe 'Evaluation'. ATTENTION : les données de vol doivent être préalablement extraites en csv.



Architecture du programme principal :

OptiClimb-model-ws/

		(Script) load-and-learn-standard.py
			Extraction des données non OptiClimb, construction du modèle et sérialisation de l'objet dans le dossier 'models/standard/'
			Usage: python load-and-learn-standard.py [data_path] [nr_fights] [regressor]

		(Script) load-and-learn-opticlimb.py
			Extraction des données OptiClimb, construction du modèle et sérialisation de l'objet dans le dossier 'models/opticlimb/'
			Usage: python load-and-learn-standard.py [data_path] [nr_fights] [regressor]

		(Script) Web-service.py
			(Class) WebService
				Initialisation du web service pour l'échange de données entre Java et Python à travers des requètes au format JSON.
				Usage: python Web-service.py [host] [port]
				Si les paramètres 'host' et 'port' ne sont pas renseignés, les valeurs par défaut sont host = 127.0.0.1 et port = 8080.

				Cette classe contient trois méthodes pouvant être appelées depuis Java :
					1- test : Teste le web service et renvoie 'Test ok'. Appel depuis Java : host:port/test
					2- index : récupère les données en JSON, charge les derniers modèles (dans les dossiers 'models/standard/' et 'models/opticlimb/') et exécute les prédictions. Appel depuis Java : host:port/
					3- reload : recharge les derniers modèles. Appel depuis Java : host:port/reload

		(Dossier) models/standard/ et models/opticlimb/
			Contient les objets serialisés et utilisables ensuite. DOSSIER A NE PAS SUPPRIMER! ACTUELLEMENT PAS DE TEST.


Utilisation standard :
(1.1) python load-and-learn-standard.py [data_path] [nr_fights] [regressor]
(1.2) python load-and-learn-opticlimb.py [data_path] [nr_fights] [regressor]
(2) python Web-service.py [host] [port]

Note : si (1.1) ou (1.2) n'est pas exécuté ou ne s'est pas terminé correctement, (2) retourne l'erreur suivante :

'The status equals 0. The models must be trained using 'load_and_learn' method.'



---	FORMAT D'ÉCHANGE ---

Le format d'échange entre l'application Java et le programme Python est le JSON. Les clés sont :
	'tow', 'delta_isa', 'time_to_toc', 'toc', 'registration', 'origin'
	
	- tow : take-off weight en kg, int
	- delta_isa : delta isa en degré, float
	- time_to_toc : temps de montée en secondes, int
	- toc : top of climb, int
	- registration : immatriculation de l'avion, str
	- origin : aéroport d'origine, str

Le web service renvoie un message d'erreur si les clés ne sont pas correctement renseignées :

'Parameter missing in request. Must be ['tow', 'delta_isa', 'time_to_toc', 'toc', 'registration', 'origin']'


Exemple :

{'tow':70000, 'delta_isa':3, 'time_to_toc':501, 'toc':370, 'registration':'F-GZHA', 'origin':'LFPO'}

