import sys, csv, math, os, traceback
import numpy as np
from sys import exit

if len(sys.argv) != 4:
	print 'Usage: python load-and-learn-standard.py [data_path] [nr_fights] [regressor]'
	exit(0)

from OptiClimbModel.OptiClimbModel import *

# 
# OFFLINE : load data and learn model
# 

# INPUT PARAMETERS
data_path = sys.argv[1]
nr_flights = int(sys.argv[2])
regressor = sys.argv[3]				# regressor in ('ETR', 'RF', 'SVR')
output_path = 'models/standard/'	# path for models objects


# EVALUATION OBJECT
OCmodel = OptiClimbModel(data_path, nr_flights, output_path, regressor, ncpus=-1)

# TEST load_data via load_and_learn
OCmodel.load_and_learn()
OCmodel.serialize('standard-'+regressor)

