

echo 'Extract ConsumptionModel package'
tar -xzvf ConsumptionModel-1.0.1.tar.gz
cd ConsumptionModel-1.0.1/

echo 'Installing ConsumptionModel package, v1.0.1'
python setup.py install --v 1.0.1


cd ..

echo 'Extract OptiClimbModel package'
tar -xzvf OptiClimbModel-1.0.3.tar.gz
cd OptiClimbModel-1.0.3/

echo 'Installing OptiClimbModel package, v1.0.3'
python setup.py install --v 1.0.3

cd ..

echo 'Installing main directory'
mkdir ../OptiClimbModel-1.0.3-ws/
mkdir ../OptiClimbModel-1.0.3-ws/models/
mkdir ../OptiClimbModel-1.0.3-ws/models/standard/
mkdir ../OptiClimbModel-1.0.3-ws/models/opticlimb/
cp load-and-learn-standard.py load-and-learn-opticlimb.py Web-service.py ../OptiClimbModel-1.0.3-ws/


echo 'Cleaning temporary files'
rm -rf ConsumptionModel-1.0.1/ OptiClimbModel-1.0.3/
