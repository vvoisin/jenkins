import sys, traceback
import numpy as np

from sys import exit
from Reader import *
from math import cos, pi, acos, sin

sys.path.append('../../OC-StepsEvaluation-package/StepsEvaluation')
from Evaluation import *
import matplotlib.pyplot as plt, pandas as pd, seaborn as sns


# 
# ONLINE : import Evaluation object and compute indexes
# 

print 'Import Evaluation object'
with open('../models/eval-funcSVR.object', 'r') as f:
	eval_func_object = cPickle.load(f)



# EVAL PROFILE

reader = Reader('./')
TIME, ALT, TAS, IAS, MACH, GW, _, FF, _, GAMMA, _ = reader.read_new(filename=sys.argv[1])


ALT = np.array(ALT); alt_min=ALT[0]; alt_max=ALT[-1]
TAS = np.array(TAS)
GAMMA = np.array(GAMMA)
TIME = np.array(TIME) - min(TIME)
GW = np.array(GW)
FF = np.array([FF[0]] + FF); FF_cruise = FF[-1]

# TEST TRANSITION COSTS
# steps = [	{'alt_begin': 5000, 'alt_end': 10000, 'ias': 241}, 
# 			{'alt_begin': 10000, 'alt_end': 26000, 'ias': 264}, 
# 			{'alt_begin': 26000, 'alt_end': 38000, 'mach': 0.79}]
# get_transition_costs(steps, FF, ALT)

# exit(0)


min_ias = 160
max_ias = 340


# EVAL CONSIGN

print '\n\tTest 1: 2 ias and 1 mach'
start_time = time.time()
steps_opt, eval_index, list_of_steps, list_of_indexes = eval_func_object.evaluation(alt_min=ALT[0], alt_max=ALT[-1], tas_profile=TAS, gamma_profile=GAMMA, time_profile=TIME, mass_flmin=GW[0], registration='FGZHA', min_alt_step=50, ff_cruise=FF_cruise, mach_cruise=None, ias_min=min_ias, ias_max=max_ias, mach_min=.3, mach_max=.82)
print("--- %s seconds ---" % (time.time() - start_time))



I0 = list_of_indexes[:,0]
I1 = list_of_indexes[:,1]
I2 = list_of_indexes[:,2]
idx = np.where(I0 == eval_index)[0]



N = len(TAS)
tas_profile = np.array(TAS)
alt_profile = np.linspace(alt_min, alt_max, N)
ias_profile = tas_to_ias(tas_profile, alt_profile)
mach_profile = tas_to_mach(tas_profile, alt_profile)
alt_mach_step = alt_profile[ alt_profile >= steps_opt[2]['alt_begin'] ]
ias_mach_step = [ mach_to_ias(steps_opt[2]['mach'], z) for z in alt_mach_step ]

plt.subplot(121)
plt.plot(alt_profile, ias_profile, '-')
plt.plot([steps_opt[0]['alt_begin'], steps_opt[0]['alt_end']], [steps_opt[0]['ias'], steps_opt[0]['ias']], 'r-')
plt.plot([steps_opt[1]['alt_begin'], steps_opt[1]['alt_end']], [steps_opt[1]['ias'], steps_opt[1]['ias']], 'r-')
plt.plot(alt_profile[alt_profile >= steps_opt[2]['alt_begin']], ias_mach_step, 'r-')
plt.ylim(200,320)


mach_step_mean = np.mean(mach_profile[alt_profile >= steps_opt[2]['alt_begin']])
plt.subplot(122)
plt.plot(alt_profile, mach_profile, '-')
plt.plot([steps_opt[2]['alt_begin'], steps_opt[2]['alt_end']], [steps_opt[2]['mach'], steps_opt[2]['mach']], 'r-', label='Median(MACH)')
# plt.plot([steps_opt[2]['alt_begin'], steps_opt[2]['alt_end']], [mach_step_mean, mach_step_mean], 'g-', label='Mean(MACH)')
plt.legend(loc=4)

plt.show()
