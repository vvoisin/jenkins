import sys, httplib, json, time
import numpy as np
from sys import exit
from Reader import *
import matplotlib.pyplot as plt, pandas as pd, seaborn as sns
sys.path.append('../../OC-StepsEvaluation-package/StepsEvaluation')
from utils import *


# CONNECT TO WEB SERVICE

HOST = '192.168.31.20'
PORT = '8081'

def request(dict_params):
	headers = {'Content-type':'application/json', 'Accept': '*/*'}

	try:
		conn = httplib.HTTPConnection(HOST, PORT)
		conn.request('POST', '/index', json.dumps(dict_params), headers)
		response = conn.getresponse()
		if (response.status == 200):
			data = response.read()
			return data
		else:
			print response.status, response.reason

	except Exception as err:
		print err

	finally:
		conn.close()



# REQUEST

reader = Reader('./')
TIME, ALT, TAS, _, _, GW, _, FF, _, GAMMA, _ = reader.read_new(filename=sys.argv[1])

alt_min=ALT[0]; alt_max=ALT[-1]
TIME = [z - min(TIME) for z in TIME]
FF = [FF[0]] + FF



# EVAL CONSIGN VIA WEB SERVICE

start_time = time.time()
dict_params = {	'alt_min':ALT[0], 'alt_max':ALT[-1], 'tas_profile':TAS, 'gamma_profile':GAMMA, 'time_profile':TIME, 
				'mass_profile':GW, 'registration':'FGZHA', 'ff_profile':FF}
response = json.loads(request(dict_params))
print("--- %s seconds ---" % (time.time() - start_time))

steps_opt = response['steps_opt']

print response['eval_index']
print steps_opt

N = len(TAS)
tas_profile = np.array(TAS)
alt_profile = np.linspace(alt_min, alt_max, N)
ias_profile = tas_to_ias(tas_profile, alt_profile)
mach_profile = tas_to_mach(tas_profile, alt_profile)
alt_mach_step = alt_profile[ alt_profile >= steps_opt[2]['alt_begin'] ]
ias_mach_step = [ mach_to_ias(steps_opt[2]['mach'], z) for z in alt_mach_step ]

plt.plot(alt_profile, ias_profile, '-')
plt.plot([steps_opt[0]['alt_begin'], steps_opt[0]['alt_end']], [steps_opt[0]['ias'], steps_opt[0]['ias']], 'r-')
plt.plot([steps_opt[1]['alt_begin'], steps_opt[1]['alt_end']], [steps_opt[1]['ias'], steps_opt[1]['ias']], 'r-')
plt.plot(alt_profile[alt_profile >= steps_opt[2]['alt_begin']], ias_mach_step, 'r-')
plt.show()


exit(0)

# OTHER CRITERION (RMSE MACH) --> TODO
I0 = list_of_indexes[:,0]
I1 = list_of_indexes[:,1]
I2 = list_of_indexes[:,2]
idx = np.where(I0 == indexe_opt_consumption)[0]
I0_star, I1_star, I2_star = list_of_indexes[idx][0]

print steps_opt_consumption
print I0_star, I1_star, I2_star


