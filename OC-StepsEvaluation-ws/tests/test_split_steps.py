import sys, traceback
import numpy as np

from sys import exit
from Reader import *

sys.path.append('../../OC-StepsEvaluation-package/StepsEvaluation')
from Evaluation import *


# EVAL PROFILE

reader = Reader('./')
_, ALT, TAS, IAS, MACH, GW, delta_mass, FF = reader.read_new(filename='problem_bocop_1.sol')
ALT = np.array(ALT)
GW = np.array(GW)
IAS = np.array([IAS[0]] + IAS)
MACH = np.array([MACH[0]] + MACH)
FF = np.array([FF[0]] + FF)



# EVAL CONSIGN
# CONSIGNES 1
# alt_1 = 13600
# alt_2 = 31500
# consign_ias_1 = 233
# consign_ias_2 = 270
# mach_3 = .74

eval_func_object = Evaluation(None, None, None, None)
start_time = time.time()
list_of_steps = eval_func_object.generate_steps(alt_profile=ALT, ias_profile=IAS, mach_profile=MACH, initial_level=int(np.min(ALT)/100), final_level=int(np.max(ALT)/100))
print("--- %s seconds ---" % (time.time() - start_time))


# print list_of_steps[0]


# PLOT
from matplotlib.backends.backend_pdf import PdfPages

pdf = PdfPages('test_consignes.pdf')

for step in list_of_steps[0:50]:

	plt.figure()
	plt.plot(ALT, IAS, '-', linewidth=2)
	for z in step:
		if z.has_key('ias'):
			plt.plot([z['alt_begin'], z['alt_end']], [z['ias'], z['ias']], 'r-', linewidth=2)
	pdf.savefig()
	plt.close()

pdf.close()



# 
# 		output examples
# 


# strategy == 2
# Readable:
# [
# 	[	
# 		{'alt_begin': 10000, 'alt_end': 11500, 'ias': 226.69140436104769}, 
# 		{'alt_begin': 11500, 'alt_end': 13000, 'ias': 236.7642321041416}, 
# 		{'alt_begin': 13000, 'alt_end': 36700, 'mach': 0.64852782025687983}
# 	], 
# 	[
# 		{'alt_begin': 10000, 'alt_end': 11500, 'ias': 226.69140436104769}, 
# 		{'alt_begin': 11500, 'alt_end': 14500, 'ias': 243.67937094790048}, 
# 		{'alt_begin': 14500, 'alt_end': 36700, 'mach': 0.65763780491812718}
# 	], 
# 	[
# 		{'alt_begin': 10000, 'alt_end': 11500, 'ias': 226.69140436104769}, 
# 		{'alt_begin': 11500, 'alt_end': 16000, 'ias': 256.39748228198744}, 
# 		{'alt_begin': 16000, 'alt_end': 36700, 'mach': 0.66482732693167657}
# 	]
# ]



# output example
# strategy == 2
# Readable:

# [
# 	[
# 		{'alt_begin': 10000, 'alt_end': 11500, 'ias': 226.69140436104769}, 
# 		{'alt_begin': 11500, 'alt_end': 13000, 'ias': 236.7642321041416}, 
# 		{'alt_begin': 13000, 'alt_end': 14500, 'ias': 251.97753756041101}, 
# 		{'alt_begin': 14500, 'alt_end': 36700, 'mach': 0.65763780491812718}
# 	], 
# 	[
# 		{'alt_begin': 10000, 'alt_end': 11500, 'ias': 226.69140436104769}, 
# 		{'alt_begin': 11500, 'alt_end': 13000, 'ias': 236.7642321041416}, 
# 		{'alt_begin': 13000, 'alt_end': 16000, 'ias': 267.10652783353964}, 
# 		{'alt_begin': 16000, 'alt_end': 36700, 'mach': 0.66482732693167657}
# 	], 
# 	[
# 		{'alt_begin': 10000, 'alt_end': 11500, 'ias': 226.69140436104769}, 
# 		{'alt_begin': 11500, 'alt_end': 13000, 'ias': 236.7642321041416}, 
# 		{'alt_begin': 13000, 'alt_end': 17500, 'ias': 274.00664716126505}, 
# 		{'alt_begin': 17500, 'alt_end': 36700, 'mach': 0.67071980571213119}
# 	]
# ]