import sys, csv, math, os, traceback
import numpy as np, matplotlib.pyplot as plt, seaborn as sns
from scipy.optimize import minimize
from sklearn.linear_model import LinearRegression
from pyearth import Earth

class Reader():
	""" Data reader for Bocop output data (.sol) """
	
	def __init__(self, data_path):
		self.data_path = data_path
	
	def read_new(self, mass=None, toc=None, filename=None):

		time, tas, ias, mach, gw, ff, dist_m, gamma, sat = [],[],[],[],[],[],[],[],[]
		try:
			if filename is None and mass is not None and toc is not None:
				fname = 'FL' + str(toc) + '/masse' + str(mass) + '000.sol'

				if toc not in [350, 380]:
					raise ValueError('toc parameter must be equal to [350, 380]')
			else:
				fname = filename


			with open(self.data_path+fname, 'r') as f:

				check = ''
				while True:

					line = f.next()

					# GET NORMALIZATION VALUES
					if 'time.initial' in line:
						time_initial = line
					if 'time.final' in line:
						time_final = line


					if '# State 0\n' in line or '# State variable 0 :' in line: # begin time
						check = 'time'

					if '# State 1\n' in line or '# State variable 1 :' in line: # begin tas
						check = 'tas'

					if '# State 2\n' in line or '# State variable 2 :' in line: # begin gamma
						check = 'gamma'

					if '# State 3\n' in line or '# State variable 3 :' in line: # begin gw
						check = 'gw'

					if '# State 4\n' in line or '# State variable 4 :' in line: # begin distance in meters
						check = 'dist_m'

					if '# Control variable 0 :' in line or '# Control 0' in line: # end distance in meters
						check = ''

					if '# Path constraint 0 :' in line: # begin sat
						check = 'sat'

					if '# Path constraint 1 :' in line: # end sat
						check = ''

					if '# Path constraint 2 :' in line: # begin mach
						check = 'mach'

					if '# Path constraint 3 :' in line: # end mach
						check = ''

					if '# Path constraint 7 :' in line: # begin fuel flow
						check = 'ff'

					if '# Path constraint 8 :' in line: # end fuel flow
						check = ''

					if '# Path constraint 10 :' in line: # begin ias
						check = 'ias'

					if '# Path constraint 11 :' in line: # end ias
						break

					if check == 'time':
						time.append(line)
					if check == 'tas':
						tas.append(line)
					if check == 'gw':
						gw.append(line)
					if check == 'ias':
						ias.append(line)
					if check == 'mach':
						mach.append(line)
					if check == 'ff':
						ff.append(line)
					if check == 'dist_m':
						dist_m.append(line)
					if check == 'gamma':
						gamma.append(line)
					if check == 'sat':
						sat.append(line)
				

		except (IOError, ValueError) as err:
			print err
			sys.exit(0)

		except StopIteration:
			print '\t\t\t\tFin du fichier'
			idx = ias.index('\r\n')
			ias = ias[0:idx+1]

		# TIME
		time.pop(0)
		time.pop()
		time = [float(z) for z in time]
		# TAS
		tas.pop(0)
		tas.pop()
		tas = [float(z) / .5144444 for z in tas]
		# GW
		gw.pop(0)
		gw.pop()
		gw = [float(z) for z in gw]
		delta_mass = gw[0] - gw[-1]
		# IAS
		ias.pop(0); ias.pop(0)
		ias.pop()
		ias = [float(z) / .5144444 for z in ias]
		# MACH
		mach.pop(0); mach.pop(0)
		mach.pop()
		mach = [float(z) for z in mach]
		# SAT IN K
		sat.pop(0); sat.pop(0)
		sat.pop()
		sat = [float(z) for z in sat]
		# FF
		ff.pop(0); ff.pop(0)
		ff.pop()
		ff = [float(z) for z in ff]
		# ALT
		time_initial = float(time_initial.split(' ')[-1])
		time_final = float(time_final.split(' ')[-1])
		alt = np.linspace(time_initial, time_final, len(time))
		alt = [float(z) / .3048 for z in alt]
		# DISTANCE
		dist_m.pop(0)
		dist_m.pop()
		dist_m = [float(z) for z in dist_m]
		# GAMMA
		gamma.pop(0)
		gamma.pop()
		gamma = [float(z) for z in gamma]

		return time, alt, tas, ias, mach, gw, delta_mass, ff, dist_m, gamma, sat



	def read(self, mass=None, toc=None, filename=None):

		alt, tas, ias, mach = [],[],[],[]
		try:

			if filename is None and mass is not None and toc is not None:
				fname = 'FL' + str(toc) + '/masse' + str(mass) + '000.sol'
	
				if toc not in [350, 360, 380]:
					raise ValueError('toc parameter must be in [350, 360, 380]')
	
			else:
				fname = filename


			with open(self.data_path+fname, 'r') as f:

				check = ''
				while True:

					line = f.next()

					if '# State variable 0 :' in line: # begin alt
						check = 'alt'

					if '# State variable 1 :' in line: # begin tas
						check = 'tas'

					if '# State variable 2 :' in line: # end tas
						check = ''

					if '# Path constraint 3 :' in line: # begin ias
						check = 'ias'

					if '# Path constraint 4 :' in line: # begin mach
						check = 'mach'

					if '# Dynamic constraint 0 :' in line: # end
						break

					if check == 'alt':
						alt.append(line)
					if check == 'tas':
						tas.append(line)
					if check == 'ias':
						ias.append(line)
					if check == 'mach':
						mach.append(line)
				alt.pop(0)
				alt.pop()
				alt = [float(z) / .3048 for z in alt]
				tas.pop(0)
				tas.pop()
				tas = [float(z) / .514 for z in tas]
				ias.pop(0); ias.pop(0)
				ias.pop()
				ias = [float(z) / .514 for z in ias]
				mach.pop(0); mach.pop(0)
				mach.pop()
				mach = [float(z) for z in mach]

		except (IOError, ValueError) as err:
			print err
			sys.exit(0)
		except StopIteration:
			print 'Fin du fichier'
			sys.exit(0)


		return range(len(alt)), alt, tas, ias, mach


	# def TAStoIAS(self):
	# 	pass


# IAS : # constraint.3 string IAS_visu --> # Path constraint 3 : ?
# MACH # constraint.4 string Mach_visu --> # Path constraint 4 : ?

# Conversion TAS-IAS
# // IAS for visualisation
# Tdouble h_feet = h_m / 0.3048e0;											ALTITUDE IN FEET
# Tdouble SAT_C = SAT_K - 272.15e0;											SAT IN C
# Tdouble T_S = 15e0 - 2e0 * h_feet / 1000e0;								?
# Tdouble IAS = V_ms / (1e0 + 0.01e0 * (h_feet/600e0 + (SAT_C - T_S)));		IAS IN KTS

# Conversion TAS-Mach
# Tdouble Vair_ms = 20.05 * sqrt(SAT_K);
# Tdouble M = V_ms / Vair_ms;




class PLR():
	""" Piecewise Linear Regression with fixed number of knots """
	
	def __init__(self, init_knots): #nknots, funcs
		if len(init_knots) < 3 or len(init_knots) > 4:
			sys.exit('The number of knots must be in {3, 4}')

		self.init_knots = init_knots

		self.estimated_knots = None # a list of size nknots - 1
		self.model = None
		self.fitted_values = None

	# def get_linear(self, args):
	# 	""" args = (alpha, beta) """
	# 	if isinstance(args, tuple):
	# 		return lambda x: args[0] * x + args[1]
	# 	else:
	# 		return lambda x: args

	# def get_f_lam_point(self, x, params, knots):

	# 	# GET BASIS FUNCTIONS
	# 	alph_bet = [ (params[0], params[1]), params[2], (params[3], params[4]), params[5] ]
	# 	thet = [0] + knots + [np.inf]
	# 	res = 0
	# 	for i,z in enumerate(alph_bet):
	# 		fbasis = self.get_linear(z)
	# 		res += fbasis(x) * int(np.where(x >= thet[i] and x < thet[i+1], 1, 0))
	# 	return res

	def fit(self, X, y, knots=None, verbose=False, keep_model=False):

		n = len(X)
		if knots is None: 
			knots = self.init_knots
		X0 = np.array( [hinge(x - knots[0]) for x in X] )
		X1 = np.array( [hinge(x - knots[1]) for x in X] )
		X2 = np.array( [hinge(x - knots[2]) for x in X] )
		if len(knots) == 4:
			X3 = np.array( [hinge(x - knots[3]) for x in X] )
			design = np.concatenate((X.reshape((n,1)), X0.reshape((n,1)), X1.reshape((n,1)), X2.reshape((n,1)), X3.reshape((n,1))), axis=1)
		else:
			design = np.concatenate((X.reshape((n,1)), X0.reshape((n,1)), X1.reshape((n,1)), X2.reshape((n,1))), axis=1)

		model = LinearRegression()
		model.fit(design, y)
		yhat = model.predict(design)
		R2 = model.score(design, y)

		if keep_model:
			self.model = model
			self.fitted_values = yhat

		if verbose:
			print '\n--- MODEL SUMMARY ---\n'
			print 'Model coefficients'
			print model.coef_
			print '\nIntercept'
			print model.intercept_
			print '\nR2 = ' + str(R2)
			print '\nRoot Mean Square Error = ' + str( np.sqrt( np.mean((y - yhat)**2) ) )

			# plt.plot(X, y, 'o', alpha=.5)
			# plt.plot(X, yhat, 'r-')
			# plt.plot([knots[0], knots[0]], [min(y), max(y)], 'k--', linewidth=2)
			# plt.plot([knots[1], knots[1]], [min(y), max(y)], 'k--', linewidth=2)
			# plt.plot([knots[2], knots[2]], [min(y), max(y)], 'k--', linewidth=2)
			# plt.show()

		return 1 - R2

	def optimize_knots(self, X, y):
		
		print '\nParameters optimization...'
		objective = lambda knots : self.fit(X=X, y=y, knots=knots)
		opt = minimize(fun=objective, x0=self.init_knots, options={'disp': True, 'gtol':1e-6})
		print '\nEstimated knots:'
		print opt.x

		print '\nFitting best model...'
		self.estimated_knots = opt.x
		self.fit(X, y, knots=opt.x, keep_model=True, verbose=True)


	def fit_old(self, X, y):

		n = len(X)
		theta = self.init_knots
		X0 = X * np.array([indicator(theta[0] - x) for x in X])
		X1 = np.array([indicator(x - theta[0]) * indicator(theta[1] - x) for x in X])
		X2 = X * np.array([indicator(x - theta[1]) * indicator(theta[2] - x) for x in X])
		X3 = np.array([indicator(x - theta[2]) for x in X])
		design = np.concatenate((X0.reshape((n,1)), X1.reshape((n,1)), X2.reshape((n,1)), X3.reshape((n,1))), axis=1)
		model = LinearRegression()
		model.fit(design, y)
		self.model = model

		print model.coef_
		print model.intercept_
		yhat = model.predict(design)
		
		plt.plot(X, y, 'o', alpha=.5)
		plt.plot(X, yhat, 'r-')
		plt.plot([theta[0], theta[0]], [min(y), max(y)], 'k--', linewidth=2)
		plt.plot([theta[1], theta[1]], [min(y), max(y)], 'k--', linewidth=2)
		plt.plot([theta[2], theta[2]], [min(y), max(y)], 'k--', linewidth=2)
		plt.show()

		# WITH minimize
		# OBJECTIVE FUNCTION
		# objective = lambda pars, knots : sum( [ ( y[i] - self.get_f_lam_point(x, params=pars, knots=knots) )**2 for i,x in enumerate(X) ] )
		# # AT ITERATION 0
		# # print objective(self.init_parameters, self.init_knots)
		# # MINIMIZE PARAMETERS WITH FIXED KNOTS
		# opt = minimize(fun=objective, x0=self.init_parameters, args=(self.init_knots,), options={'disp': True})
		# print opt.x
		# print opt.fun



def hinge(z):
	return max(0,z)

def indicator(z):
	return int(np.where(z >= 0, 1, 0))



