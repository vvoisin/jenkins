import sys, traceback
import numpy as np
from sys import exit
sys.path.append('../../OC-StepsEvaluation-package/StepsEvaluation')
from Evaluation import *
import matplotlib.pyplot as plt, pandas as pd, seaborn as sns
from matplotlib.backends.backend_pdf import PdfPages

flight = pd.read_csv('/Users/bgregorutti/Documents/Projets/Data/TVF/TVF-data/FGZHA-2015-04-14-02-03.csv')
idx_null = np.where(flight.LEVEL_OFF == 'null')[0][0]
flight = flight.iloc[0:idx_null]


LEVEL_OFF = np.array(flight.LEVEL_OFF, dtype=int)
IAS = np.array(flight.IAS, dtype=float)[(LEVEL_OFF > 0) & (LEVEL_OFF < 3)]
MACH = np.array(flight.MACH, dtype=float)[(LEVEL_OFF > 0) & (LEVEL_OFF < 3)]
ALTITUDE = np.array(flight.ALTITUDE, dtype=float)[(LEVEL_OFF > 0) & (LEVEL_OFF < 3)]


pdf = PdfPages('check_utils.pdf')

print 'Computing mach_to_tas (1)...'
tas_1 = mach_to_tas(MACH, ALTITUDE)


print 'Computing ias_to_tas (2) and ias_to_tas_approx (3)...'
tas_2 = ias_to_tas(IAS, ALTITUDE)
tas_3 = ias_to_tas_approx(IAS, ALTITUDE)



print 'Testing tas_to_mach...'
new_mach_1 = tas_to_mach(tas_1, ALTITUDE)
new_mach_2 = tas_to_mach(tas_2, ALTITUDE)
new_mach_3 = tas_to_mach(tas_3, ALTITUDE)
plt.figure()
plt.plot(ALTITUDE, MACH, '-', label='MACH')
plt.plot(ALTITUDE, new_mach_1, '-', label='mach -> tas -> mach')
plt.plot(ALTITUDE, new_mach_2, '-', label='ias -> tas -> mach')
plt.plot(ALTITUDE, new_mach_3, '-', label='ias -> tas -> mach (approx)')
plt.legend(loc=4)
pdf.savefig()

print '\t(1) OK'
print '\t(2) OK (3) NON OK'



print 'Testing tas_to_ias (1)...'
new_ias_1 = tas_to_ias(tas_1, ALTITUDE)


print 'Testing mach_to_ias and mach_to_ias_approx...'
new_ias_2 = mach_to_ias(MACH, ALTITUDE)
new_ias_3 = mach_to_ias_approx(MACH, ALTITUDE)
plt.figure()
plt.plot(ALTITUDE, IAS, '-', label='IAS')
plt.plot(ALTITUDE, new_ias_1, '-', label='mach -> tas -> ias')
plt.plot(ALTITUDE, new_ias_2, '-', label='mach -> ias')
plt.plot(ALTITUDE, new_ias_3, '-', label='mach -> ias (approx)')
plt.legend(loc=4)
pdf.savefig()

print '\t(1) OK'
print '\t(2) OK (3) NON OK'

pdf.close()

