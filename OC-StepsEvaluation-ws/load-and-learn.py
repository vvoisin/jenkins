import sys, csv, math, os, pkg_resources
import numpy as np

from sys import exit
# from StepsEvaluation.Evaluation import *

# ENLEVER DES QUE C EST OK
sys.path.append('../OC-StepsEvaluation-package/StepsEvaluation/')
from Evaluation import Evaluation


if len(sys.argv) != 4:
	print 'Usage: python load-and-learn.py [data_path] [nr_flights] [ncpus]'
	exit(0)

# 
# OFFLINE : load data and learn model
# 

# INPUT PARAMETERS
data_path = sys.argv[1]
nr_flights = int(sys.argv[2])
ncpus = int(sys.argv[3])
regressor = 'SVR'				# regressor in ('ETR', 'RF', 'SVR')
output_path = 'models/'

print '\n\t\t--- StepsEvaluation consumption model v' + pkg_resources.require('StepsEvaluation')[0].version
print '\t\t--- data_path ' + str(data_path)
print '\t\t--- nr_flights ' + str(nr_flights)
print '\t\t--- ncpus ' + str(ncpus)
print '\t\t--- regressor ' + str(regressor)
print '\t\t--- output_path ' + str(output_path) + '\n'


# EVALUATION OBJECT
eval_funct = Evaluation(data_path, nr_flights, output_path, regressor, ncpus)
eval_funct.load_and_learn()
eval_funct.serialize('eval-func'+regressor)

