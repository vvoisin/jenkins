# !/usr/bin/env python
# -*- coding:utf-8 -*-

import json, cherrypy, sys, os, cPickle, pkg_resources
import numpy as np
from sys import exit
# from StepsEvaluation.Evaluation import Evaluation

# ENLEVER DES QUE C EST OK
sys.path.append('../OC-StepsEvaluation-package/StepsEvaluation/')
from Evaluation import Evaluation



reload(sys)  
sys.setdefaultencoding('utf8')

class WebService(object):


	def __init__(self):

		print '*** Import Evaluation object'
		
		# IMPORT THE LAST OBJECT
		path = 'models/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			eval_func_object = cPickle.load(f)

		print path+listdir[-1] + ' imported\n'

		if eval_func_object.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load_and_learn' method.\n"
			print msg
			return msg
		else:
			print 'Model trained with ' + str(eval_func_object.nr_flights) + ' flights.\n'

		self.eval_func_object = eval_func_object


	@cherrypy.expose
	def index(self):
		if 'Content-Length' not in cherrypy.request.headers:
			return 'No data received. Use a POST request.'

		cl = cherrypy.request.headers['Content-Length']
		request = cherrypy.request.body.read(int(cl))
		cherrypy.response.headers['Content-Type'] = 'application/json'
		return self.process(request)


	@cherrypy.expose
	def test(self):
		return 'Test ok'


	@cherrypy.expose
	def get_version(self):
		return 'StepsEvaluation version v' + pkg_resources.require('StepsEvaluation')[0].version


	def process(self, request):

		print '*** Request into dict'
		request_dict = eval(request)
		alt_min,alt_max,tas_profile,gamma_profile,time_profile,mass_flmin,registration,mach_cruise,min_alt_step,ff_cruise,ias_min,ias_max,mach_min,mach_max = self.check_request(request_dict)

		print '*** Processing'

		try:
			steps_opt_eval, index_opt_eval, _, _ = self.eval_func_object.evaluation(alt_min, alt_max, tas_profile, gamma_profile, time_profile, mass_flmin, registration, min_alt_step, ff_cruise, mach_cruise, ias_min, ias_max, mach_min, mach_max)
		
		except ValueError as err:
			print err
			if err[1] == 1:
				msg = 'Problem in operating speeds: ' + str(err[0])
				print msg
				raise cherrypy.HTTPError(400, msg)
			else:
				raise err

		print '\tIndex = ' + str(index_opt_eval)
		print '\tSelected steps = ' + str(steps_opt_eval)

		result = {'steps_opt':steps_opt_eval, 'eval_index':index_opt_eval, 'pkg_version':pkg_resources.require('StepsEvaluation')[0].version}

		print '*** dict into Response'
		return json.dumps(result)


	def check_request(self, request_dict):

		keys = ['alt_min', 'alt_max', 'tas_profile', 'gamma_profile', 'time_profile', 'mass_profile', 'registration', 'ff_profile', 'ias_min', 'ias_max', 'mach_min', 'mach_max']
		if sum([z in request_dict.keys() for z in keys]) != len(keys):
			msg = 'Parameter missing in request. Must be ' + str(keys)
			print msg
			raise cherrypy.HTTPError(400, msg)

		alt_min = np.array(request_dict['alt_min'])
		alt_max = np.array(request_dict['alt_max'])
		tas_profile = np.array(request_dict['tas_profile'])
		gamma_profile = np.array(request_dict['gamma_profile'])
		time_profile = np.array(request_dict['time_profile'])
		mass_profile = request_dict['mass_profile']
		mass_flmin = mass_profile[0]
		ff_profile = request_dict['ff_profile']
		ff_cruise = ff_profile[-1]
		registration = request_dict['registration']
		
		ias_min = request_dict['ias_min']
		ias_max = request_dict['ias_max']
		mach_min = request_dict['mach_min']
		mach_max = request_dict['mach_max']


		if request_dict.has_key('mach_cruise'):
			print 'Generating 3 IAS and the cruise mach at crossover'
			mach_cruise = request_dict['mach_cruise']
		else:
			print 'Generating 2 IAS and 1 mach'
			mach_cruise = None

		if request_dict.has_key('min_alt_step'):
			min_alt_step = request_dict['min_alt_step']
			print 'min_alt_step = ' + str(min_alt_step)
		else:
			min_alt_step = 50
			print 'min_alt_step = ' + str(min_alt_step)
		
		if alt_min < 1000 or alt_max > 50000:
			print '\t\talt_min cannot be lower than 1000 and alt_max cannot be larger than 50000\n'
			raise cherrypy.HTTPError(400, 'alt_min cannot be lower than 1000 and alt_max cannot be larger than 50000')

		if np.min(tas_profile) < 1 or np.max(tas_profile) > 1000:
			print '\t\ttas_profile cannot be lower than 1 and larger than 1000\n'
			raise cherrypy.HTTPError(400, 'tas_profile cannot be lower than 1 and larger than 1000')

		if (mach_cruise is not None) and (mach_cruise <= 0 or mach_cruise >= 1):
			print '\t\tmach_cruise cannot be lower than 0 and larger than 1\n'
			raise cherrypy.HTTPError(400, 'mach_cruise cannot be lower than 0 and larger than 1')

		if mass_flmin <= 1:
			print '\t\tmass_flmin cannot be lower than 1\n'
			raise cherrypy.HTTPError(400, 'mass_flmin cannot be lower than 1')

		if registration not in self.eval_func_object.registrations:
			print '\t\tregistration not in available registration list\n'
			print self.eval_func_object.registrations
			raise cherrypy.HTTPError(400, 'registration not in available registration list')

		if min_alt_step < 40:
			print '\t\tmin_alt_step too low (default = 50)\n'
			raise cherrypy.HTTPError(400, 'min_alt_step too low (default = 50)')

		if min_alt_step > 1000:
			print '\t\tmin_alt_step too high (default = 50)\n'
			raise cherrypy.HTTPError(400, 'min_alt_step too high (default = 50)')

		if ff_cruise > 10:
			print '\t\tff_cruise too high. It should be in kg/s\n'
			raise cherrypy.HTTPError(400, 'ff_cruise too high. It should be in kg/s')

		if ff_cruise < 0:
			print '\t\tff_cruise too low. It should be in kg/s\n'
			raise cherrypy.HTTPError(400, 'ff_cruise too low. It should be in kg/s')
		
		if (time_profile < 0).any():
			print '\t\ttime_profile must be non negative\n'
			raise cherrypy.HTTPError(400, 'time_profile must be non negative')

		if (time_profile > 10000).any():
			print '\t\ttime_profile contains too large values\n'
			raise cherrypy.HTTPError(400, 'time_profile contains too large values')

		if time_profile[0] > 0:
			time_profile -= np.min(time_profile)

		if ( (gamma_profile < -45) | (gamma_profile > 45) ).any():
			print '\t\tgamma_profile contains non coherent values\n'
			raise cherrypy.HTTPError(400, 'gamma_profile contains non coherent values')


		return alt_min,alt_max,tas_profile,gamma_profile,time_profile,mass_flmin,registration,mach_cruise,min_alt_step,ff_cruise,ias_min,ias_max,mach_min,mach_max


	@cherrypy.expose
	def reload(self):
		print '*** RELOAD'
		print '*** Import Evaluation object'
		
		# IMPORT THE LAST OBJECT
		path = 'models/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			eval_func_object = cPickle.load(f)

		msg = path+listdir[-1] + ' imported\n'

		if eval_func_object.status == 0:
			msg = "The status equals 0. The models must be trained using 'load_and_learn' method."
			print msg
			return msg

		self.eval_func_object = eval_func_object

		print msg
		return msg


if len(sys.argv) == 3:
	host = sys.argv[1]
	port = int(sys.argv[2])
else:
	print 'Initialize web service at localhost'
	host = '127.0.0.1'
	port = 8080



if __name__ == '__main__':
	cherrypy.config.update({
		'server.socket_host' : host,
		'server.socket_port' : port,
		'server.thread_pool' : 5,
		'tools.sessions.on' : True,
		'tools.encode.encoding' : 'Utf-8'
	})
	cherrypy.quickstart(WebService())

