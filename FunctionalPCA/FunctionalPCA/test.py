import sys, csv, math, os, traceback
from sys import exit
import numpy as np, matplotlib.pyplot as plt, seaborn as sns

sys.path.append('../../DataExtractor/')
from DataExtractor import *
from FPCA import FPCA


Folder = '../../../Data/TVF/dataTest/TVF-extract/'
Files = ['F-GZHA-0415160.csv', 'F-GZHC-0415190.csv', 'F-GZHG-0415170.csv', 'F-GZHK-0415080.csv', 'F-GZHL-0415190.csv', 'F-GZHO-0415170.csv', 'F-GZHA-0415180.csv', 'F-GZHE-0415130.csv', 'F-GZHI-0415030.csv', 'F-GZHK-0415140.csv', 'F-GZHM-0415040.csv', 'F-GZHO-0531050.csv', 'F-GZHA-0415210.csv', 'F-GZHE-0415170.csv', 'F-GZHI-0415060.csv', 'F-GZHK-0415180.csv', 'F-GZHM-0415080.csv', 'F-GZHO-0531100.csv', 'F-GZHA-0516080.csv', 'F-GZHE-0719120.csv', 'F-GZHI-0415080.csv', 'F-GZHK-0812050.csv', 'F-GZHN-0415040.csv', 'F-GZHP-0415060.csv', 'F-GZHA-0516120.csv', 'F-GZHE-0722190.csv', 'F-GZHI-0415110.csv', 'F-GZHK-0812090.csv', 'F-GZHN-0415070.csv', 'F-GZHP-0415090.csv', 'F-GZHB-0415090.csv', 'F-GZHF-0415060.csv', 'F-GZHI-0415130.csv', 'F-GZHK-0812140.csv', 'F-GZHN-0415100.csv', 'F-GZHP-0415130.csv', 'F-GZHB-0415120.csv', 'F-GZHF-0415090.csv', 'F-GZHI-0415180.csv', 'F-GZHK-0812170.csv', 'F-GZHN-0415130.csv', 'F-GZHP-0415190.csv', 'F-GZHB-0415160.csv', 'F-GZHF-0415140.csv', 'F-GZHI-0724080.csv', 'F-GZHL-0415040.csv', 'F-GZHN-0415160.csv', 'F-GZHV-0415100.csv']
extractor = DataExtractor(params=['ALTITUDE', 'CAS', 'FF1', 'FF2'], ns=None, flight_phase='climb')
Time, data_sparse, data_list = extractor.run(Files, Folder, ncpus=-1)

Alt = data_sparse[0]
Cas = data_sparse[1]
ff1 = data_sparse[2]
ff2 = data_sparse[3]
ff = ff1 + ff2


# Step 2: FPCA
x = Alt
PCobj = FPCA(.9, 32)
PCobj.fit(x, False)
PC_alt = PCobj.PCs
print 'FPCA ALTITUDE'
print PCobj.PCs.shape

x = Cas
PCobj = FPCA(.9, 32)
PCobj.fit(x, False)
PC_cas = PCobj.PCs
print 'FPCA AIRSPEED'
print PCobj.PCs.shape

x = ff
PCobj = FPCA(.9, 32)
PCobj.fit(x, False)
PC_ff = PCobj.PCs
print 'FPCA FUEL FLOW'
print PCobj.PCs.shape


# DATA FOR LEARNING STEP
designMatrix = np.concatenate((PC_alt, PC_cas), axis=1)
Y = PC_ff
print designMatrix.shape






