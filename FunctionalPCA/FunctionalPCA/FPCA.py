"""!@module Contains the FPCA class which computes the new explicative coefficients of a vector and can project any vector on the same base.
"""

import numpy as np
import scipy.interpolate as si
from scipy.interpolate import UnivariateSpline
from sklearn.decomposition import PCA

class FPCA:
	"""!@brief Functional Principal Components Analysis for sparse and non sparse data
	"""
	def __init__(self, propVar, nBasisInit, verbose=True):
		"""!@brief Instantiation.
			@param x: data
			@param propVar: the percentage of explained variance for choosing the principal components
			@param nBasisInit: number of Spline coefficients we want for the smoothing step
			@param verbose: should the details be printed ?
		"""
		self.propVar = propVar
		self.nBasisInit = nBasisInit
		self.verbose = verbose
		self.PCs = None
		self.idxPCs = None
		self.smoothData = None
		self.splineBasis = None
		self.eigenFunc = None
		self.pca = None
		self.reconstruct = False

	def fit(self, x, reconstruct=False):
		"""!@brief Reduce the dimension of the data by computing the FPCA.
			@param reconstruct: should the curves be reconstructed after dimension reduction ?
		"""
		## STEP 1: smoothing spline

		nBasis = self.nBasisInit - 1

		dims = x.shape
		N = dims[1]
		n = dims[0]
		t = np.linspace(0., 1., N)

		# Construct Splines fonctions
		norder = 4
		nrPointsToEval = N
		nknots = nBasis - norder + 2
		knots = np.linspace(0.0, 1, nknots+2)
		
		ref = x[0,:]
		xRepres = si.splrep(t, ref, k=norder, t=knots[1:(nknots-1)])
		ipl_t = np.linspace(0., 1., nrPointsToEval)
		basis = np.zeros((nrPointsToEval,nBasis+1))
		for i in range(nBasis+1):
		    coeffs = np.zeros(nBasis+1+norder+1)
		    coeffs[i] = 1.0
		    x_list = list(xRepres)
		    x_list[1] = coeffs.tolist()
		    basis[:,i] = si.splev(ipl_t, x_list)

		# Normalize the basis functions
		u,s,v = np.linalg.svd(basis, False)
		basis = u
		self.splineBasis = basis

		# Project the data on the spline basis
		tmpx = x
		idx_nan = np.where(np.isnan(tmpx))
		tmpx[np.isnan(tmpx)] = 0
		C = np.dot(tmpx, basis)
		# print C

		## STEP 2: perform PCA on the spline coefficients
		pca = PCA()
		PCtr = pca.fit_transform(C)
		eigFunc = pca.components_ # eigen functions: array of size n_components x n_features

		explVar = np.cumsum(pca.explained_variance_ratio_)
		idxPCs = np.where(explVar <= self.propVar)[0]
		if len(idxPCs)<=1:
			idxPCs = np.array([0,1])
		PCs = PCtr[:,idxPCs]
		self.PCs = PCs
		self.idxPCs = idxPCs
		self.eigenFunc = eigFunc[:,idxPCs]
		self.pca = pca

		if self.verbose:
			pass
			# print str(len(idxPCs)) + " selected PCs over " + str(nBasis+1)

		if reconstruct:
			if idx_nan[0] is not []:
				print 'Warning: the reconstructed curves might not be well computed!'

			newPCs = np.concatenate((PCs, np.zeros((n,nBasis+1-len(idxPCs)))), axis=1)
			C_estimate = pca.inverse_transform(newPCs)
			smoothData = np.dot(C_estimate, basis.T)
			self.smoothData = smoothData
			self.reconstruct = True


	def project(self, newx, debug=False):
		"""!@brief Project a new data of the Karhunen-Loeve expansion.
			@param newx: new data
			@param debug: should the details be printed ?
			@param f : file where commentaries are written.

			@return The design matrix containing the PCs.

			@code
			xRef = np.loadtxt("data/eap_wind.csv", delimiter=',', skiprows=1)
			fpca = FPCA(xRef[0:1000,:], .99, 64, True)
			fpca.fit(True)
			newPCs = fpca.project(xRef[1001:1200,:], debug=True)
		"""

		C = np.dot(newx, self.splineBasis)
		# PCs = np.dot(C, self.eigenFunc)
		PCs = self.pca.transform(C)[:,self.idxPCs]
		nBasis = self.nBasisInit - 1
		nPC = PCs.shape[1]
		n = PCs.shape[0]

		if debug:
			# print "Check the reconstructed curve"
			newPCs = np.concatenate((PCs, np.zeros((n,nBasis+1-nPC))), axis=1)
			C_estimate = self.pca.inverse_transform(newPCs)
			smoothData = np.dot(C_estimate, self.splineBasis.T)
			
			# print newx.shape
			if len(newx.shape)==1:
				xToPlot = newx
			else:
				xToPlot = newx[0,:]
			t = np.linspace(0., 1., len(xToPlot))

			# print " C " + str(C.shape)
			# print "PCs " + str(PCs.shape) + " NewPcs " + str(newPCs.shape) + " C_estimate " + str(C_estimate.shape) + " smooth " + str(smoothData.shape) + " t " + str(len(t))
			# print PCs[0,:]
			# print newPCs[0,:]

			if plt.fignum_exists(1):
				plt.figure()
			plt.plot(t, xToPlot, 'o')
			plt.plot(t, smoothData[0,:], color='r')
			plt.show()
		return PCs


	def inverse_transform(self, newPCs):
		dims = newPCs.shape
		n = dims[0]

		# print n, self.nBasisInit, len(self.idxPCs), self.nBasisInit - 1+1-len(self.idxPCs)
		newnewPCs = np.concatenate((newPCs, np.zeros((n,self.nBasisInit - 1+1-len(self.idxPCs)))), axis=1)
		C_estimate = self.pca.inverse_transform(newnewPCs)
		smoothData = np.dot(C_estimate, self.splineBasis.T)
				
		return smoothData

	def plot(self, idx):
		"""!@brief Plot method.
			@param idx: the indexes of the curves to be plotted. It can be a scalar or a list, e.g. 'range(10)'

			@code
			xRef = np.loadtxt("data/eap_wind.csv", delimiter=',', skiprows=1)
			fpca = FPCA(xRef, .99, 64, True)
			fpca.fit(True)
			fpca.plot(range(100))
		"""
		if self.reconstruct==False:
			f.write("ERROR: You must fit the functional PCA with 'reconstruct == True' before plotting the results\n")
			return
		z = self.x[idx,:]
		t = np.linspace(0., 1., self.x.shape[1])
		# if plt.fignum_exists(1):
		# 	plt.figure()
		if type(idx)==int:
			plt.plot(t, z, 'o')
			plt.plot(t, self.smoothData[idx,:], color='r')
		elif type(idx)==list:
			for i in idx:
				plt.plot(t, self.smoothData[i,:])
		else:
			f.write("Unknown type for idx parameter\n")
			return
