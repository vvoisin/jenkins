==============================
Functional PCA Python package
==============================


Installation
============

- Unzip tar archive
- Enter the folder
- Type ‘sudo python setup.py install’


Documentation
=============

FPCA for sparse and non sparse functional data
