# !/usr/bin/env python
# -*- coding:utf-8 -*-

import json, cherrypy, sys, traceback
import numpy as np

from sys import exit
from StepsEvaluation.Evaluation import *

reload(sys)  
sys.setdefaultencoding('utf8')

class WebService(object):


	def __init__(self):

		print '*** Import Evaluation object'
		
		# IMPORT THE LAST OBJECT
		path = 'models/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			eval_func_object = cPickle.load(f)

		print path+listdir[-1] + ' imported\n'

		if eval_func_object.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load_and_learn' method."
			print msg
			return msg

		self.eval_func_object = eval_func_object


	@cherrypy.expose
	def index(self):
		if 'Content-Length' not in cherrypy.request.headers:
			return 'No data received. Use a POST request.'

		cl = cherrypy.request.headers['Content-Length']
		request = cherrypy.request.body.read(int(cl))
		return self.process(request)


	@cherrypy.expose
	def test(self):
		return 'Test ok'


	def process(self, request):

		print '*** Request into dict'
		request_dict = eval(request)

		keys = ['alt_profile', 'ias_profile', 'mach_profile', 'mass_flmin', 'steps']
		if sum([z in request_dict.keys() for z in keys]) != 5:
			msg = 'Parameter missing in request. Must be ' + str(keys)
			print msg
			return msg

		steps = request_dict['steps']
		alt_profile = np.array(request_dict['alt_profile'])
		ias_profile = np.array(request_dict['ias_profile'])
		mach_profile = np.array(request_dict['mach_profile'])
		mass_flmin = request_dict['mass_flmin']


		print '*** Processing'
		index_consumption, index_distance, rmse_ias, rmse_mach = self.eval_func_object.evaluation(alt_profile, ias_profile, mach_profile, mass_flmin, steps)
		
		print '\tRMSE IAS (steps 1 & 2) = ' + str(rmse_ias)
		print '\tRMSE Mach (step 3) = ' + str(rmse_mach)
		print '\tIndex consumption (RMSE in kg) = ' + str(index_consumption)
		print '\tindex distance (RMSE in km) = ' + str(index_distance)


		print 'Warning: evaluation index is only based on consumption model...'
		# result = {'rmse_ias':rmse_ias, 'rmse_mach':rmse_mach, 'rmse_consumption':index_consumption, 'rmse_distance':index_distance}
		result = {'rmse_consumption':index_consumption}

		print '*** dict into Response'
		return json.dumps(result)




	@cherrypy.expose
	def reload(self):
		print '*** RELOAD'
		print '*** Import Evaluation object'
		
		# IMPORT THE LAST OBJECT
		path = 'models/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			eval_func_object = cPickle.load(f)

		msg = path+listdir[-1] + ' imported\n'

		if eval_func_object.status == 0:
			msg = "The status equals 0. The models must be trained using 'load_and_learn' method."
			print msg
			return msg

		self.eval_func_object = eval_func_object

		print msg
		return msg


if len(sys.argv) == 3:
	host = sys.argv[1]
	port = int(sys.argv[2])
else:
	print 'Initialize web service at localhost'
	host = '127.0.0.1'
	port = 8080



if __name__ == '__main__':
	cherrypy.config.update({
		'server.socket_host' : host,
		'server.socket_port' : port,
		'server.thread_pool' : 5,
		'tools.sessions.on' : True,
		'tools.encode.encoding' : 'Utf-8'
	})
	cherrypy.quickstart(WebService())

