# !/usr/bin/env python
# -*- coding:utf-8 -*-

import json, cherrypy, sys, traceback
import numpy as np

from sys import exit
# from StepsEvaluation.Evaluation import *

# ENLEVER DES QUE C EST OK
sys.path.append('../OC-StepsEvaluation-package/StepsEvaluation/')
from Evaluation import *



reload(sys)  
sys.setdefaultencoding('utf8')

class WebService(object):


	def __init__(self):

		print '*** Import Evaluation object'
		
		# IMPORT THE LAST OBJECT
		path = 'models/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			eval_func_object = cPickle.load(f)

		print path+listdir[-1] + ' imported\n'

		if eval_func_object.status == 0:
			msg = "The status equals to 0. The models must be trained using 'load_and_learn' method."
			print msg
			return msg

		self.eval_func_object = eval_func_object


	@cherrypy.expose
	def index(self):
		if 'Content-Length' not in cherrypy.request.headers:
			return 'No data received. Use a POST request.'

		cl = cherrypy.request.headers['Content-Length']
		request = cherrypy.request.body.read(int(cl))
		return self.process(request)


	@cherrypy.expose
	def test(self):
		return 'Test ok'


	def process(self, request):

		print '*** Request into dict'
		request_dict = eval(request)
		alt_profile,ias_profile,mach_profile,mass_flmin,registration,mach_cruise,min_alt_step = self.check_request(request_dict)

		print '*** Processing'
		steps_opt_consumption, indexe_opt_consumption, _, _ = self.eval_func_object.evaluation(alt_profile, ias_profile, mach_profile, mass_flmin, registration, min_alt_step, mach_cruise)
		
		print '\tIndex consumption (RMSE in kg) = ' + str(indexe_opt_consumption)
		print '\tSelected steps = ' + str(steps_opt_consumption)

		print 'Warning: evaluation index is only based on consumption model...'
		result = {'steps_opt':steps_opt_consumption, 'rmse_consumption':indexe_opt_consumption}

		print '*** dict into Response'
		cherrypy.response.headers['Content-Type'] = 'application/json'
		return json.dumps(result)


	def check_request(self, request_dict):

		keys = ['alt_profile', 'ias_profile', 'mach_profile', 'mass_flmin', 'registration']
		if sum([z in request_dict.keys() for z in keys]) != 5:
			msg = 'Parameter missing in request. Must be ' + str(keys)
			print msg
			raise cherrypy.HTTPError(400, msg)

		alt_profile = np.array(request_dict['alt_profile'])
		ias_profile = np.array(request_dict['ias_profile'])
		mach_profile = np.array(request_dict['mach_profile'])
		mass_flmin = request_dict['mass_flmin']
		registration = request_dict['registration']

		if request_dict.has_key('mach_cruise'):
			print 'Generating 3 IAS and the cruise mach at crossover'
			mach_cruise = request_dict['mach_cruise']
		else:
			print 'Generating 2 IAS and 1 mach'
			mach_cruise = None

		if request_dict.has_key('min_alt_step'):
			min_alt_step = request_dict['min_alt_step']
			print 'min_alt_step = ' + str(min_alt_step)
		else:
			min_alt_step = 50
			print 'min_alt_step = ' + str(min_alt_step)
		
		if np.min(alt_profile) < 1000 or np.max(alt_profile) > 50000:
			print '\t\talt_profile cannot be lower than 1000 and larger than 50000\n'
			raise cherrypy.HTTPError(400, 'alt_profile cannot be lower than 1000 and larger than 50000')
		if np.min(ias_profile) < 1 or np.max(ias_profile) > 1000:
			print '\t\tias_profile cannot be lower than 1 and larger than 1000\n'
			raise cherrypy.HTTPError(400, 'ias_profile cannot be lower than 1 and larger than 1000')
		if np.min(mach_profile) <= 0 or np.max(mach_profile) >= 1 or mach_cruise <= 0 or mach_cruise >= 1:
			print '\t\tmach_profile and mach_cruise cannot be lower than 0 and larger than 1\n'
			raise cherrypy.HTTPError(400, 'mach_profile and mach_cruise cannot be lower than 0 and larger than 1')
		if mass_flmin <= 1:
			print '\t\tmass_flmin cannot be lower than 1\n'
			raise cherrypy.HTTPError(400, 'mass_flmin cannot be lower than 1')
		if registration not in self.eval_func_object.registrations:
			print '\t\tregistration not in available registration list\n'
			raise cherrypy.HTTPError(400, 'registration not in available registration list')
		if min_alt_step < 40:
			print '\t\tmin_alt_step too low (default = 50)\n'
			raise cherrypy.HTTPError(400, 'min_alt_step too low (default = 50)')
		if min_alt_step > 1000:
			print '\t\tmin_alt_step too high (default = 50)\n'
			raise cherrypy.HTTPError(400, 'min_alt_step too high (default = 50)')

		return alt_profile,ias_profile,mach_profile,mass_flmin,registration,mach_cruise,min_alt_step


	@cherrypy.expose
	def reload(self):
		print '*** RELOAD'
		print '*** Import Evaluation object'
		
		# IMPORT THE LAST OBJECT
		path = 'models/'
		mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
		listdir = list(sorted(os.listdir(path), key=mtime))
		if '.DS_Store' in listdir:
			listdir.pop(listdir.index('.DS_Store'))

		with open(path+listdir[-1], 'r') as f:
			eval_func_object = cPickle.load(f)

		msg = path+listdir[-1] + ' imported\n'

		if eval_func_object.status == 0:
			msg = "The status equals 0. The models must be trained using 'load_and_learn' method."
			print msg
			return msg

		self.eval_func_object = eval_func_object

		print msg
		return msg


if len(sys.argv) == 3:
	host = sys.argv[1]
	port = int(sys.argv[2])
else:
	print 'Initialize web service at localhost'
	host = '127.0.0.1'
	port = 8080



if __name__ == '__main__':
	cherrypy.config.update({
		'server.socket_host' : host,
		'server.socket_port' : port,
		'server.thread_pool' : 5,
		'tools.sessions.on' : True,
		'tools.encode.encoding' : 'Utf-8'
	})
	cherrypy.quickstart(WebService())

