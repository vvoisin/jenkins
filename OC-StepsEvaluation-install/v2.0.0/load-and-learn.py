import sys, csv, math, os, traceback
import numpy as np

from sys import exit
from StepsEvaluation.Evaluation import *


if len(sys.argv) != 4:
	print 'Usage: python load-and-learn.py [data_path] [nr_flights] [regressor]'
	exit(0)

# 
# OFFLINE : load data and learn model
# 

# INPUT PARAMETERS
data_path = sys.argv[1]
nr_flights = int(sys.argv[2])
regressor = sys.argv[3]			# regressor in ('ETR', 'RF', 'SVR')
 				


# EVALUATION OBJECT
eval_funct = Evaluation(data_path, nr_flights, 'models/', regressor)
eval_funct.load_and_learn()
eval_funct.serialize('eval-func'+regressor)

