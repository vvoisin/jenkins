

echo 'Extract ConsumptionModel package'
tar -xzvf ConsumptionModel-1.0.0.tar.gz
cd ConsumptionModel-1.0.0/

echo 'Installing ConsumptionModel package, v1.0.0'
python setup.py install --v 1.0.0


cd ..

echo 'Extract StepsEvaluation package'
tar -xzvf StepsEvaluation-2.0.0.tar.gz
cd StepsEvaluation-2.0.0/

echo 'Installing StepsEvaluation package, v2.0.0'
python setup.py install --v 2.0.0

cd ..

echo 'Installing main directory'
mkdir ../StepsEvaluation-2.0.0-ws/
mkdir ../StepsEvaluation-2.0.0-ws/models/
cp load-and-learn.py Web-service.py ../StepsEvaluation-2.0.0-ws/


echo 'Cleaning temporary files'
rm -rf ConsumptionModel-1.0.0/ StepsEvaluation-2.0.0/
