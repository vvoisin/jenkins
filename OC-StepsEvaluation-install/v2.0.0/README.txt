-----------------------------------------------
---											---
---		Package Generation de consignes		---
---											---
-----------------------------------------------


---	RÉSUMÉ ---


Evaluation d'un set de consignes via un modèle statistique de prédiction de la consommation instantanée. 
Chaque jeu de consignes (2 ou 3 consignes IAS et une consigne Mach) fournit un profil constant par morceaux et est comparé à un profil optimisé par Bocop.

Deux étapes :
	
	1- (Offline Learning Step) Extraction des données de vol et construction d'un modèle de régression pour la prédiction de la consommation en fonction de l'altitude, de la masse et de la vitesse.
	
	2- (Online Evaluation) Le modèle est évalué pour chaque jeu de consignes à la fois pour le profil Bocop et pour le profil de consignes. Autrement dit, une prédiction de la consommation instantanée est faite pour chaque altitude. Le modèle construit ainsi une prédiction de la courbe de consommation instantanée en fonction de l'altitude. L'indicateur permettant d'évaluer un jeu de consignes est alors donné par le RMSE (Root Mean Squared Error) entre la consommation prédite du profil Bocop et la consommation prédite du profil de consignes.

	En pratique, l'interface entre l'application (en Java) et les modèles statistiques (en Python) est effectuée via un web service Python (package cherrypy). L'application Java envoie des requètes à Python via ce web service et selon un format JSON. Les données réçues sont traitées et renvoyées à Java en JSON également.



---	PRE-REQUIS ---

- Installation des paquets "python-dev", "python-numpy", "python-scipy" et "python-setuptools" via apt

- Installation manuelle de Parallel Python :

	wget http://www.parallelpython.com/downloads/pp/pp-1.5.7.tar.gz
	tar xvzf pp-1.5.7.tar.gz
	cd pp-1.5.7 && python setup.py install


---	INSTALLATION ---

L'installation se fait en local en executant le script bash 'install.sh' : 

sudo sh install.sh

Cette commande installe dans un premier temps le package 'ConsumptionModel' contenant la librairie statistique pour construire les modèles puis dans un second temps la librarie principale 'StepsEvaluation'. Les autres dépendances sont automatiquement installées via les dépots distant de Python. 

Liste des dépendances :
	- cherrypy
	- pickle / cPickle
	- numpy, scipy
	- pandas


Ensuite, le script installe le dossier principal (web service) dans le dossier parent.


--- UTILISATION ---


Architecture de la librairie :

StepsEvaluation/
		
		(Module) Evaluation.py
			(Class) Evaluation
				Construction d'un modèle de consommation (méthode 'load_and_learn')
				Génération de toutes les consignes possibles et selection du jeu de consignes optimal (méthode 'evaluation').

		(Module) DataExtractor.py
			(Class) DataExtractor
				Utilitaire d'extraction des données de vol pour les modèles statistiques. Utilisé uniquement dans la classe 'Evaluation'. ATTENTION : les données de vol doivent être préalablement extraites en csv.

		(Module) crossover.py
			Fonctions utilitaires pour le calcul de l'altitude de crossover sachant une vitesse IAS et un mach


Architecture du programme principal :

StepsEvaluation-ws/

		(Script) load-and-learn.py
			Extraction des données, construction du modèle et serialisation de l'objet
			Usage: python load-and-learn.py [data_path] [nr_fights] [output_path] [regressor]

		(Script) Web-service.py
			(Class) WebService
				Initialisation du web service pour l'échange de données entre Java et Python à travers des requètes au format JSON.
				Usage: python Web-service.py [host] [port]
				Si les paramètres 'host' et 'port' ne sont pas renseignés, les valeurs par défaut sont host = 127.0.0.1 et port = 8080.

				Cette classe contient trois méthodes pouvant être appelées depuis Java :
					1- test : Teste le web service et renvoie 'Test ok'. Appel depuis Java : host:port/test
					2- index : récupère les données en JSON, charge le dernier modèle du dossier 'models/' et exécute les calculs d'évaluation. Appel depuis Java : host:port/
					3- reload : recharge le dernier modèle du dossier 'models/'. Appel depuis Java : host:port/reload

		(Dossier) models
			Contient les objets serialisés et utilisables ensuite. DOSSIER A NE PAS SUPPRIMER! ACTUELLEMENT PAS DE TEST.


Utilisation standard :
(1) python load-and-learn.py [data_path] [nr_fights] [regressor]
(2) python Web-service.py [host] [port]

Note : si (1) n'est pas exécuté ou ne s'est pas terminé correctement, (2) retourne l'erreur suivante :

'The status equals 0. The models must be trained using 'load_and_learn' method.'



---	FORMAT D'ÉCHANGE ---

Le format d'échange entre l'application Java et le programme Python est le JSON. Les clés sont :
	
	- ias_profile : liste de valeurs d'IAS (sortie de Bocop)
	- mach_profile : liste de valeurs de mach (sortie de Bocop)
	- alt_profile : liste de valeurs d'altitude (sortie de Bocop)
	- mass_flmin : masse en kg au niveau minimum (sortie de Bocop)
	- registration
	- min_alt_step : borne fixant l'altitude (en niveau de vol) minimale de chaque step, optionnel, default: 50
	- mach_cruise: mach de croisière, optionnel. Permet de générer 3 consignes d'IAS et de calculer l'altitude de crossover.

Le web service renvoie un message d'erreur (400 Bad request) si la requête n'est pas valide. En particulier, si les clés ne sont pas correctement renseignées, un message d'erreur indique :

	'Parameter missing in request. Must be ['alt_profile', 'ias_profile', 'mach_profile', 'mass_flmin', 'registration']'


Le web service renvoie le jeu de consignes optimale en JSON. Les clés sont:
	- steps_opt : jeu de consignes optimales
	- rmse_consumption : indice d'évaluation


Exemple de données d'entrée :

{
	"ias_profile": [223.74146798555054, 223.74146798555054, 225.5136247246629, 227.28870966205108, 229.05630209012315, 230.80685371834784, 232.53186192999257, 234.2241393000638, 235.87911473749435, 237.49981621775817, 239.20324761227937, 241.24721282726148, 243.83318002318427, 247.06503368808495, 251.05273776637912, 255.94879686258264, 261.9879394618242, 269.57804697713965, 275.8575788292394, 279.5936710125211, 282.54377850978403, 284.65488600210676, 286.0561570360903, 286.874838759703, 287.222497054828, 287.1914796453656, 286.8574706488398, 286.2816205888755, 285.5132888749574, 284.59329074337154, 283.5548728833136, 282.4251834787232, 281.22727270039337, 279.98022517378945, 278.6999537130334, 277.40071952660975, 276.0944675884946, 274.7918267473265, 273.50197973290005, 272.2329129559993, 270.9922957942651, 269.7869155745967, 268.6227250063057, 267.50560643585743, 266.44078897133153, 265.43294918684944, 264.48706310174236, 263.6076774626022, 262.79879060012667, 262.06467191927806, 261.40913633496183, 260.8355440503447, 260.34775159049275, 259.949235941157, 259.642827269146, 259.4313191866756, 259.3175254866721, 259.3036254055716, 259.3916010373101, 259.58272017578906, 259.8775238879187, 260.2761982741842, 260.7777508856404, 261.3803810416628, 262.0810932194292, 262.87478124703995, 263.75490193669464, 264.7135295383686, 265.7402838369564, 266.82302477223186, 267.94719800014275, 269.09612820712675, 270.2523261411564, 271.3969362665272, 272.511424549661, 273.5782113394678, 274.5803363902591, 275.50386404111083, 276.33910218023686, 277.07943053167077, 278.22746697656146, 280.5509935818453, 283.89389158538427, 286.69135624450286, 288.0611118006014, 288.41678953431096, 288.04465960177095, 287.20296800958107, 286.09665130197, 284.85424976512815, 283.5545239017676, 282.24923164362104, 280.9747746848468, 279.7569661039924, 278.61668624822767, 277.57215343519886, 276.63781519432325, 275.8262923278394, 275.1474732169637, 274.6076021149629, 274.20712106216973], 

	"mach_profile": [0.4136615941602992, 0.4136615941602992, 0.4189439876428133, 0.42426779657844194, 0.42961367856584093, 0.4349635632076157, 0.44030101726975435, 0.4456118025674272, 0.4508864206858564, 0.4561297992720291, 0.46156689994465305, 0.46769989295713477, 0.4749306080527369, 0.48347730247606735, 0.49357404911985114, 0.505543053896936, 0.5198753761940318, 0.5374160133917917, 0.5524772703246806, 0.5625428033644893, 0.5710946577379844, 0.5780035651495283, 0.5835098024834391, 0.5878544303795731, 0.5912508954380553, 0.5938770087031736, 0.5958793736264324, 0.5973770663293739, 0.5984667149207139, 0.599228791909684, 0.5997292835266342, 0.6000224623039448, 0.6001548731926699, 0.6001654279452993, 0.6000869295888596, 0.599949212441704, 0.5997776224964648, 0.5995950913084677, 0.5994217902696232, 0.5992756129552915, 0.5991740633517848, 0.5991329875435382, 0.5991666383340879, 0.5992893482410647, 0.5995142159795668, 0.5998533034145297, 0.6003195434477332, 0.6009250947803385, 0.6016810491691833, 0.6025992897713345, 0.6036908300785406, 0.6049657918894363, 0.6064355988986581, 0.6081109535429208, 0.6100011910690709, 0.6121156878508032, 0.614463994809843, 0.617054281213548, 0.6198943486715752, 0.6229903806405391, 0.6263468767134323, 0.6299675157570847, 0.6338531361905576, 0.6380025795614631, 0.6424117011617244, 0.6470710537479629, 0.6519674427961578, 0.6570839858910779, 0.6623973673118345, 0.6678794467043809, 0.6734955194097078, 0.6792049270229551, 0.6849642547282787, 0.6907258781188719, 0.6964421995847813, 0.7020672842374797, 0.7075560422174503, 0.7128704801363468, 0.717983024835819, 0.7228736896561812, 0.7288562107513583, 0.7379634434618435, 0.7498212583347005, 0.7603131001713632, 0.7670721538714557, 0.7711580521000004, 0.7733063078206556, 0.7741892414769881, 0.7743460905976672, 0.7741174132187455, 0.773713561279828, 0.7732743681840182, 0.772899711408177, 0.77266184191347, 0.7726204889107278, 0.7728289142406829, 0.7733306702046789, 0.7741648497647468, 0.7753634360148701, 0.7769485723479479, 0.7789262287792308], 

	"alt_profile": [10000.0, 10267.45406824147, 10534.90813648294, 10802.362204724408, 11069.816272965878, 11337.270341207348, 11604.724409448818, 11872.178477690288, 12139.632545931758, 12407.086614173228, 12674.540682414698, 12941.994750656168, 13209.448818897636, 13476.902887139107, 13744.356955380576, 14011.811023622047, 14279.265091863515, 14546.719160104987, 14814.173228346455, 15081.627296587925, 15349.081364829395, 15616.535433070865, 15883.989501312333, 16151.443569553805, 16418.897637795275, 16686.351706036745, 16953.805774278215, 17221.259842519685, 17488.71391076115, 17756.167979002625, 18023.622047244095, 18291.076115485565, 18558.53018372703, 18825.9842519685, 19093.438320209974, 19360.89238845144, 19628.34645669291, 19895.80052493438, 20163.254593175854, 20430.70866141732, 20698.16272965879, 20965.61679790026, 21233.07086614173, 21500.5249343832, 21767.979002624666, 22035.43307086614, 22302.88713910761, 22570.34120734908, 22837.79527559055, 23105.24934383202, 23372.70341207349, 23640.15748031496, 23907.61154855643, 24175.065616797896, 24442.51968503937, 24709.973753280836, 24977.42782152231, 25244.881889763776, 25512.33595800525, 25779.790026246716, 26047.244094488186, 26314.698162729655, 26582.152230971125, 26849.606299212595, 27117.06036745406, 27384.514435695535, 27651.968503937005, 27919.422572178475, 28186.87664041995, 28454.33070866142, 28721.784776902885, 28989.238845144355, 29256.69291338582, 29524.14698162729, 29791.601049868765, 30059.055118110235, 30326.509186351705, 30593.963254593178, 30861.41732283464, 31128.871391076114, 31396.32545931758, 31663.77952755905, 31931.23359580052, 32198.687664041994, 32466.141732283464, 32733.595800524934, 33001.0498687664, 33268.503937007874, 33535.95800524934, 33803.41207349081, 34070.86614173228, 34338.320209973754, 34605.77427821522, 34873.22834645669, 35140.68241469816, 35408.136482939626, 35675.5905511811, 35943.044619422566, 36210.49868766404, 36477.952755905506, 36745.40682414698], 

	"mass_flmin": 69457.999305420002,

	"registration": "F-GZHA",

	"mach_cruise": 0.79
}



Exemples de données de sortie :

Cas 1 :

{
	"steps_opt":[
					{"alt_begin": 10000, "alt_end": 11500, "ias": 226.69140436104769}, 
					{"alt_begin": 11500, "alt_end": 13000, "ias": 236.7642321041416}, 
					{"alt_begin": 13000, "alt_end": 36700, "mach": 0.64852782025687983}
				], 
	"rmse_consumption": 1.1635985275415189e-07
}


Cas 2 :

{
	"steps_opt":[
					{"alt_begin": 10000, "alt_end": 13000, "ias": 231.72781823259467}, 
					{"alt_begin": 13000, "alt_end": 31000, "ias": 269.59808169063217}, 
					{"alt_begin": 31000, "alt_end": 32844, "ias": 283.27443432212527}, 
					{"alt_begin": 32844, "alt_end": 36700, "mach": 0.79}
				], 
	"rmse_consumption": 5.6750829411750918e-07
}

