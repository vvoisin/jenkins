# !/usr/bin/env python
#  -*- coding: utf-8 -*-

import json, sys, os, traceback, pp, pandas, numpy

class Preprocessing():
	""" Flight data extractor """
	
	def __init__(self, dict_names, data_path_in, data_path_out):
		self.dict_names = dict_names
		self.data_path_in = data_path_in
		self.data_path_out = data_path_out


	def run_one(self, file_name):

		try:
			tmpDat = pandas.read_csv(file_name, delimiter=',')
			### CHANGE COLUMNS
			tmpDat = tmpDat.rename(columns = self.dict_names)
			
			### CHANGE UNITS

			# GW: kg --> lbs (pounds)
			GW = tmpDat['GW'] * 2.2
			tmpDat['GW'] = pandas.Series(GW)

			# FF: kg/h --> pounds per hour
			FF1 = tmpDat['FF1'] * 2.2
			tmpDat['FF1'] = pandas.Series(FF1)
			FF2 = tmpDat['FF2'] * 2.2
			tmpDat['FF2'] = pandas.Series(FF2)


			## RECOMPUTE DATA
			# IAS
			h_m = tmpDat.ALTITUDE * .3048
			sat_K = tmpDat.SAT + 274.15
			mach = tmpDat.MACH
			ias = compute_ias(h_m, sat_K, mach)
			tmpDat['IAS'] = pandas.Series(ias)

			# GS
			tmpDat['GS'] = pandas.Series(ias)

			# FROM / TO
			FROM = tmpDat.FROM
			FROM = FROM[~FROM.isnull()]
			tmpDat.FROM = FROM.iloc[0]
			TO = tmpDat.TO
			TO = TO[~TO.isnull()]
			tmpDat.TO = TO.iloc[0]

			# REGIST
			fname_split = file_name.split('_')
			REGIST = fname_split[-1]
			REGIST = REGIST.split('.')[0]
			tmpDat['REGIST'] = [REGIST] * len(tmpDat)
			
			ID = fname_split[-2].split('/')
			ID = ID[-1]

			# WRITE CSV
			year = '0000'
			mounth = '00'
			day = '00'
			hour = '00'
			minutes = ID
			new_file_name = REGIST+'-'+year+'-'+mounth+'-'+day+'-'+hour+'-'+minutes+'.csv'
			tmpDat.to_csv(self.data_path_out+new_file_name, index=False)

			return 1

		except IOError as err:
			print fname
			print err
			return 0

	def run(self, ncpus=-1):

		print 'Warning: for DLH only'

		folder = self.data_path_in
		Files = numpy.array(os.listdir(folder))

		if ncpus in [0,1]:
			print 'Sequential data extraction'
			tmp = [self.run_one(folder + f) for f in Files]

		elif ncpus == -1:
			
			job_server = pp.Server(ppservers=())
			ncp = job_server.get_ncpus()
			job_server.set_ncpus(ncp)
			modules = ('pandas', 'numpy')
			depfuncs = (compute_ias,)
			
			print 'Parallel data extraction with ncpus = ' + str(ncp)
			jobs = [job_server.submit(func=self.run_one, args=(folder + f,), modules=modules, depfuncs=depfuncs) for f in Files]
			tmp = [job() for job in jobs]

		else:

			job_server = pp.Server(ppservers=())
			job_server.set_ncpus(ncpus)
			modules = ('pandas',)
			depfuncs = ()
			
			print 'Parallel data extraction with ncpus = ' + str(ncpus)
			jobs = [job_server.submit(func=self.run_one, args=(folder + f,), modules=modules, depfuncs=depfuncs) for f in Files]
			tmp = [job() for job in jobs]

		n_keep = sum(tmp)
		print str(n_keep) + ' files kept over ' + str(len(tmp))





def compute_ias(h_m, sat_K, mach):
	p0 = 101325														# Pa
	T0 =  288.15													# K
	g  = 9.80665													# m/s^2
	Rs = 287.053													# J/(kg K)
	alf = -0.0065													# K/m
	P_Pa_SI = p0 * ( ( 1 + alf/T0 * h_m )**( -g / (alf * Rs) ) )

	# VITESSE DU SON____________m/s
	lam = 1.4;
	vson_ms = numpy.sqrt( lam * Rs * sat_K )						# en m/s
	TAS_ms = mach * vson_ms;

	#relation TAS - CAS, eq 10, docu Cindie
	a0_ms = 661.4787 * 0.514444										# m/s
	rap = (lam-1) / lam
	irap=1/rap
	C1 = TAS_ms**2 / ( 5 * vson_ms**2)  + 1
	C2  = C1**irap - 1
	C3 = P_Pa_SI / p0 * C2 + 1

	IAS_ms = numpy.sqrt(5) * a0_ms * numpy.sqrt( C3**rap - 1 )

	return IAS_ms / .514444
