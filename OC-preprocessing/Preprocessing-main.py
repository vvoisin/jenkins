# !/usr/bin/env python
#  -*- coding: utf-8 -*-


"""
	
	PREPROCESSING MODULE -- PREPROCESS EACH FILE IN input_folder: CHANGE THE NAMES ACCORDING TO THE MATCHING GIVEN BY AN EXTERNAL JSON FILE, 
	ADD NEW PARAMETERS AND WRITE INTO output_folder


	DEPENDENCIES:
		pp (PARALLEL PYTHON)
		pandas
		numpy


	USAGE:
		
		python Preprocessing-main.py [input_folder] [output_folder]


	EXAMPLE OF JSON FILE names.json:
		
		{	"PRESSURE ALTITUDE 1 (ft)":"ALTITUDE",	
			"Outside Air Temperature* (deg C)":"OAT"
		}

	EXAMPLE OF JSON FILE names_recompute.json:
		
		{	"IAS":["ALTITUDE", MACH, SAT],	
			...
		}

"""



import sys, json
import numpy as np
from sys import exit
from Preprocessing import *


try:
	input_folder = sys.argv[1]
	output_folder = sys.argv[2]
except:
	print 'Usage: python Preprocessing-main.py [input_folder] [output_folder]'
	exit(0)


with open('names.json') as f:
	dict_names = json.load(f)


if input_folder[-1] != '/':
	print 'Correcting input_folder'
	input_folder = input_folder + '/'
if output_folder[-1] != '/':
	print 'Correcting output_folder'
	output_folder = output_folder + '/'


obj = Preprocessing(dict_names, input_folder, output_folder)
obj.run()


