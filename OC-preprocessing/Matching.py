# !/usr/bin/env python
#  -*- coding: utf-8 -*-

import json, sys, os, traceback, pp, pandas, numpy

class Matching():
	""" Flight data extractor """
	
	def __init__(self, dict_names, data_path_in, data_path_out):
		self.dict_names = dict_names
		self.data_path_in = data_path_in
		self.data_path_out = data_path_out


	def run_one(self, file_name):

		try:
			tmpDat = pandas.read_csv(file_name, delimiter=',')
			tmpDat = tmpDat.rename(columns = self.dict_names)
			fname = file_name.split('/')[-1]
			tmpDat.to_csv(self.data_path_out+fname)

			return 1

		except IOError as err:
			print fname
			print err
			return 0

	def run(self, ncpus=-1):

		folder = self.data_path_in
		Files = numpy.array(os.listdir(folder))

		if ncpus in [0,1]:
			print 'Sequential data extraction'
			tmp = [self.run_one(folder + f) for f in Files]

		elif ncpus == -1:
			
			job_server = pp.Server(ppservers=())
			ncp = job_server.get_ncpus()
			job_server.set_ncpus(ncp)
			modules = ('pandas',)
			depfuncs = ()
			
			print 'Parallel data extraction with ncpus = ' + str(ncp)
			jobs = [job_server.submit(func=self.run_one, args=(folder + f,), modules=modules, depfuncs=depfuncs) for f in Files]
			tmp = [job() for job in jobs]

		else:

			job_server = pp.Server(ppservers=())
			job_server.set_ncpus(ncpus)
			modules = ('pandas',)
			depfuncs = ()
			
			print 'Parallel data extraction with ncpus = ' + str(ncpus)
			jobs = [job_server.submit(func=self.run_one, args=(folder + f,), modules=modules, depfuncs=depfuncs) for f in Files]
			tmp = [job() for job in jobs]

		n_keep = sum(tmp)
		print str(n_keep) + ' files kept over ' + str(len(tmp))


