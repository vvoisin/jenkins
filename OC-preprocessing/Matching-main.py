# !/usr/bin/env python
#  -*- coding: utf-8 -*-


"""
	
	NAME MATCHING MODULE -- CHANGE THE NAMES OF EACH FILE IN input_folder ACCORDING TO THE MATCHING GIVEN BY AN EXTERNAL JSON FILE 
	AND WRITE INTO output_folder


	DEPENDENCIES:
		pp (PARALLEL PYTHON)
		pandas
		numpy


	USAGE:
		
		python Matching-main.py [input_folder] [output_folder]


	EXAMPLE OF JSON FILE (ONLY THE COLUMNS WE WANT):
		
		{	"PRESSURE ALTITUDE 1 (ft)":"ALTITUDE",	
			"Outside Air Temperature* (deg C)":"OAT"
		}

"""



import sys, json
import numpy as np
from sys import exit
from Matching import *

try:
	input_folder = sys.argv[1]
	output_folder = sys.argv[2]
except:
	print 'Usage: python Matching-main.py [input_folder] [output_folder]'


with open('names.json') as f:
	dict_names = json.load(f)


if input_folder[-1] != '/':
	print 'Correcting input_folder'
	input_folder = input_folder + '/'
if output_folder[-1] != '/':
	print 'Correcting output_folder'
	output_folder = output_folder + '/'


obj = Matching(dict_names, input_folder, output_folder)
obj.run()





# A VIRER

# import pandas as pd, matplotlib.pyplot as plt

# # NOK
# a = pd.read_csv('101_DAIZG.csv')
# plt.subplot(211)
# plt.plot(a['ALTITUDE'], '-')
# plt.subplot(212)
# plt.plot(a['LEVEL OFF'], '-')
# plt.show()


# a = pd.read_csv('102_DAIZG.csv')
# plt.subplot(211)
# plt.plot(a['ALTITUDE'], '-')
# plt.subplot(212)
# plt.plot(a['LEVEL OFF'], '-')
# plt.show()






# # OK
# a = pd.read_csv('100_DAIZG.csv')
# plt.subplot(211)
# plt.plot(a['ALTITUDE'], '-')
# plt.subplot(212)
# plt.plot(a['LEVEL OFF'], '-')
# plt.show()


# # OK
# a = pd.read_csv('183_DAIZG.csv')
# plt.subplot(211)
# plt.plot(a['ALTITUDE'], '-')
# plt.subplot(212)
# plt.plot(a['LEVEL OFF'], '-')
# plt.show()


