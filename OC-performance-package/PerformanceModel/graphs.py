import sys, csv, math, os, traceback
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

import time as ti
from datetime import *

from Linear_smoother import *

def convert(t):
	t_format = datetime(2015, int(t[0:2]), int(t[2:4]), int(t[4:6]), 0, 0, 0)
	return ti.mktime(t_format.timetuple())

def adjust_spines(ax, spines):
	for loc, spine in ax.spines.items():
		if loc in spines:
			spine.set_position(('outward', 10))  # outward by 10 points
			spine.set_smart_bounds(True)
		else:
			spine.set_color('none')  # don't draw spine

	# turn off ticks where there is no spine
	if 'left' in spines:
		ax.yaxis.set_ticks_position('left')
	else:
		# no yaxis ticks
		ax.yaxis.set_ticks([])

	if 'bottom' in spines:
		ax.xaxis.set_ticks_position('bottom')
	else:
		# no xaxis ticks
		ax.xaxis.set_ticks([])





def graph_vs_time(list_perfo_data, cols):
	""" From a list of performances data files, plot (Consumption - Performance) / Performance 
		over time and over travelled distance
	"""
	
	fig = plt.figure(figsize=(12, 6))
	# ax = fig.add_subplot(111)
	ymin, ymax, xmax = float('inf'), 0, 0
	smooth_objects = []
	for i,f in enumerate(list_perfo_data):
		results = pd.read_csv(f, sep=',', header=False)

		# ADD TIME AND REGISTRATION
		Time = [z[7:14] for z in results.file]
			
		Time_sec = np.array([convert(t) for t in Time], dtype=int)
		results['Regist'] = pd.Series([z[0:6] for z in results.file])
		if i == 0:
			Regist = results['Regist']
		results['Time'] = pd.Series(Time_sec)

		Time_low = convert('0601000') #FROM JUNE 2015, 0:00
		results = results[results['Time'] >= Time_low]
		
		Time_high = convert('1101000') #TO NOV 2015, 0:00
		results = results[results['Time'] < Time_high]


		x = 100 * (results.conso - results.perfo) / results.perfo

		ecart = np.mean(np.array(results.conso) - np.array(results.perfo))
		ecart_std = np.std(np.array(results.conso) - np.array(results.perfo))
		# print '\t' + f 
		# print '\t\t' + str(len(x)) + ' flights'
		# print '\t\tMean savings = ' + str(round(ecart, 2)) + ' kg (std = ' + str(round(ecart_std, 2)) + ' kg)'
		# print '\t\tMean savings in % = ' + str(round(np.mean(x), 2)) + ' % (std = ' + str(round(np.std(x), 2)) + ' %)'

		print '\t' + f + ' ' + str(len(x)) + ' ' + str(round(ecart, 2)) + ' ' + str(round(ecart_std, 2)) + ' ' + str(round(np.mean(x), 2)) + ' ' + str(round(np.std(x), 2))

		# EXPORT GRAPHS
		
		if i==0:
			al = .5
		else:
			al = 1
		# ax.plot(results.Time, x, 'o', c=cols[i], clip_on=False, alpha=al)
		# adjust_spines(ax, ['left', 'bottom'])

		plt.plot(results.Time, x, 'o', c=cols[i], alpha=al)
		
		# xmin = min(xmin, min(results.Time))
		# xmax = max(xmax, max(results.Time))

		Smoother = Linear_smoother(results.Time, x, gamma=1, h=604800)
		Smoother.fit(np.linspace(min(results.Time), max(results.Time),100))
		smooth_objects.append(Smoother)


		ymin = min(ymin, min(x))
		ymax = max(ymax, max(x))
		xmax = max(xmax, max(results.Time))

	plt.plot([Time_low,min(xmax, Time_high)], [0,0], 'k-', linewidth=2.0)
	plt.ylim(ymin = ymin, ymax = ymax)
	plt.xlabel('Time in seconds from 2015-06-01 to 2015-11-01')
	plt.ylabel('(Consommation - Performance) / Performance')

	Regist = Regist.unique()
	Regist.sort()

	return smooth_objects, ymin, ymax, xmax, Regist


def graph_vs_time_regist(list_perfo_data, r, cols):
	""" From a list of performances data files, plot (Consumption - Performance) / Performance 
		over time and over travelled distance
	"""
	
	fig = plt.figure(figsize=(12, 6))
	ymin, ymax, xmax = float('inf'), 0, 0
	smooth_objects = []
	for i,f in enumerate(list_perfo_data):
		try:
			results = pd.read_csv(f, sep=',', header=False)

			# ADD TIME AND REGISTRATION
			Time = [z[7:14] for z in results.file]
				
			Time_sec = np.array([convert(t) for t in Time], dtype=int)
			results['Regist'] = pd.Series([z[0:6] for z in results.file])
			results['Time'] = pd.Series(Time_sec)

			results = results[results.Regist == r]


			Time_low = convert('0601000') #FROM JUNE 2015, 0:00
			results = results[results['Time'] >= Time_low]
			
			Time_high = convert('1101000') #TO NOV 2015, 0:00
			results = results[results['Time'] < Time_high]

			if results.shape[0] == 0:
				raise ValueError

			x = 100 * (results.conso - results.perfo) / results.perfo

			ecart = np.mean(np.array(results.conso) - np.array(results.perfo))
			ecart_std = np.std(np.array(results.conso) - np.array(results.perfo))
			# print '\t' + f 
			# print '\t\t' + str(len(x)) + ' flights'
			# print '\t\tMean savings = ' + str(round(ecart, 2)) + ' kg (std = ' + str(round(ecart_std, 2)) + ' kg)'
			# print '\t\tMean savings in % = ' + str(round(np.mean(x), 2)) + ' % (std = ' + str(round(np.std(x), 2)) + ' %)'

			print '\t' + f + ' ' + str(len(x)) + ' ' + str(round(ecart, 2)) + ' ' + str(round(ecart_std, 2)) + ' ' + str(round(np.mean(x), 2)) + ' ' + str(round(np.std(x), 2))

			# EXPORT GRAPHS
			
			if i==0:
				al = .5
			else:
				al = 1

			plt.plot(results.Time, x, 'o', c=cols[i], alpha=al)
		
			Smoother = Linear_smoother(results.Time, x, gamma=1, h=604800)
			Smoother.fit(np.linspace(min(results.Time), max(results.Time),100))
			smooth_objects.append(Smoother)
	
			ymin = min(ymin, min(x))
			ymax = max(ymax, max(x))
			xmax = max(xmax, max(results.Time))
		except ValueError:
			print 'Next!'



	plt.plot([Time_low,min(xmax, Time_high)], [0,0], 'k-', linewidth=2.0)
	plt.ylim(ymin = ymin, ymax = ymax)
	plt.xlabel('Time in seconds from 2015-06-01 to 2015-11-01')
	plt.ylabel('(Consommation - Performance) / Performance')
	plt.title(r)

	return smooth_objects, ymin, ymax, xmax


def graph_distance_vs_time(list_perfo_data, cols):
	""" From a list of performances data files, plot (Consumption - Performance) / Performance 
		over time and over travelled distance
	"""
	
	fig = plt.figure(figsize=(12, 6))
	ymin, ymax = float('inf'),0
	smooth_objects = []
	for i,f in enumerate(list_perfo_data):
		results = pd.read_csv(f, sep=',', header=False)
		

		# ADD TIME AND REGISTRATION
		Time = [z[7:14] for z in results.file]
			
		Time_sec = np.array([convert(t) for t in Time], dtype=int)
		results['Regist'] = pd.Series([z[0:6] for z in results.file])
		results['Time'] = pd.Series(Time_sec)

		if i == 0:
			Regist = results['Regist']

		Time_low = convert('0601000') #FROM JUNE 2015, 0:00
		results = results[results['Time'] >= Time_low]
		
		Time_high = convert('1101000') #TO NOV 2015, 0:00
		results = results[results['Time'] < Time_high]


		# STATS
		# print '\t' + f
		# print '\t\t' + str(len(Time)) + ' flights'
		# print '\tMean travelled distance = ' + str(round(np.mean(results.distance), 2)) + ' km (std = ' + str(round(np.std(results.distance), 2)) + ' km)'

		print '\t' + f + ' ' + str(len(Time)) + ' ' + str(round(np.mean(results.distance), 2)) + ' ' + str(round(np.std(results.distance), 2))


		# EXPORT GRAPHS
		if i==0:
			al = .5
		else:
			al = 1

		plt.plot(results.Time, results.distance, 'o', c=cols[i], alpha=al)

		Smoother = Linear_smoother(results.Time, results.distance, gamma=1, h=604800)
		Smoother.fit(np.linspace(min(results.Time), max(results.Time),100))
		smooth_objects.append(Smoother)

		ymin = min(ymin, min(results.distance))
		ymax = max(ymax, max(results.distance))

	plt.ylim(ymin = ymin, ymax = ymax)
	plt.ylabel('Travelled distance in km')
	plt.xlabel('Time in seconds from 2015-06-01 to 2015-11-01')

	Regist = Regist.unique()
	Regist.sort()

	return smooth_objects, ymin, ymax, Regist


def graph_distance_vs_time_regist(list_perfo_data, r, cols):
	""" From a list of performances data files, plot (Consumption - Performance) / Performance 
		over time and over travelled distance
	"""
	
	fig = plt.figure(figsize=(12, 6))
	ymin, ymax = float('inf'),0
	smooth_objects = []
	for i,f in enumerate(list_perfo_data):
		try:
			results = pd.read_csv(f, sep=',', header=False)

			# ADD TIME AND REGISTRATION
			Time = [z[7:14] for z in results.file]
				
			Time_sec = np.array([convert(t) for t in Time], dtype=int)
			results['Regist'] = pd.Series([z[0:6] for z in results.file])
			results['Time'] = pd.Series(Time_sec)

			results = results[results.Regist == r]

			Time_low = convert('0601000') #FROM JUNE 2015, 0:00
			results = results[results['Time'] >= Time_low]
			
			Time_high = convert('1101000') #TO NOV 2015, 0:00
			results = results[results['Time'] < Time_high]

			if results.shape[0] == 0:
				raise ValueError

			# STATS
			# print '\t' + f
			# print '\t\t' + str(len(results.distance)) + ' flights'
			# print '\tMean travelled distance = ' + str(round(np.mean(results.distance), 2)) + ' km (std = ' + str(round(np.std(results.distance), 2)) + ' km)'

			print '\t' + f + ' ' + str(len(results.distance)) + ' ' + str(round(np.mean(results.distance), 2)) + ' ' + str(round(np.std(results.distance), 2))


			# EXPORT GRAPHS
			if i==0:
				al = .5
			else:
				al = 1

			plt.plot(results.Time, results.distance, 'o', c=cols[i], alpha=al)

			Smoother = Linear_smoother(results.Time, results.distance, gamma=1, h=604800)
			Smoother.fit(np.linspace(min(results.Time), max(results.Time),100))
			smooth_objects.append(Smoother)

			ymin = min(ymin, min(results.distance))
			ymax = max(ymax, max(results.distance))
		except ValueError:
			print 'Next'

	plt.ylim(ymin = ymin, ymax = ymax)
	plt.title(r)
	plt.ylabel('Travelled distance in km')
	plt.xlabel('Time in seconds from 2015-06-01 to 2015-11-01')

	return smooth_objects, ymin, ymax

