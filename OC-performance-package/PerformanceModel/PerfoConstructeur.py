import sys, traceback, os, json
import numpy as np
import pandas as pd

import rpy2.robjects as R
from rpy2.rinterface import RRuntimeError

class PerfoConstructeur():
	def __init__(self, R_path, flight_phase):
		
		if flight_phase != 'climb' and flight_phase != 'takeoff':
			sys.exit('flight_phase must be "climb" or "takeoff"')

		self.flight_phase = flight_phase
		self.R_path = R_path
		self.performances = {}
		

	def run(self, FLmin, FLmax, GW, ISA, fuel_factor=0):

		if FLmax <= FLmin:
			sys.exit('FLmax must be larger than FLmin')

		# Reference consumption in kg
		command = "source('" + self.R_path + "interp3D_donnees_construct.R')"
		fct_cons = R.r(command)[0]

		if FLmin == 0: # From Take-off to FLmax
			cons_FLmin = 0
		else: # From FLmin to FLmax
			cons_FLmin = fct_cons(FLmin, GW, ISA)[0]
		cons_FLmax = fct_cons(FLmax, GW, ISA)[0]


		# Reference distance in km
		command = "source('" + self.R_path + "interp3D_donnees_construct_distance.R')"
		fct_dist = R.r(command)[0]

		if FLmin == 0: # From Take-off to FLmax
			dist_FLmin = 0
		else: # From FLmin to FLmax
			dist_FLmin = fct_dist(FLmin, GW, ISA)[0]
		dist_FLmax = fct_dist(FLmax, GW, ISA)[0]


		# Reference time in minutes
		command = "source('" + self.R_path + "interp3D_donnees_construct_time.R')"
		fct_time = R.r(command)[0]

		if FLmin == 0: # From Take-off to FLmax
			time_FLmin = 0
		else: # From FLmin to FLmax
			time_FLmin = fct_time(FLmin, GW, ISA)[0]
		time_FLmax = fct_time(FLmax, GW, ISA)[0]




		perfo = {	'FLmin':FLmin, 'FLmax':FLmax, 'GW':GW, 'ISA':ISA, 
					'consumption_ref':(cons_FLmax - cons_FLmin) * (1 + fuel_factor / 100), 
					'distance_ref':(dist_FLmax - dist_FLmin) * 1.852, # 1 NM = 1.852 km
					'time_ref':time_FLmax - time_FLmin}
		return perfo


	def compute_deltaISA(self, ALT, MACH, TAT):

		gamma = 1.4
		idxFL200 = np.where(ALT >= 20000)[0][0]
		TAT_FL200 = TAT[idxFL200]
		MACH_FL200 = MACH[idxFL200]

		SAT_FL200 = (TAT_FL200 + 274.15) / (1 + MACH_FL200**2 * (gamma - 1) / 2)
		SAT_STD_FL200 = 288.15 - 6.5e-3 * (ALT[idxFL200] * .3048)
		ISA = SAT_FL200 - SAT_STD_FL200
		return ISA


	def get_consumption(self, idxFLmin, idxFLmax, LEVEL_OFF, FF1, FF2):
		''' Get observed fuel consumption from FLmin to FLmax '''
		
		# Fuel Flow in kg/h
		tmpFF1 = FF1[idxFLmin:idxFLmax+1] / 2.2 
		tmpFF2 = FF2[idxFLmin:idxFLmax+1] / 2.2

		if self.flight_phase == 'climb':
			# REDUCE DATA FROM FLmin TO FLmax
			LEVEL_OFF = np.array(LEVEL_OFF[idxFLmin:idxFLmax+1], dtype=float)
			# DELETE LEVEL-OFF
			without_LO = np.where(LEVEL_OFF == 0)[0]
			tmpFF1 = tmpFF1[without_LO]
			tmpFF2 = tmpFF2[without_LO]

		if len(tmpFF1) == 0 or len(tmpFF2) == 0:
			raise ValueError('FF1 or FF2 is of size 0')

		# Fuel Flow in kg/s
		FF = (tmpFF1 + tmpFF2) / 3600

		return np.cumsum(FF)[len(FF)-1] - FF[0]



	def get_distance(self, idxFLmin, idxFLmax, LEVEL_OFF, GS):
		''' Get observed travelled distance from FLmin to FLmax '''

		tmpGS = GS[idxFLmin:idxFLmax+1]

		if self.flight_phase == 'climb':
			# REDUCE DATA FROM FLmin TO FLmax
			LEVEL_OFF = np.array(LEVEL_OFF[idxFLmin:idxFLmax+1], dtype=float)
			# DELETE LEVEL-OFF
			without_LO = np.where(LEVEL_OFF == 0)[0]
			tmpGS = tmpGS[without_LO]

		#GS in m/s
		tmpGS = [z * .514 for z in tmpGS]
		return np.sum(tmpGS) / 1000


	def get_time(self, idxFLmin, idxFLmax, LEVEL_OFF):
		''' Get observed time from FLmin to FLmax '''

		time = idxFLmax - idxFLmin + 1

		if self.flight_phase == 'climb':
			# OVERAL TIME - LEVEL-OFF TIME
			LEVEL_OFF = np.array(LEVEL_OFF[idxFLmin:idxFLmax+1], dtype=float)
			time_LO = np.sum(LEVEL_OFF == 2)
		else:
			time_LO = 0

		return (time - time_LO) / 60.




	def run_files(self, Files, folder, fuel_factor_df=None):
		
		for j,f in enumerate(Files):

			if j % 100 == 0:
				print str(j) + ' files processed ' + str(round(float(j) / len(Files) * 100, 2)) + '%'

			try:
				dat = pd.read_csv(folder + f, delimiter=';')
				N = len(dat.index)
				dat = dat.iloc[1:N]
				idx_null = np.where(dat['LEVEL OFF'] == 'null')[0][0]
				dat = dat.iloc[0:idx_null]

				GW = np.array(dat.GW[0:5], dtype=float)[0] / (1000 * 2.2)
				Alt = np.array(dat.ALTITUDE, dtype=float)
				FF1 = np.array(dat.FF1, dtype=float)
				FF2 = np.array(dat.FF2, dtype=float)
				GS = np.array(dat.GS, dtype=float)
				MACH = np.array(dat.MACH, dtype=float)
				TAT = np.array(dat.TAT, dtype=float)

				LEVEL_OFF = np.array(dat['LEVEL OFF'], dtype=float)
				if 3 not in LEVEL_OFF:
					raise IndexError('Level-off not detected')
	

				# COMPUTE DELTA ISA FROM TAT
				ISA = self.compute_deltaISA(Alt, MACH, TAT)


				# GET FUEL FACTOR
				if fuel_factor_df is not None:
					Regist = f[0:6]
					ffactor = float(fuel_factor_df.FuelFactor[fuel_factor_df.REGIST == Regist])
					if np.isnan(ffactor):
						raise ValueError('Fuel factor = NaN')
				else:
					ffactor = 0


				# GET FLmin AND FLmax
				if self.flight_phase == 'takeoff':
					# FLmin
					FLmin = 0
					idxFLmin = np.where(LEVEL_OFF == 0.0)[0][0] # The first indice for climb
					# FLmax
					FLmax = 100
					idxFLmax = np.where(Alt >= 10000)[0][0]

				if self.flight_phase == 'climb':
					# FLmin
					FLmin = 100
					idxFLmin = np.where(Alt >= 10000)[0][0]
					# FLmax
					idxFLmax = np.where(LEVEL_OFF == 3.0)[0][0] # The first indice for cruise
					FLmax = int(Alt[idxFLmax] / 100)
					if FLmax < 250 or FLmax > 400:
						raise ValueError('ToC must be between 250 and 400')


				# COMPUTE REFERENCE CONSUMPTIONS, DISTANCES AND TIME
				perfo = self.run(FLmin, FLmax, GW, ISA, fuel_factor=ffactor)


				# COMPUTE FUEL CONSUMPTION IN kg
				consumption = self.get_consumption(idxFLmin, idxFLmax, LEVEL_OFF, FF1, FF2)


				# COMPUTE TRAVELLED DISTANCE IN km
				distance = self.get_distance(idxFLmin, idxFLmax, LEVEL_OFF, GS)


				# COMPUTE TIME IN MINUTES
				time = self.get_time(idxFLmin, idxFLmax, LEVEL_OFF)
				

				perfo['consumption'] = consumption
				perfo['distance'] = distance
				perfo['time'] = time
				self.performances[f] = perfo


			except (ValueError, IndexError, RRuntimeError) as err:
				print '\nFail to process ' + folder+f
				print err
				print str(j) + ' files processed ' + str(round(float(j) / len(Files) * 100, 2)) + '%'


