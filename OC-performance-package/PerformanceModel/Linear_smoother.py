import numpy
from sklearn.metrics.pairwise import rbf_kernel
import sys, pp

class Linear_smoother():
	""" Nadaraya-Watson estimator for non parametric model y_i = f(x_i) + e_i """
	
	def __init__(self, x, y, gamma, h, ncpus=-1):
		self.x = numpy.array(x, dtype=float) #Time
		self.y = numpy.array(y, dtype=float) #values
		self.gamma = gamma
		self.h = h
		self.ncpus = ncpus

		self.grid_x = None
		self.fitted_values = None
	
	def weights(self, newx):
		eval_points = numpy.array([Kh(newx, x_i, self.h) for x_i in self.x])
		return eval_points / numpy.sum(eval_points)

	# def fit_point(self, newx):
	# 	if type(newx) in [float, int, 'numpy.float64']:
	# 		W = self.weights(newx)
	# 		return numpy.sum(W * self.y)
	# 	else:
	# 		print newx, type(newx)
	# 		sys.exit('Oups something wrong is happening!')


	def fit_point(self, newx):
		W = self.weights(float(newx))
		return numpy.sum(W * self.y)

	def fit(self, grid_x):
		if self.ncpus == 0:
			self.fitted_values = [self.fit_point(float(z)) for z in grid_x]
		elif self.ncpus == -1:
			job_server = pp.Server(ppservers=())
			ncpus = job_server.get_ncpus()
			job_server.set_ncpus(ncpus)
			modules = ('numpy', 'sklearn.metrics.pairwise')
			depfuncs = (Kh,)
			jobs = [job_server.submit(func=self.fit_point, args=(z,), modules=modules, depfuncs=depfuncs) for z in grid_x]
			self.fitted_values = numpy.array([job() for job in jobs])
		else:
			job_server = pp.Server(ppservers=())
			job_server.set_ncpus(self.ncpus)
			modules = ('numpy', 'sklearn.metrics.pairwise')
			depfuncs = (Kh,)
			jobs = [job_server.submit(func=self.fit_point, args=(z,), modules=modules, depfuncs=depfuncs) for z in grid_x]
			self.fitted_values = numpy.array([job() for job in jobs])
		self.grid_x = grid_x
		if self.ncpus != 0:
			job_server.destroy()


# Gaussian kernel
def Kh(x, y, h):
	return float(rbf_kernel(x / h, y / h) / h)





