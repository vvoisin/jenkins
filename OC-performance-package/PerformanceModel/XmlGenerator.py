import sys, traceback, os
import numpy as np
import rpy2.robjects as R

from lxml import etree


class XmlGenerator():
	""" Generate an XML file for Boeing performances"""


	# REFERENCES (min, max)
	ISA_ref = (0,20)
	GW_ref = (40,85)
	ToC_ref = (100, 400)


	def __init__(self, R_path, actype):

		sys.exit('XmlGenerator not up-to-date. Ref distances not computed.')

		self.R_path = R_path
		self.actype = actype
		self.etree_object = None
		
	ajouter ToCmin et ToCmax pour etre utilisé pour TO et Climb

	def R_call(self, ToC, GW, ISA):
		
		command = "source('" + self.R_path + "interp3D_donnees_construct.R')"

		fct = R.r(command)[0]

		try:
			perfo_FL100 = fct(100, GW, ISA)[0]
			perfo_ToC = fct(ToC, GW, ISA)[0]
		
		''' Ici récup un RunTimeError (de R) et retourner NA '''
		except:
			print 'Error in R call'
			return 'NA'

		return perfo_ToC - perfo_FL100


	def generate_xml(self):

		# ISA PAR PAS DE 1 DEGRE
		grid_isa = range(self.ISA_ref[0], self.ISA_ref[1] + 1)
		# GW PAR PAS DE 500 KG
		grid_gw = np.arange(self.GW_ref[0], self.GW_ref[1] + .5, .5)
		# TOC PAR PAS DE 1000 FT
		grid_toc = range(self.ToC_ref[0], self.ToC_ref[1] + 10, 10)

		# POUR TESTER
		# grid_isa = [0, 20]
		# grid_gw = [40, 82]
		# grid_toc = [180,400]

		aircraft = etree.Element('aircraft')
		aircraft.set('info', self.actype)
		
		for gw in grid_gw:
			print 'Gw\t' + str(gw)
			gw_node = etree.SubElement(aircraft, 'gw')
			gw_node.set('value', str(gw))
			
			for toc in grid_toc:
				toc_node = etree.SubElement(gw_node, 'toc')
				toc_node.set('value', str(toc))
		
				for isa in grid_isa:
					isa_node = etree.SubElement(toc_node, 'isa')
					isa_node.set('value', str(isa))
					isa_node.text = str(self.R_call(ToC=toc, GW=gw, ISA=isa))

		self.etree_object = aircraft


	def serialize(self, outfile):
		
		with open(outfile, 'w') as f:
			f.write('<?xml version="1.0" encoding="UTF-8" ?>\n')
			f.write(etree.tostring(self.etree_object, pretty_print=True))
		f.close()


