#!/usr/bin/env python
from distutils.core import setup
import sys, os


if '--v' in sys.argv:
	idx = sys.argv.index('--v')
	version = sys.argv[idx+1]
	sys.argv.remove('--v')
	sys.argv.remove(version)
	
	print '\n-----------------------------------------------------'
	print '--- Building OC-performance-package version ' + version + ' ---'
	print '-----------------------------------------------------\n\n'

	setup(
		name='PerformanceModel',
		version=version,
		author='SafetyLine',
		packages=['PerformanceModel'],
		license='LICENSE.txt',
		description='Python package for OptiClimb project -- Boeing Performance model',
		long_description=open('README.txt').read(),
		requires=['os', 'sys', 'numpy', 'time', 'matplotlib', 'warnings''csv', 'scipy', 'sklearn']
		)

	if 'clean' in sys.argv:
		s = 'PerformanceModel-' + version + '.tar.gz'
		command = 'rm dist/'+s
		print '\n' + command
		os.system(command)

else:
	print 'Error: version missing'

