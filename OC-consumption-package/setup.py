#!/usr/bin/env python
from setuptools import setup
import sys, os


if '--v' in sys.argv:
	idx = sys.argv.index('--v')
	version = sys.argv[idx+1]
	sys.argv.remove('--v')
	sys.argv.remove(version)
	
	print '\n-----------------------------------------------------'
	print '--- Building OC-consumption-package version ' + version + ' ---'
	print '-----------------------------------------------------\n\n'

	setup(
		name='ConsumptionModel',
		version=version,
		author='SafetyLine',
		packages=['ConsumptionModel', 'ConsumptionModel.Learning', 'ConsumptionModel.Preprocessing'],
		license='LICENSE.txt',
		description='Python package for OptiClimb project -- Consumption model',
		long_description=open('README.txt').read(),
		install_requires=['scikit-learn', 'numpy', 'scipy']
		)

	if 'clean' in sys.argv:
		s = 'ConsumptionModel-' + version + '.tar.gz'
		command = 'rm dist/'+s
		print '\n' + command
		os.system(command)

else:
	print 'Error: version missing'

