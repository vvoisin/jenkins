"""
FLIGHT DATA READER
		Read flight data and preprocessing

- Import flight data from csv files

- Compute flight informations

- Compute fuel consumption

- Preprocess time series

OUT: FlightInfos.csv, Preprocessed data (a csv file for each flight parameter)
"""


import sys, csv, math, os, traceback
import numpy as np
import pandas as pd
from scipy.interpolate import UnivariateSpline

from ConsumptionModel.Learning.utils import *





nr = 1000
control = False

# dataPath = sys.argv[1]
# dataPath = sys.argv[2]
# mounth = sys.argv[3]

dataPath = '/home/bapt/Documents/Flight data/TVF/extract/'
outPath = './data/'
Files_all = np.array(os.listdir(dataPath))
mounth_all = np.array([s[7:9] for s in Files_all])

# GET APRIL
idx = np.where(mounth_all == '04')[0]
Files04 = Files_all[idx]

# GET MAY
idx = np.where(mounth_all == '05')[0]
Files05 = Files_all[idx]


# STEP 1: extract files, get infos (ToC, Registration) and compute fuel consumption
DAT = {}
I = []
t_ToC = []
Consumption = []
NR_LO = []
cpt = 0
nr_flights = 0
for j,f in enumerate(Files):

	infile = dataPath + f
	
	if control: print 'Processing ' + infile
	if j % 100 == 0:
		print str(j) + ' files processed ' + str(round(float(j) / len(Files) * 100, 2)) + '%'

	try:
		tmpDat = pd.read_csv(dataPath + f, delimiter=';')

		N = len(tmpDat.index)
		tmpDat = tmpDat.iloc[1:N]

		# GET DATA
		LEVEL_OFF = tmpDat['LEVEL OFF']
		if 3 not in LEVEL_OFF:
			raise IndexError

		Alt = np.array(tmpDat.ALTITUDE, dtype='float')
		
		# GET ALTITUDE AND ToC
		idxFL100 = np.where(Alt >= 10000)[0][0]

		idxToC = np.where(LEVEL_OFF == '3.0')[0][0] # The first indice for cruise

		AltToC = Alt[idxToC]
		if AltToC <= 10000:
			sys.exit('AltToC <= 10000')

		LEVEL_OFF = np.array(LEVEL_OFF[idxFL100:idxToC+1], dtype=float)
		without_LO = np.where(LEVEL_OFF == 0)[0]


		# GET INFORMATIONS
		Regist = f[0:6]
		From = tmpDat.FROM.value_counts().axes[0][0]
		To = tmpDat.TO.value_counts().axes[0][0]
		GWFL100 = float(tmpDat.GW[idxFL100])
		Flt_number = tmpDat['FLT NUMBER'][idxFL100]


		# COMPUTE DELTA ISA FROM TAT
		idxFL200 = np.where(Alt >= 20000)[0][0]
		TAT_FL200 = tmpDat.TAT[idxFL200]
		MACH_FL200 = tmpDat.MACH[idxFL200]
		gamma = 1.4
		SAT_FL200 = (TAT_FL200 + 274.15) / (1 + MACH_FL200**2 * (gamma - 1) / 2)
		SAT_STD_FL200 = 288.15 - 6.5e-3 * Alt[idxFL200] * .3048
		deltaISA = SAT_FL200 - SAT_STD_FL200


		# COMPUTE FUEL CONSUMPTION
		tmpFF1 = np.array(tmpDat.FF1, dtype=float)[idxFL100:idxToC+1] / 2.2 # Fuel Flow in kg/h
		tmpFF1 = tmpFF1[without_LO]

		tmpFF2 = np.array(tmpDat.FF2, dtype=float)[idxFL100:idxToC+1] / 2.2 # Fuel Flow in kg/h
		tmpFF2 = tmpFF2[without_LO]

		if len(tmpFF1) == 0 or len(tmpFF2) == 0:
			sys.exit('Error FF')

		FF = (tmpFF1 + tmpFF2) / 3600 # Fuel Flow in kg/s
		cons = np.cumsum(FF)[len(FF)-1] - FF[0]


		# GET FUNCTIONAL VARIABLES
		varName = tmpDat.columns
		DataPPDict = {}
		for v in varName:
			
			if v not in ['TAT', 'FROM', 'TO', 'TIME', 'GW', 'FF1', 'FF2', 'WINDSHEAR', 'LEVEL OFF', 'FLT NUMBER', 'GMT HOURS', 'GMT MINUTES', 'GMT SECONDS']:
				tmp = np.array(tmpDat[v], dtype=float)[idxFL100:idxToC+1]
				tmp = tmp[without_LO]

				spl = UnivariateSpline(range(len(tmp)), tmp, s=0)
				tmps = spl(np.linspace(0, len(tmp)-1, nr))
				DataPPDict[v] = tmps.tolist()
		DAT[f] = {	'data':DataPPDict, 'AltToC':AltToC, 'Regist':Regist, 'From':From, 'To':To, 'GWFL100':GWFL100, 
					'Consumption':cons, 't_ToC':idxToC - idxFL100 + 1, 'Flt_number':Flt_number, 'File':f, 'deltaISA':deltaISA}

		nr_flights += 1


	except SystemExit:
		cpt += 1
		print '\n\nFail to process ' + dataPath+f
		print '\tWhy ? ' + str(sys.exc_info()[1])

		traceback.print_exc()
		if cpt > 20:
			sys.exit('Too much errors. Stopping process...')

	except IndexError:
		print '\n\nWarning: cruise not detected.'

	except:
		print '\n\nFail to process ' + dataPath+f
		print '\tWhy ? ' + str(sys.exc_info()[1])


print str(nr_flights) + ' flihts processed over ' + str(len(Files))


# STEP 2: write a csv file for each variable
print '\n\n Ending...'
for v in varName:
	if v not in ['TAT', 'FROM', 'TO', 'TIME', 'GW', 'FF1', 'FF2', 'WINDSHEAR', 'LEVEL OFF', 'FLT NUMBER', 'GMT HOURS', 'GMT MINUTES', 'GMT SECONDS']:
		tmp = []
		for z in sorted(DAT.keys()):
			tmp.append([z] + DAT[z]['data'][v])

		tmp = np.asarray(tmp).T.tolist()
		outFile = outPath + 'Pre-processed_data/' + v + '.csv'
		print 'Writing ' + outFile
		with open(outFile, 'w') as csvFile:
			writer = csv.writer(csvFile, delimiter=',')
			writer.writerows(tmp)


# STEP 3: Flight infos
print 'Writing ' + outPath + 'FlightInfos.csv'

A = [DAT[z]['AltToC'] for z in sorted(DAT.keys())]
R = [DAT[z]['Regist'] for z in sorted(DAT.keys())]
F = [DAT[z]['From'] for z in sorted(DAT.keys())]
T = [DAT[z]['To'] for z in sorted(DAT.keys())]
G = [DAT[z]['GWFL100'] for z in sorted(DAT.keys())]
CONS = [DAT[z]['Consumption'] for z in sorted(DAT.keys())]
t_ToC = [DAT[z]['t_ToC'] for z in sorted(DAT.keys())]
Flt_number = [DAT[z]['Flt_number'] for z in sorted(DAT.keys())]
File = [DAT[z]['File'] for z in sorted(DAT.keys())]
deltaISA = [DAT[z]['deltaISA'] for z in sorted(DAT.keys())]

FlightInfos = pd.DataFrame({'File':File, 'deltaISA':deltaISA, 'AltToC':A, 'Regist':R, 'From':F, 'To':T, 'GWFL100':G, 'Consumption':CONS, 't_ToC':t_ToC, 'Flt_number':Flt_number})
FlightInfos.to_csv(outPath + 'FlightInfos.csv', delimiter=',', index=False)



