# !/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
FEATURE EXTRACTION
	Extract feature from preprocessed data

- Import preprocessed data

- FPCA for time series

- Other features

OUT: Design matrix and outcome (fuel consumption from FL100 to ToC)
"""

import numpy as np
import pandas as pd
import json, os
from sklearn.cluster import KMeans
from sklearn import preprocessing

from ConsumptionModel.Learning.utils import *
from ConsumptionModel.Learning.FeatureSelection import *
from ConsumptionModel.Preprocessing.Fpca import *




# DONNÉES DE MAI 2015


# PARAMETRES IMPROTANTS
# nr de points dans chaque séries (N=1000)
# 

print 'Feature extraction'

dataPath = 'data/Pre-processed_data/'
infosPath = 'data/'


print 'STEP 1: FLIGHT INFORMATIONS'
Infos = pd.read_csv(infosPath+'FlightInfos.csv', delimiter=',')
n = Infos.shape[0]

Regist_dummy = dummy(Infos.Regist)
From_dummy = dummy(Infos.From)
# To_dummy = dummy(Infos.To) AU LIEU DE METTRE TO, METTRE LA DISTANCE ENTRE LES DEUX
# Flt_number_dummy = dummy(Infos.Flt_number)

Gw = np.array(Infos.GWFL100.tolist()).reshape((n,1))
AltToC = np.array(Infos.AltToC.tolist()).reshape((n,1))
t_ToC = np.array(Infos.t_ToC.tolist()).reshape((n,1))
deltaISA = np.array(Infos.deltaISA.tolist()).reshape((n,1))

Other = np.concatenate((Regist_dummy[0], From_dummy[0], Gw, AltToC, t_ToC, deltaISA), axis=1) #, Flt_number_dummy[0], To_dummy[0]
Other = preprocessing.scale(Other)

NamesOther = list(Regist_dummy[1]) + list(From_dummy[1]) + ['Gw', 'AltToC', 't_ToC', 'deltaISA'] # + list(Flt_number_dummy[1]) + list(To_dummy[1])


plt.plot(Infos.Consumption, deltaISA, '.')
plt.xlabel('Consommation')
plt.ylabel('Delta ISA')
plt.show()



# IMPORT DES VARIABLES ET FPCA. C'EST JUSTE DU NUMÉRIQUE
print 'STEP 2: FUNCTIONAL PCA'
FilesVars = os.listdir(dataPath)
DesignMatrix = Other
List_Fpca = []
nr_PCs = []
for f in FilesVars:
	infile = dataPath+f
	print '\tProcessing ' + f
	tmpDat = np.loadtxt(infile, delimiter=',', skiprows=1).T
	
	if f == 'ALTITUDE.csv':
		Alt = normalize(tmpDat)
	if f == 'CAS.csv':
		Cas = normalize(tmpDat)


	fpca = FPCA(normalize(tmpDat), .999, 64)
	fpca.fit()
	DesignMatrix = np.concatenate((DesignMatrix, fpca.PCs), axis = 1)
	List_Fpca.append(fpca)
	nr_PCs.append(fpca.PCs.shape[1])

Names = [s.split('.')[0] for s in FilesVars]
Names_all = NamesOther + Names
nvarGroup_list = [1]*Other.shape[1] + nr_PCs
# nvarGroup = {Names_all[i]:z for i,z in enumerate(nvarGroup_list)}

Y = Infos.Consumption

idx = np.where(Y>500)[0]
# plt.hist(Y[idx]); plt.show()
DesignMatrix = DesignMatrix[idx]
Y = Y[idx]

n = len(Y)
p = DesignMatrix.shape[1]


print 'STEP 3: L2 DISTANCES TO 3-NN FOR ALTITUDE AND CAS'
from sklearn.neighbors import NearestNeighbors

knn_Alt = NearestNeighbors(n_neighbors=3)
knn_Alt.fit(Alt[idx])
distances_Alt, _ = knn_Alt.kneighbors()
	
knn_Cas = NearestNeighbors(n_neighbors=3)
knn_Cas.fit(Cas[idx])
distances_Cas, _ = knn_Cas.kneighbors()
	
L2dist = np.mean(distances_Alt, axis=1) + np.mean(distances_Cas, axis=1)
DesignMatrix = np.concatenate((DesignMatrix, L2dist.reshape((n,1))), axis = 1)
nvarGroup_list.append(1)
Names_all.append('L2dist')


np.savetxt('DesignMatrix.csv', DesignMatrix, delimiter=',')
np.savetxt('Consumption.csv', Y, delimiter=',')



groupsInfos = {'Names':Names_all, 'nvarGroup':nvarGroup_list}
with open('groupsInfos.json', 'w') as f:
	json.dump(groupsInfos, f)
