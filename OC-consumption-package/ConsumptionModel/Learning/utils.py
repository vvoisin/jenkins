import sys, csv, math, os
import numpy as np
import pandas as pd
from scipy.interpolate import UnivariateSpline


################
### ROUTINES ###
################



def Wmean(X, W):
	
	N = len(W)
	if N != X.shape[1]:
		sys.exit('Error shape')

	return np.dot(X, W.reshape((N,1))) / float(np.sum(W))


def dummy(x):
	# if type(x) != 'pandas.core.series.Series':
	# 	sys.exit('Error dummy type(x)')
	cat = x.unique()
	M = []
	for z in cat:
		tmp = np.array([int(zz) for zz in (x==z)])
		M.append(tmp)
	return (np.array(M).T, cat)


def norm_2(X):
	"""!@brief Computes the norm-2 of a vector.
	@param X : numpy array. Vector whose we want to compute the norm-2.
	@return float. Norm-2 of the vector given in argument.
	"""
	return np.linalg.norm(X, ord = 2)


def normalize(X):
	"""!@brief Normalizes the curves.
	@param X : numpy array. curve of one parameter and all flights.
	@return XX : numpy array. The normalized numpy array X given in argument.
	@return meanNorm : float. coefficient of the normalization.
	"""
	meanNorm = np.apply_along_axis(norm_2, 1, X).mean()
	def f(x):
		return x / meanNorm
	XX = np.apply_along_axis(f, 1, X)
	return XX



def smoothAlt(x):
	time = range(len(x))
	spl = UnivariateSpline(time, x, s=5000000)
	return spl(time)


def processAlt(x):
	n = len(x)
	Der = np.concatenate(([0], np.diff(x)))
	res = []

	for i,z in enumerate(Der):
		if i>0 and z > 100:
			res.append((x[i] + x[i-1]) / 2)
		else:
			res.append(x[i])
	return res




def processIvv(x, threshold=500):

	n = len(x)
	der = np.zeros(n)
	for i in range(n): # Derivative
		if i != n-1:
			tmp = x[i+1] - x[i]
			if abs(tmp) > threshold:
				der[i] = 0
			else:
				der[i] = tmp

	newIvv = np.cumsum(der) #Integration
	return newIvv



def extractOnes(x):

	if len(x) == 0:
		return 0

	if len(x) == 1 and x > 1:
		return 0

	# No ones
	if not np.any(x == 1):
		return 0

	# Only ones
	sumx = np.sum(x)
	if sumx == len(x):
		return sumx


	if x[0] > 1:
		return extractOnes(x[1:len(x)])

	top = np.where(x > 1)[0]


	if len(top) == 1:
		if top == 0:
			return sum(x[top+1:len(x)])
		elif top == len(x)-1:
			return sum(x[0:top])
		else:
			return [sum(x[0:top]), sum(x[top+1:len(x)])]
	else:

		if top[0] != 0:
			top = np.concatenate(([0],top))

		if top[len(top)-1] != len(x)-1:
			top = np.concatenate((top,[len(x)-1]))

		res = [extractOnes(x[0:top[1]+1])]
		for i, zz in enumerate(top[1:len(top)-1]):
			toAdd = extractOnes(x[zz+1:top[i+2]+1])
			res.append(toAdd)

		# if type(res) == int: res = [res]
		return np.asarray(res)


def level_off_detection(x, threshold=350, control = False):
	""" In: 
			Alt signal extracted from FL100 and ToC = max(Alt). 
		Out:
			nr of level off
			indexes of begin
			new ToC if false detection by max(Alt)
			index of ToC
	"""

	idxToC = None
	idxLO = None
	nrLO = 0
	newToC = None
	Ivv = np.diff(smoothAlt(x)) * 60
	idxInf300 = np.where(Ivv <= threshold)[0]
	nrOnes = extractOnes(np.diff(idxInf300))
	if nrOnes == []:
		if control:
			print 'idxToC = ' + str(idxToC)
			print 'newToC = ' + str(newToC)
			print 'idxLO = ' + str(idxLO)
			print 'nrLO = ' + str(nrLO)
		return 0, None, None, None

	if type(nrOnes) == list:
		nrOnes = np.asarray(nrOnes)

	if np.isscalar(nrOnes):
		nrOnes = np.array([nrOnes])

	idx = np.where(nrOnes > 300)[0]
	# print nrOnes, type(nrOnes)
	# print np.where(nrOnes > 300)
	
	if len(idx)!=0:
		# print 'ici' + str(np.sum(nrOnes[0:idx[0]]) + idx[0]) + str(idxInf300)
		idxToC = idxInf300[np.sum(nrOnes[0:idx[0]]) + idx[0]]
		newToC = x[idxToC]
		idx = np.where(nrOnes[0:idx[0]] > 20)[0]
		if control:
			print 'Cruise detected at index ' + str(idxToC)
	else:
		if control: print 'No cruise detected'
		idx = np.where(nrOnes > 20)[0]

	# print nrOnes
	# print idx

	if len(idx)==0:
		if control: print 'No level off detected'
	elif len(idx)==1:
		if idx[0] == 0:
			idxLO = idxInf300[0]
			nrLO = 1
		if control:
			print 'One level off detected at index ' + str(idxLO)
	else:
		idxLO = [idxInf300[np.sum(nrOnes[0:z]) + z] for z in idx]
		nrLO = len(idxLO)

		if control:
			print str(nrLO) + ' level off detected at index(es) ' + str(idxLO)

	# if len(idx)!=0:
	# 	if idx[0] != 0:
	# 		idxLO = [idxInf300[np.sum(nrOnes[0:z]) + z] for z in idx]
	# 		nrLO = len(idxLO)
	# 	else:
	# 		idxLO = idxInf300[0]
	# 		nrLO = 1

	# 	if control:
	# 		print 'Level off detected at index(es) ' + str(idxLO)
	# else:
	# 	if control: print 'No level off detected'


	if control:

		print 'idxToC = ' + str(idxToC)
		print 'newToC = ' + str(newToC)
		print 'idxLO = ' + str(idxLO)
		print 'nrLO = ' + str(nrLO)

		# plt.subplot(211)
		# plt.plot(x, '.-')
		# if idxToC != None: plt.plot([idxToC, idxToC], [min(x), max(x)], 'r-')
		# if idxLO != None: plt.plot([idxLO, idxLO], [min(x), max(x)], 'k-')
		# plt.ylabel('Alt')
		# plt.subplot(212)
		# plt.plot(Ivv, '.-')
		# plt.plot([0, len(Ivv)], [threshold, threshold], 'k-')
		# if idxToC != None: plt.plot([idxToC, idxToC], [min(Ivv), max(Ivv)], 'r-')
		# if idxLO != None: plt.plot([idxLO, idxLO], [min(Ivv), max(Ivv)], 'k-')
		# plt.ylabel('IVV')
		# plt.show()

	return nrLO, idxLO, newToC, idxToC


