# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.linear_model import SGDRegressor, Lasso
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import make_scorer, mean_squared_error
from sklearn.cross_validation import train_test_split, KFold

from ConsumptionModel.Learning.utils import *

import sys, traceback
import numpy as np




REGRESSOR_DEFAULT = {
	'RF': lambda: RandomForestRegressor(),
	'ETR': lambda: ExtraTreesRegressor(),
	'GBR': lambda: GradientBoostingRegressor(),
	'KNN': lambda: KNeighborsRegressor(),
	'SVR': lambda: SVR()
}


class Learner():
	"""Class for learning method"""
	
	# REGRESSOR = {
	# 	'RF': lambda n_estimators: RandomForestRegressor(n_jobs=-1, n_estimators=n_estimators),
	# 	'ETR': lambda n_estimators: ExtraTreesRegressor(n_jobs=-1, n_estimators=n_estimators),
	# 	'GBR': lambda n_estimators: GradientBoostingRegressor(n_estimators=n_estimators, loss='huber'),
	# 	'KNN': lambda k: KNeighborsRegressor(n_neighbors=k, weights='distance'),
	# 	'SVR': lambda C: SVR(kernel='rbf', C=C),
	# 	'SGD': lambda: SGDRegressor(loss='epsilon_insensitive', penalty='l2'),
	# 	'L1SGD': lambda: SGDRegressor(loss='epsilon_insensitive', penalty='l1')
	# }

	REGRESSOR = {
		'RF': RandomForestRegressor,
		'ETR': ExtraTreesRegressor,
		'GBR': GradientBoostingRegressor,
		'KNN': KNeighborsRegressor,
		'SVR': SVR,
		'SGD': SGDRegressor,
		'L1SGD': SGDRegressor
	}

	# DEFAULTS_PARAMETERS = {'RF': 500, 'ETR': 500, 'GBR': 500, 'KNN': 5, 'SVR': 1}

	def __init__(self, regressor, model=None, **kwargs):
		self.regressor = regressor
		self.kwargs = kwargs
		self.model = model
		self.predictions = None
		self.KfoldCV_ = None


	def __class__(self):
		pass

	# def getClass(self):
	# 	return 'Learner'

	def train(self, xtrain, ytrain):
		# print self.kwargs
		# if len(self.kwargs) == 1:
		model = self.REGRESSOR[self.regressor](**self.kwargs)
		model.fit(xtrain, ytrain)
		self.model = model


	def predict(self, xtest, ytest=None):
		pred = self.model.predict(xtest)
		# self.predictions = pred
		if ytest is not None:
			# self.mse = np.mean((ytest - pred)**2)
			# self.mae = np.mean(np.fabs(ytest - pred))
			mse = np.mean((ytest - pred)**2)
			mae = np.mean(np.fabs(ytest - pred))
			return pred, mse, mae
		return pred


	def KfoldCV(self, xdata, ydata, K=5, verbose=False):
		""" Je peux prendre la fct 'cross_val_score' de sklearn mais je prefere recoder moi meme pour etre sur. Voir pour calculer en parallèle 
			+ suffle = False dans KFold ? Toujours les meme fold alors ..."""
		n = len(ydata)
		kf = KFold(n, n_folds=K)
		mae, mse = [], []
		if self.model is None:
			model = self.REGRESSOR[self.regressor](**self.kwargs)
		else:
			model = self.model

		for idxtr, idxte in kf:
			model.fit(xdata[idxtr], ydata[idxtr])
			pred = model.predict(xdata[idxte])
			mae.append( np.fabs(ydata[idxte] - pred) )
			mse.append(  (ydata[idxte] - pred)**2 )

		return mae, mse



# 
# ROUTINES
# 

def gridSearch(regressor, xdata, ydata, param_grid, n_jobs=8):
	""" Grid search by cross validation """
	estimator = REGRESSOR_DEFAULT[regressor]()
	try:
		grid = GridSearchCV(estimator, param_grid=param_grid, n_jobs=n_jobs)
		grid.fit(xdata, ydata)
	except (KeyboardInterrupt, SystemExit):
		sys.exit('KeyboardInterrupt')

	return Learner(regressor=regressor, model=grid.best_estimator_), grid.best_params_
