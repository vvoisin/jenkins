# !/usr/bin/env python
#  -*- coding: utf-8 -*-

from sklearn.grid_search import GridSearchCV
from sklearn.metrics import mean_squared_error
from sklearn.cross_validation import KFold
from sklearn import preprocessing

from ConsumptionModel.Learning.utils import *
from ConsumptionModel.Learning.Learner import *

import sys, traceback
import numpy as np



class CombinedLearner():
	"""Class for a combined learning machine"""


	def __init__(self, method, machines, eta=None, W=None):
	
		# ICI TEST LA LISTE DES MACHINES : METHODES FIT ET PREDICT
		if machines.__class__ is not dict:
			sys.exit('The machines must be a dictionary: {<Name>:<Learner instance>, ...}')
		self.machines = machines

		if len(machines) != 0 and not self.control():
			sys.exit('The machines must be a dictionary of Learner instances')

		if method == 'ewa' and eta is None:
			sys.exit('For EWA, eta must be passed')

		# if method.__class__.__name__ != 'function':
		# 	sys.exit('The agregation method must be a function. Ex. numpy.mean or numpy.median.')
		self.method = method
		self.W = W
		self.eta = eta


		self.combined_predictions = None


	def control(self):
		cont = [m.__class__.__name__ == 'Learner' for m in self.machines.values()]
		return all(cont)

	def add_machine(self, machine):
		# ICI TEST LA VALIDITE DE LA MACHINE : METHODES FIT ET PREDICT
		self.machine.append(machine)


	def train(self, xtrain, ytrain, verbose=True):
		for m in iter(self.machines):
			try:
				if verbose: print 'Training machine ' + str(m)
				self.machines[m].train(xtrain, ytrain)
			except:
				print 'Fail in training machine ' + str(m)
				traceback.print_exc()
				sys.exit('Ending')


	def predict(self, xtest, ytest=None):
		
		predictions = np.array([self.machines[m].predict(xtest) for m in iter(self.machines)]).T

		if self.method == 'mean':
			self.combined_predictions = np.mean(predictions, axis=1)

		if self.method == 'Wmean':
			if self.W is None:
				sys.exit('Please enter the weight vector')
			self.combined_predictions = Wmean(predictions, self.W)

		if self.method == 'median':
			self.combined_predictions = np.median(predictions, axis=1)


		if self.method == 'ewa':
			weights = self.ewa(self.eta)
			self.combined_predictions = np.sum(weights * predictions)

		if self.method == 'cobra':
			sys.exit('Not yet implemented')

		if ytest is not None:
			return self.combined_predictions, np.mean(np.fabs(ytest - self.combined_predictions))
		else:
			return self.combined_predictions


	def ewa(self, eta):
		control = [self.machines[m].KfoldCV_ is None for m in iter(self.machines)]
		if any(control):
			sys.exit('You must run KfolCV for each machines')

		errors = np.array([self.machines[m].KfoldCV_ for m in iter(self.machines)])
		tmp = np.exp(-eta * errors)
		return tmp / np.sum(tmp)


	def KfoldCV(self, xdata, ydata, K=5, verbose=False):
		""" Je peux prendre la fct 'cross_val_score' de sklearn mais je prefere recoder moi meme pour etre sur. Voir pour calculer en parallèle 
			+ suffle = False dans KFold ? Toujours les meme fold alors ..."""
		n = len(ydata)
		kf = KFold(n, n_folds=K)
		mae = []
		
		for idxtr, idxte in kf:
			self.train(xdata[idxtr], ydata[idxtr], verbose=False)
			pred = self.predict(xdata[idxte])
			mae.append(np.mean(np.fabs(ydata[idxte] - pred)))
		return mae



class EWA():
	""" Exponentially Weighted Aggregation method """


	def __init__(self, machines, gamma):
	
		self.machines = machines
		if machines.__class__ is not dict:
			sys.exit('The machines must be a dictionary: {<Name>:<Learner instance>, ...}')

		if len(machines) != 0 and not self.control():
			sys.exit('The machines must be a dictionary of Learner instances')

		self.gamma = gamma
		self.combined_predictions = None
		self.risks = []
		self.weights = []


	def control(self):
		cont = [m.__class__.__name__ == 'Learner' for m in self.machines.values()]
		return all(cont)


	def add_machine(self, machine):
		self.machine.append(machine)


	def train(self, xtrain, ytrain, verbose=False):
		""" Three step:
				1- Train each machine (if not trained before)
				2- Compute the quadratic risks L_n(f_1), \ldots, L_n(f_N)
				3- Compute the weights w_k = exp(-gamma * L_n(f_k)) / \sum_l exp(-gamma * L_n(f_l))
		"""
		W = []
		for m in iter(self.machines):
			if self.machines[m].model is None:
				# try:
				if verbose: print 'Training machine ' + str(m)
				self.machines[m].train(xtrain, ytrain)
				# except:
				# 	traceback.print_exc()
				# 	sys.exit('Fail in training machine ' + str(m))

			# EMPIRICAL RISK does not work well
			# pred = self.machines[m].predict(xtrain)
			# Ln = np.mean( (pred - ytrain)**2 )
			# 3-FOLD CV
			if verbose: print '3 fold CV'
			_, Ln_cv = self.machines[m].KfoldCV(xtrain, ytrain, K=3)
			Ln = np.mean(Ln_cv)
			self.risks.append(Ln)
			W.append( np.exp( -self.gamma * Ln) )
		
		W = np.array(W)
		if np.sum(W) == 0:
			print 'Weights = ' + str(W)
			raise ZeroDivisionError
		self.weights = W / np.sum(W)
		
		if verbose:
			print 'Weights ' + str(self.weights)
			print 'Risks ' + str(self.risks)


	def predict(self, xtest, ytest=None):
		""" 1- Predict a new sample X_{n+1} with each machine f_1, \dots, f_N
			2- f_ewa(X_{n+1}) = \sum_k w_k f_k(X_{n+1})
		"""
		predictions = np.array([self.machines[m].predict(xtest) for m in iter(self.machines)]).T
		weights = np.array(self.weights)
		pred_ewa = np.dot(predictions, weights)
		
		if ytest is not None:
			mae = np.mean( np.fabs(ytest - pred_ewa) )
			mse = np.mean( (ytest - pred_ewa)**2 )
			return pred_ewa, mae, mse
		else:
			return pred_ewa



	def KfoldCV(self, xdata, ydata, K=5, verbose=False):
		""" Je peux prendre la fct 'cross_val_score' de sklearn mais je prefere recoder moi meme pour etre sur. Voir pour calculer en parallèle 
			+ suffle = False dans KFold ? Toujours les meme fold alors ..."""
		n = len(ydata)
		kf = KFold(n, n_folds=K)
		mae = []
		
		for idxtr, idxte in kf:
			self.train(xdata[idxtr], ydata[idxtr], verbose=False)
			pred = self.predict(xdata[idxte])
			mae.append( np.fabs(ydata[idxte] - pred) )
		return mae


	# def bootstrap(self, xdata, ydata, nboots=100, verbose=False):

	# 	n = len(ydata)
	# 	ntr = round(2*n/3)
	# 	nte = round(n/3)

	# 	mae = []
	# 	for k in range(nboots):
	# 		xdata_tr, xdata_te, ydata_tr, xdata_te = train_test_split(xdata, ydata, train_size = ntr, test_size=nte)
	# 		self.train(xdata_tr, ydata_tr, verbose=False)
	# 		pred = self.predict(xdata_te)
	# 		mae.append(np.mean(np.fabs(ydata_te - pred)))
	# 	return mae

