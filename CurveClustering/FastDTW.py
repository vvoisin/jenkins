# !/usr/bin/env python
#  -*- coding: utf-8 -*-

import time, sys, os, traceback, pp, numpy
import pandas as pd

# import matplotlib
# matplotlib.use('Agg')

import matplotlib.pyplot as plt
import fastdtw
from sklearn.cluster import SpectralClustering


class FastDTW():
	""" 
		Clustering functional data using DTW pseudo-distance 
	"""
	
	def __init__(self, gamma=1, ncpus=-1, affinity=True):
		self.gamma = gamma
		self.ncpus = ncpus
		
		self.affinity = affinity
		if affinity:
			self.affinity_matrix = None
		else:
			self.distance_matrix = None

		self.labels = None


	def DTW_matrix(self, data_list, verbose=True):
		""" 
			Distance matrix using fastDtw. 
			data_list : list of size n which elements are lists of length N_i 
		"""

		n = len(data_list)
		indexes = []

		# GET INDEXES
		for i in range(0,n):
			for j in range(i+1,n):
				indexes.append((i,j))

		# INIT DISTANCE MATRIX
		distance_matrix = numpy.zeros((n,n))


		# COMPUTE DTW IN PARALLEL
		start_time = time.time()
		def wrapper(i, j, dat):
			return fastdtw(dat[i], dat[j])[0]

		job_server = pp.Server(ppservers=())
		if self.ncpus == -1:
			ncp = job_server.get_ncpus()
		else:
			ncp = self.ncpus
		if verbose: print 'Distance matrix: parallel execution with ncpus = ' + str(ncp)
		job_server.set_ncpus(ncp)
		depfuncs = (fastdtw,)
		if verbose: print("--- CPUs initialization: %s seconds ---" % round(time.time() - start_time, 2))

		start_time = time.time()
		jobs = [job_server.submit(func=wrapper, args=(i, j, data_list), depfuncs=depfuncs) for i,j in indexes]
		distance_vect = numpy.array([job() for job in jobs])
		if verbose: print("--- DTW computation: %s seconds ---" % round(time.time() - start_time, 2))


		for k,(i,j) in enumerate(indexes):
			distance_matrix[i,j] = distance_vect[k]
			distance_matrix[j,i] = distance_vect[k]

		if self.affinity:
			self.affinity_matrix = numpy.exp(-self.gamma * distance_matrix**2)
		else:
			self.distance_matrix = distance_matrix


	def SpectralClust(self, n_clusters):
		SC = SpectralClustering(n_clusters=n_clusters, affinity='precomputed')
		
		if self.affinity:
			SC.fit(self.affinity_matrix)
		else:
			TypeError('Spectral clustering works better with affinity matrix')

		self.labels = SC.labels_


def normalize(X):
	meanNorm = numpy.apply_along_axis(norm_2, 1, X).mean()
	def f(x):
		return x / meanNorm
	XX = numpy.apply_along_axis(f, 1, X)
	return XX



