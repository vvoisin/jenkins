# !/usr/bin/env python
#  -*- coding: utf-8 -*-

import time, sys, os, traceback
import pandas as pd

# import matplotlib
# matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
from FastDTW import *
sys.path.append('/Users/bgregorutti/Documents/Projets/fuel-research/DataExtractor/')
from DataExtractor import *

# 
# MAIN
# 

obj = DataExtractor(params=['ALTITUDE', 'GS'], ns=None, flight_phase='takeoff')
Folder = '../../Data/TVF/dataTest/TVF-extract/'
Files = ['F-GZHA-0415180.csv', 'F-GZHA-0415210.csv', 'F-GZHA-0516080.csv', 'F-GZHA-0516120.csv']
Time, data_sparse, data_list = obj.run(Files, Folder, ncpus=-1)


# GET DATA
data_list_alt = [z['GS'] for z in data_list]
# NORMALIZE
meanNorm = np.mean([np.linalg.norm(z, ord = 2) for z in data_list_alt])
data_list_alt_norm = [z / meanNorm for z in data_list_alt]

print 'Spectral Clustering'
DtwClust = FastDTW(gamma=1, ncpus=-1, affinity=True)
DtwClust.DTW_matrix(data_list_alt_norm)
DtwClust.SpectralClust(n_clusters=2)

COLS = np.where(DtwClust.labels == 1, 'r', 'b')
for i,z in enumerate(data_list_alt):
	plt.plot(z, '-', c=COLS[i])
plt.show()






sys.exit(0)







# 2 courbes

ALT = data_sparse[0]

c0 = ALT[0,:]
idx_null = np.where(np.isnan(c0))[0][0]
c0 = c0[0:idx_null]

c1 = ALT[1,:]
idx_null = np.where(np.isnan(c1))[0][0]
c1 = c1[0:idx_null]

print c0
print c1

print len(c0), len(c1)




dtw = fastdtw(c0, c1)

distDtw = dtw[0]
warp = dtw[1]

warp_x = [z[0] for z in warp]
warp_y = [z[1] for z in warp]
plt.plot(warp_x, warp_y, '-', linewidth=2)
plt.show()












# 
# TEST ONE
# 

obj = DataExtractor(params=['ALTITUDE', 'GS'], ns=500, flight_phase='takeoff')
R1 = obj.run_one('../../Data/TVF/dataTest/TVF-extract/F-GZHA-0415180.csv')

print np.max(R1['ALTITUDE']), len((R1['ALTITUDE']))

plt.figure()
plt.plot(R1['Time'], R1['ALTITUDE'], '-')
plt.ylabel('ALTITUDE')
plt.xlabel('Time')

plt.figure()
plt.plot(R1['Time'], R1['GS'], '-')
plt.ylabel('GS')
plt.xlabel('Time')


plt.show()
